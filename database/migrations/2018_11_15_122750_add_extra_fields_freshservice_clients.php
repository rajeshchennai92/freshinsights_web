<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraFieldsFreshserviceClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('freshservice_clients', function($table) {
            $table->integer('initial_process')->default(0);
            $table->integer('cron')->default(0)->change();
            $table->bigInteger('ticket_count')->default(0);
            $table->bigInteger('agent_count')->default(0);
            $table->bigInteger('page_count')->default(1);
            $table->bigInteger('frequency_days')->default(0);
            $table->bigInteger('frequency_hours')->default(0);
            $table->string('organization_db_name');
            $table->dateTime('updated_on')->nullable()->default(null);
            $table->dropColumn('is_initial_completed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('freshservice_clients', function($table) {
            $table->dropColumn('cron');
            $table->dropColumn('initial_process');
            $table->dropColumn('ticket_count');
            $table->dropColumn('agent_count');
            $table->dropColumn('page_count');
            $table->dropColumn('frequency_days');
            $table->dropColumn('frequency_hours');
            $table->dropColumn('organization_db_name');
            $table->dropColumn('updated_on');
        });
    }
}
