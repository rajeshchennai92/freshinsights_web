<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFreshdeskClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('freshdesk_clients', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('organization_name');
            $table->string('organization_url');
            $table->string('organization_api_key');

            $table->string('export_url')->nullable();

            $table->integer('initial_process')->default(0);
            $table->integer('cron')->default(0);
            $table->bigInteger('ticket_count')->default(0);
            $table->bigInteger('agent_count')->default(0);
            $table->bigInteger('page_count')->default(1);
            $table->bigInteger('frequency_days')->default(0);
            $table->bigInteger('frequency_hours')->default(0);
            $table->boolean('scheduled_report')->default(false);
            $table->boolean('basic')->default(true);
            $table->boolean('status')->default(false);
            $table->string('organization_db_name');

            $table->dateTime('updated_on')->nullable()->default(null);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('freshdesk_clients');
    }
}
