<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHistoricalDateToFreshserviceClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('freshservice_clients', function($table) {
            $table->integer('historical_payment')->default(0);
            $table->bigInteger('historical_ticket_count')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('freshservice_clients', function($table) {
            $table->dropColumn('historical_payment');
            $table->dropColumn('historical_ticket_count');
        });
    }
}
