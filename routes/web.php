<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a given Closure or controller and enjoy the fresh air.
|
*/

/*
|--------------------------------------------------------------------------
| Welcome Page
|--------------------------------------------------------------------------
*/

Route::get('/', 'PagesController@home');

/*
|--------------------------------------------------------------------------
| Contact form
|--------------------------------------------------------------------------
*/

Route::post('/contactform/send', 'PagesController@contact');

/*
|--------------------------------------------------------------------------
| Contact form
|--------------------------------------------------------------------------
*/

Route::get('/terms-of-service', 'PagesController@terms')->name('termsPage');

/*
|--------------------------------------------------------------------------
| Freshservice Tableau registration success page
|--------------------------------------------------------------------------
*/

Route::get('/freshservice/tableau/success', 'PagesController@tableauSuccess')->name('freshserviceTableauSuccess');
Route::get('/freshdesk/tableau/success', 'PagesController@freshdesktableauSuccess')->name('freshdeskTableauSuccess');

/*
|--------------------------------------------------------------------------
| Product page and Pricing page
|--------------------------------------------------------------------------
*/

Route::get('/solution/freshinsights-for-freshdesk', 'PagesController@freshdeskPage')->name('freshdeskPage');
Route::get('/solution/freshinsights-for-freshservice', 'PagesController@freshservicePage')->name('freshservicePage');
Route::get('/solution/freshinsights-for-freshservice-in-tableau', 'PagesController@freshserviceTableauPage')->name('freshserviceTableauPage');
Route::get('/solution/other-visualizations', 'PagesController@otherPage')->name('otherPage');
Route::get('/solution/freshinsights-for-freshsdesk-in-tableau', 'PagesController@freshsdeskTableauPage')->name('freshsdeskTableauPage');
Route::get('/solution/freshinsights-for-freshdesktableaupage', 'PagesController@freshdeskTableauPage')->name('freshdeskTableauPage');

/*
|--------------------------------------------------------------------------
| Login/ Logout/ Password
|--------------------------------------------------------------------------
*/
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('freshdesk/login', 'Auth\LoginController@showLoginPlanForm');
Route::post('freshdesk/login', 'Auth\LoginController@loginPlanForm');

Route::get('freshservice/login', 'Auth\LoginController@showLoginFreshserviceForm');
Route::post('freshservice/login', 'Auth\LoginController@loginFreshserviceForm');

Route::get('freshservice/tableau/login', 'Auth\LoginController@showLoginFreshserviceTableauForm');
Route::post('freshservice/tableau/login', 'Auth\LoginController@loginFreshserviceTableauForm');


Route::get('freshdesk/tableau/login', 'Auth\LoginController@showLoginFreshdeskTableauForm');
Route::post('freshdesk/tableau/login', 'Auth\LoginController@loginFreshdeskTableauForm');
/*
|--------------------------------------------------------------------------
| Registration & Activation
|--------------------------------------------------------------------------
*/

Route::get('register/freshdesk', 'Auth\RegisterController@freshdeskRegisterView');
Route::get('register/freshservice', 'Auth\RegisterController@freshserviceRegisterView');
Route::get('register/freshservice/tableau', 'Auth\RegisterController@freshserviceTableauRegisterView');
Route::get('register/freshdesk/tableau', 'Auth\RegisterController@freshdeskTableauRegisterView');

Route::post('register/freshdesk', 'Auth\RegisterController@freshdeskRegister');
Route::post('register/freshservice', 'Auth\RegisterController@freshserviceRegister');
Route::post('register/freshservice/tableau', 'Auth\RegisterController@freshserviceTableauRegister');
Route::post('register/freshdesk/tableau', 'Auth\RegisterController@freshdeskRegister');
Route::post('test-connection', 'Auth\RegisterController@testConnection');

//Route::group(['middleware' => ['auth']], function () {
//    Route::get('create/freshdesk', 'Auth\RegisterController@freshdeskCreate');
//    Route::get('create/freshservice', 'Auth\RegisterController@freshserviceCreate');
//});

//Route::get('activate/token/{token}', 'Auth\ActivateController@activate');
//Route::group(['middleware' => ['auth']], function () {
//    Route::get('activate', 'Auth\ActivateController@showActivate');
//    Route::get('activate/send-token', 'Auth\ActivateController@sendToken');
//});

/*
|--------------------------------------------------------------------------
| Authenticated Routes
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => ['auth']], function () {

    /*
    |--------------------------------------------------------------------------
    | General
    |--------------------------------------------------------------------------
    */

    //Route::get('/users/switch-back', 'Admin\UserController@switchUserBack');

    /*
    |--------------------------------------------------------------------------
    | User
    |--------------------------------------------------------------------------
    */

    Route::group(['prefix' => 'user', 'namespace' => 'User'], function () {
        Route::get('settings', 'SettingsController@settings');
        Route::post('settings', 'SettingsController@update');
        Route::get('password', 'PasswordController@password');
        Route::post('password', 'PasswordController@update');
    });

    /*
    |--------------------------------------------------------------------------
    | Dashboard
    |--------------------------------------------------------------------------
    */

//    Route::get('/dashboard', 'PagesController@dashboard');

    /*
    |--------------------------------------------------------------------------
    | Admin
    |--------------------------------------------------------------------------
    */

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin'], function () {

        Route::get('dashboard', 'DashboardController@index');

        /*
        |--------------------------------------------------------------------------
        | Users
        |--------------------------------------------------------------------------
        */
        Route::resource('users', 'UserController', ['except' => ['create']]);
        Route::post('users/search', 'UserController@search');
        Route::get('users/search', 'UserController@index');

        /*
        |--------------------------------------------------------------------------
        | Roles
        |--------------------------------------------------------------------------
        */
        Route::resource('roles', 'RoleController', ['except' => ['show']]);
        Route::post('roles/search', 'RoleController@search');
        Route::get('roles/search', 'RoleController@index');
    });
});
