<?php

/*
|--------------------------------------------------------------------------
| Billing Routes
|--------------------------------------------------------------------------
*/

Route::group(['namespace' => 'User'], function () {
    Route::get('freshinsights-for-freshdesk/pricing', 'FreshdeskBillingController@getFreshdeskSubscribe')->name('freshdeskPricing');
    Route::get('freshinsights-for-freshdesk-tableau/pricing', 'FreshdeskBillingController@getFreshdeskTableauSubscribe')->name('freshdeskTableauPricing');
    Route::get('freshinsights-for-freshservice/pricing', 'FreshserviceBillingController@getFreshserviceSubscribe')->name('freshservicePricing');
    Route::get('freshinsights-for-freshservice-tableau/pricing', 'FreshserviceBillingController@getFreshserviceTableauSubscribe')->name('freshserviceTableauPricing');
});

Route::group(['middleware' => 'auth', 'namespace' => 'User'], function () {

    Route::group(['prefix' => 'freshdesk'], function () {
        Route::get('select_subscription', 'FreshdeskBillingController@freshdeskSubscribeChoose')->name('freshdeskSubscriptionChoose');
        Route::get('select_subscription_tableau', 'FreshdeskBillingController@freshdeskTableauSubscribeChoose')->name('freshdeskTableauSubscribeChoose');

        Route::post('selected_plan_details', 'FreshdeskBillingController@freshdeskSelectedPlan');
        Route::post('selected_plan_details_tableau', 'FreshdeskBillingController@freshdeskTableauSelectedPlan');

        Route::get('confirm_subscription', 'FreshdeskBillingController@freshdeskSubscribeConfirm')->name('freshdeskSubscriptionConfirmation');
        Route::get('confirm_subscription_tableau', 'FreshdeskBillingController@freshdeskTableauSubscribeConfirm')->name('freshdeskTableauSubscriptionConfirmation');

    });

    Route::group(['prefix' => 'freshservice'], function () {
        Route::get('select_subscription', 'FreshserviceBillingController@freshserviceSubscribeChoose')->name('freshserviceSubscriptionChoose');
        Route::get('select_subscription_tableau', 'FreshserviceBillingController@freshserviceTableauSubscribeChoose')->name('freshserviceTableauSubscriptionChoose');

        Route::post('selected_plan_details', 'FreshserviceBillingController@freshserviceSelectedPlan');
        Route::post('selected_plan_details_tableau', 'FreshserviceBillingController@freshserviceTableauSelectedPlan');

        Route::get('confirm_subscription', 'FreshserviceBillingController@freshserviceSubscribeConfirm')->name('freshserviceSubscriptionConfirmation');
        Route::get('confirm_subscription_tableau', 'FreshserviceBillingController@freshserviceTableauSubscribeConfirm')->name('freshserviceTableauSubscriptionConfirmation');

    });

    Route::group(['prefix' => 'subscribe'], function () {
        Route::post('free/freshdesk', 'FreshdeskBillingController@freshdeskSubscribeFree');
        Route::post('freshdesk', 'FreshdeskBillingController@freshdeskSubscribe');
        Route::post('free/freshservice', 'FreshserviceBillingController@freshserviceSubscribeFree');
        Route::post('freshservice', 'FreshserviceBillingController@freshserviceSubscribe');
    });

    Route::group(['prefix' => 'modify_plan'], function () {
        Route::post('freshdesk', 'FreshdeskBillingController@freshdeskModifyPlan');
        Route::post('freshdesk/free', 'FreshdeskBillingController@freshdeskModifyFreePlan');
    });

    Route::group(['prefix' => 'cancel/subscription'], function () {
        Route::get('freshdesk', 'FreshdeskBillingController@cancelFDSubscriptionForUser')->name('freshdeskCancelSubscription');
        Route::get('freshservice', 'FreshserviceBillingController@cancelFSSubscriptionForUser')->name('freshserviceCancelSubscription');
    });

    Route::group(['prefix' => 'freshdesk'], function () {
        Route::post('ticket_count', 'FreshdeskBillingController@getFreshdeskTicketCount');
    });
    Route::group(['prefix' => 'user'], function () {

        Route::group(['prefix' => 'billing'], function () {
            Route::post('change-card', 'BillingController@postChangeCard');
//            Route::get('cancellation', 'BillingController@cancelSubscription');
            Route::get('cancellation/{id}', 'BillingController@cancelSubscription');
            Route::get('free/cancellation/{id}', 'FreshdeskBillingController@cancelFreeSubscription');
            Route::get('freshservice/free/cancellation/{id}', 'FreshserviceBillingController@cancelFreeFreshserviceSubscription');
            Route::post('free/enable/{id}', 'FreshdeskBillingController@enableFreeSubscription');
            Route::post('freshservice/free/enable/{id}', 'FreshserviceBillingController@enableFreeFreshserviceSubscription');
            Route::get('invoices', 'BillingController@getInvoices');
            Route::get('invoice/{id}', 'BillingController@getInvoiceById');
//            Route::get('coupon', 'BillingController@getCoupon');
//            Route::post('coupon', 'BillingController@postCoupon');
        });

        Route::get('myaccount', 'BillingController@getMyAccount')->name('userAccount');

    });
});

/*
|--------------------------------------------------------------------------
| Failed Payments
|--------------------------------------------------------------------------
*/

Route::post(
    'stripe/webhook',
    '\App\Http\Controllers\WebHookController@handleWebhook'
);
