<?php

/*
|--------------------------------------------------------------------------
| Invoice Config
|--------------------------------------------------------------------------
*/

return [

    'company'       => 'RADIARE INC',
    'street'        => '79 SENECA LANE',
    'location'      => 'BORDENTOWN, New Jersey 08505',
    'country'      => 'United States',
    'url'           => '/img/radiare.png',

];