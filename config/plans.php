<?php

/*
|--------------------------------------------------------------------------
| Subscription Config
|--------------------------------------------------------------------------
*/

return [

    'subscriptions' => [
        'freshdesk' => [
            'subscription_name' => 'freshdesk',
            'setup_fee' => 0,
            'additional_setup_fee' => 20,
            'plans' => [
                'free' => [
                    'name' => 'Free Trial',
                    'stripe_id' => 'plan_CzAbZeFwQj7QnK',
                    'plan_amount' => '0',
                    'annual_stripe_id' => 'annual_free',
                    'annual_plan_amount' => '0',
                    'additional_fee' => '0',
                    'allowed_history_ticket_count' => '5000',
                    'allowed_ticket_count' => '5000',
                    'additional_fee_plan_id' => 'plan_Cy4291knkeF4QH',
                    'frequency_hours' => '0',
                    'frequency_days' => '1',
                    'features' => [
                        'Monthly incremental tickets limit' => '5,000',
                        'Data refresh frequency' => 'Daily',
                        'Permission to Share Dashboards' => 'Unrestricted Sharing',
                    ],
                    'tooltip' => [
                        'Monthly incremental tickets limit' => 'In case where the number of tickets exceed the eligible number of tickets as part of the pricing plan, additional fees will be charged. Pricing for each additional ticket is given here.',
                        'Data refresh frequency' => 'Frequency of data refresh.',
                        'Permission to Share Dashboards' => 'Permission to share the dashboards internally to other members in your organization.',
                    ],
                ],
                'basic' => [
                    'name' => 'Basic',
                    'stripe_id' => 'plan_GORd1oxMpY25zW',
                    'plan_amount' => '49',
                    'annual_stripe_id' => 'annual_basic_2020_20off',
                    'annual_plan_amount' => '499',
                    'additional_fee' => '1',
                    'allowed_ticket_count' => '3000',
                    'allowed_history_ticket_count' => '50000',
                    'additional_fee_plan_id' => 'basic_add',
                    'frequency_hours' => '0',
                    'frequency_days' => '1',
                    'features' => [
                        'Monthly incremental tickets limit' => '3,000',
                        'Data refresh frequency' => 'Daily',
                        'Permission to Share Dashboards' => 'Unrestricted Sharing',
                    ],
                ],
                'basic_plus' => [
                    'name' => 'Basic Plus',
                    'stripe_id' => 'basic_plus_2020',
                    'plan_amount' => '69',
                    'annual_stripe_id' => 'annual_basic_plus_2020_20off',
                    'annual_plan_amount' => '699',
                    'additional_fee' => '1',
                    'allowed_ticket_count' => '5000',
                    'allowed_history_ticket_count' => '50000',
                    'additional_fee_plan_id' => 'basic_plus_add',
                    'frequency_hours' => '6',
                    'frequency_days' => '0',
                    'features' => [
                        'Monthly incremental tickets limit' => '5,000',
                        'Data refresh frequency' => '4 times daily',
                        'Permission to Share Dashboards' => 'Unrestricted Sharing',
                    ],
                ],
                'premium' => [
                    'name' => 'Premium',
                    'stripe_id' => 'premium_2020',
                    'plan_amount' => '89',
                    'annual_stripe_id' => 'annual_premium_2020_20off',
                    'annual_plan_amount' => '899',
                    'additional_fee' => '1',
                    'allowed_ticket_count' => '8000',
                    'allowed_history_ticket_count' => '50000',
                    'additional_fee_plan_id' => 'premium_add',
                    'frequency_hours' => '3',
                    'frequency_days' => '0',
                    'features' => [
                        'Monthly incremental tickets limit' => '8,000',
                        'Data refresh frequency' => '8 times daily',
                        'Permission to Share Dashboards' => 'Unrestricted Sharing',
                    ],
                ],
            ]
        ],
        'freshdesk_tableau' => [
            'subscription_name' => 'freshdesk_tableau',
            'setup_fee' => 0,
            'additional_setup_fee' => 20,
            'plans' => [
                'free' => [
                    'name' => 'Plan',
                    'stripe_id' => 'plan_CzAbZeFwQj7QnK',
                    'plan_amount' => '0',
                    'annual_stripe_id' => 'annual_free',
                    'annual_plan_amount' => '0',
                    'additional_fee' => '0',
                    'allowed_history_ticket_count' => '5000',
                    'allowed_ticket_count' => '5000',
                    'additional_fee_plan_id' => 'plan_Cy4291knkeF4QH',
                    'frequency_hours' => '0',
                    'frequency_days' => '1',
                    'features' => [
                        'Data Hosted on our secure Azure Server' => 'USD 1,600',
                        'Data hosted on-premise' => 'USD 2,600',
                    ],

                ],
            ]
        ],
        'freshservice_tableau' => [
            'subscription_name' => 'freshservice_tableau',
            'setup_fee' => 0,
            'additional_setup_fee' => 20,
            'plans' => [
                'free' => [
                    'name' => 'Plan',
                    'stripe_id' => 'plan_CzAbZeFwQj7QnK',
                    'plan_amount' => '0',
                    'annual_stripe_id' => 'annual_free',
                    'annual_plan_amount' => '0',
                    'additional_fee' => '0',
                    'allowed_history_ticket_count' => '5000',
                    'allowed_ticket_count' => '5000',
                    'additional_fee_plan_id' => 'plan_Cy4291knkeF4QH',
                    'frequency_hours' => '0',
                    'frequency_days' => '1',
                    'features' => [
                        'Data Hosted on our secure Azure Server' => 'USD 1,900'.'+'.'USD 2,400',
                        'Data hosted on-premise' => 'USD 2,900'.'+'.'USD 3,400',
//                        'Data Hosted on our secure Azure Server plus Tableau TWB File for Freshservice with Incidents & Assets' => 'USD 2,400',
//                        'Data hosted on-premise plus Tableau TWB File for Freshservice with Incidents & Assets' => 'USD 3,400',

                    ],

                ],
            ]
        ],
        'freshservice' => [
            'subscription_name' => 'freshservice',
            'setup_fee' => 0,
            'additional_setup_fee' => 1,
            'plans' => [
                'free' => [
                    'name' => 'Free Trial',
                    'stripe_id' => 'plan_CzAbZeFwQj7QnK',
                    'plan_amount' => '0',
                    'annual_stripe_id' => 'annual_free',
                    'annual_plan_amount' => '0',
                    'additional_fee' => '0',
                    'allowed_history_ticket_count' => '5000',
                    'allowed_ticket_count' => '5000',
                    'additional_fee_plan_id' => 'plan_Cy4291knkeF4QH',
                    'frequency_hours' => '0',
                    'frequency_days' => '1',
                    'features' => [
                        'Modules Covered' => 'Incidents and Assets',
                        'Historical Tickets Loading Limit' => '5,000',
                        'Monthly incremental tickets limit' => '5,000',
                        'Data refresh frequency' => 'Daily',
                        'Permission to Share Dashboards' => 'Unrestricted Sharing',
                    ],
                    'tooltip' => [
                        'Modules Covered' => 'Incidents or Assets',
                        'Historical Tickets Loading Limit' => 'Number of tickets eligible as part of the pricing plan. For tickets exceeding this number additional charges will apply.',
                        'Monthly incremental tickets limit' => 'In case where the number of tickets exceed the eligible number of tickets as part of the pricing plan, additional fees will be charged. Pricing for each additional ticket is given here.',
                        'Data refresh frequency' => 'Frequency of data refresh.',
                        'Permission to Share Dashboards' => 'Permission to share the dashboards internally to other members in your organization.',
                    ],
                ],
                'blossom' => [
                    'name' => 'Blossom',
                    'stripe_id' => 'blossom',
                    'plan_amount' => '99',
                    'annual_stripe_id' => 'annual_blossom',
                    'annual_plan_amount' => '1089',
                    'additional_fee' => '1',
                    'allowed_ticket_count' => '5000',
                    'allowed_history_ticket_count' => '25000',
                    'additional_fee_plan_id' => 'blossom_add',
                    'frequency_hours' => '6',
                    'frequency_days' => '0',
                    'features' => [
                        'Modules Covered' => 'Incidents Only',
                        'Historical Tickets Loading Limit' => '25,000',
                        'Monthly incremental tickets limit' => '5,000',
                        'Data refresh frequency' => '4 times daily',
                        'Permission to Share Dashboards' => 'Unrestricted Sharing',
                    ],
                ],
                'blossom_plus' => [
                    'name' => 'Blossom Plus',
                    'stripe_id' => 'blossom_plus',
                    'plan_amount' => '129',
                    'annual_stripe_id' => 'annual_blossom_plus',
                    'annual_plan_amount' => '1419',
                    'additional_fee' => '1',
                    'allowed_ticket_count' => '10000',
                    'allowed_history_ticket_count' => '50000',
                    'additional_fee_plan_id' => 'blossom_plus_add',
                    'frequency_hours' => '3',
                    'frequency_days' => '0',
                    'features' => [
                        'Modules Covered' => 'Incidents Only',
                        'Historical Tickets Loading Limit' => '50,000',
                        'Monthly incremental tickets limit' => '10,000',
                        'Data refresh frequency' => '8 times daily',
                        'Permission to Share Dashboards' => 'Unrestricted Sharing',
                    ],
                ],
                'garden' => [
                    'name' => 'Garden',
                    'stripe_id' => 'garden',
                    'plan_amount' => '159',
                    'annual_stripe_id' => 'annual_garden',
                    'annual_plan_amount' => '1749',
                    'additional_fee' => '1',
                    'allowed_ticket_count' => '10000',
                    'allowed_history_ticket_count' => '50000',
                    'additional_fee_plan_id' => 'garden_add',
                    'frequency_hours' => '3',
                    'frequency_days' => '0',
                    'features' => [
                        'Modules Covered' => 'Incidents and Assets',
                        'Historical Tickets Loading Limit' => '50,000',
                        'Monthly incremental tickets limit' => '10,000',
                        'Data refresh frequency' => '8 times daily',
                        'Permission to Share Dashboards' => 'Unrestricted Sharing',
                    ],
                ],
                'garden_plus' => [
                    'name' => 'Garden Plus',
                    'stripe_id' => 'garden_plus',
                    'plan_amount' => '199',
                    'annual_stripe_id' => 'annual_garden_plus',
                    'annual_plan_amount' => '2189',
                    'additional_fee' => '1',
                    'allowed_ticket_count' => '15000',
                    'allowed_history_ticket_count' => '70000',
                    'additional_fee_plan_id' => 'garden_plus_add',
                    'frequency_hours' => '1',
                    'frequency_days' => '0',
                    'features' => [
                        'Modules Covered' => 'Incidents and Assets',
                        'Historical Tickets Loading Limit' => '70,000',
                        'Monthly incremental tickets limit' => '15,000',
                        'Data refresh frequency' => 'Hourly',
                        'Permission to Share Dashboards' => 'Unrestricted Sharing',
                    ],
                ],
            ]
        ],
    ]
];
