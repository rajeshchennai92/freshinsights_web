<?php

namespace App\Http\Controllers\User;

use App\CashierMultiplan\Subscription;
use App\Events\UserSubscribed;
use App\Mail\SubscriptionCancelled;
use App\Mail\SubscriptionUpgraded;
use App\Services\UserService;
use Carbon\Carbon;
use function Composer\Autoload\includeFile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\BillingRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use function PHPSTORM_META\type;
use DB;

class BillingController extends Controller
{

    public function __construct(UserService $userService)
    {
        $this->service = $userService;
    }

    /**
     * change a credit card
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function getMyAccount(Request $request)
    {
        $user = $request->user();
        $userData = DB::table('user_meta')->where('user_id',$user->id)->first();
        session_start();
        if(!empty($userData->powerbi_email)){
            $_SESSION['subdata'] ='Power BI';
        }else {
            $_SESSION['subdata'] = 'Tableau';
        }
        $allSubscription = $user->meta->subscriptions;
        $fdSubscription = $user->meta->subscription(config('plans.subscriptions.freshdesk.subscription_name'));
        $fsSubscription = $user->meta->subscription(config('plans.subscriptions.freshservice.subscription_name'));
        $fd_remaining_day = 0;
        $fs_remaining_day = 0;
        if (isset($fdSubscription) && $fdSubscription->plan_name == 'free'){
            $end_date = new Carbon($fdSubscription->ends_at);
            $now = Carbon::now();
            $fd_remaining_day = $end_date->diffForHumans($now);
            $fd_remaining_day = str_replace("after","",$fd_remaining_day);
        }
        if (isset($fsSubscription) && $fsSubscription->plan_name == 'free'){
            $end_date = new Carbon($fsSubscription->ends_at);
            $now = Carbon::now();
            $fs_remaining_day = $end_date->diffForHumans($now);

            $fs_remaining_day = str_replace("after","",$fs_remaining_day);
        }
        if ($user){
            return view('billing.my-account')->with([
                'user' => $user,
                'allSubscription' => $allSubscription,
                'fdSubscription' => $fdSubscription,
                'fsSubscription' => $fsSubscription,
                'fd_remaining_day' => $fd_remaining_day,
                'fs_remaining_day' => $fs_remaining_day,
                ]);
        }
        return redirect('login');
    }

    /**
     * change a credit card
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function getChangeCard(Request $request)
    {
        $user = $request->user();

        return view('billing.change-card')
            ->with('user', $user);
    }

    /**
     * Save new credit card
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function postChangeCard(BillingRequest $request)
    {
        $user = $request->user();
        if ($user){
            try {
                $payload = $request->all();
                $creditCardToken = $payload['stripeToken'];
                auth()->user()->meta->updateCard($creditCardToken);
                return response()->json([
                    'response' => 'success',
                    'details' => 1
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'response' => 'error',
                    'details' => $e->getMessage()
                ]);
            }
        }
        return response()->json([
            'response' => 'error',
            'details' => 'Could not complete the request, please try again.'
        ]);
    }

    /**
     * Add a coupon
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function getCoupon(Request $request)
    {
        $user = $request->user();

        return view('billing.coupon')
            ->with('user', $user);
    }

    /**
     * Use a coupon
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function postCoupon(BillingRequest $request)
    {
        try {
            $payload = $request->all();
            auth()->user()->meta->applyCoupon($payload['coupon']);
            return redirect('user/billing/details')->with('message', 'Your coupon was used!');
        } catch (Exception $e) {
            throw new Exception("Could not process the coupon please try again.", 1);
        }

        return back()->withErrors(['Could not add your coupon, please try again.']);
    }

    /**
     * Get invoices
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function getInvoices(Request $request)
    {
        $user = $request->user();
        $invoices = $user->meta->invoices();
        return view('billing.invoices')
            ->with('invoices', $invoices)
            ->with('user', $user);
    }

    /**
     * Get one invoice
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function getInvoiceById($id, Request $request)
    {
        try {
            $user = $request->user();
            $response = $user->meta->downloadInvoice($id, [
                'company'    => config("invoice.company"),
                'street'    => config("invoice.street"),
                'location'  => config("invoice.location"),
                'country'     => config("invoice.country"),
                'url'       => config("invoice.url"),
                'product'       => 'FreshInsights',
                'user_name'       => $user->name,
            ]);
        } catch (Exception $e) {
            $response = back()->withErrors(['Could not find this invoice, please try again.']);
        }

        return $response;
    }

    /**
     * Cancel Subscription
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function cancelSubscription(Request $request, $id)
    {
        try {
            $user = $request->user();
            $current_user = $this->service->find($id);
            $invoice = $current_user->meta->upcomingInvoice();
            $date = Carbon::createFromTimestamp($invoice->date);
            $current_user->meta->subscription(config('plans.subscriptions.freshdesk.subscription_name'))->cancel();
            Mail::to($current_user)
                ->cc(env('FRESHDESK_SUPPORT_EMAIL'))
                ->send(new SubscriptionCancelled());
            return redirect()->back()->with('message', 'The subscription has been cancelled! It will be availale until '.$date);
        } catch (Exception $e) {
            throw new Exception("Could not process the cancellation please try again.", 1);
        }

        return back()->withErrors(['Could not cancel billing, please try again.']);
    }
}
