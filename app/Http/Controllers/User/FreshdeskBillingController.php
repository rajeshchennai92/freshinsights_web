<?php

namespace App\Http\Controllers\User;

use App\CashierMultiplan\Subscription;
use App\Events\UserSubscribed;
use App\Mail\SubscriptionCancelled;
use App\Mail\SubscriptionUpgraded;
use App\Services\UserService;
use Carbon\Carbon;
use function Composer\Autoload\includeFile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\BillingRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use function PHPSTORM_META\type;

class FreshdeskBillingController extends Controller
{

    public function __construct(UserService $userService)
    {
        $this->service = $userService;
    }

    /**
     * Get the Freshdesk subscription ticket count based on the customer
     *
     * @return \Illuminate\Http\Response
     */
    public function getFreshdeskTicketCount(Request $request)
    {
        $user = $request->user();
        $freshdeskClient = $user->freshdeskClients;
        if ($freshdeskClient){
            $date = $request->get('date')?date('Y-m-d', strtotime($request->get('date'))):date('Y-m-d', strtotime("-1000 days"));
            $avgTicketquery = "(status:2%20OR%20status:5%20OR%20status:3%20OR%20status:4)%20AND%20(created_at:>%27" . $date . "%27)";
            $freshdesk_avg_ticket_count_url = $freshdeskClient->organization_url . '/api/v2/search/tickets?query="' . $avgTicketquery . '"';
            $avgTicketCount = $this->get_freshdesk_data($freshdesk_avg_ticket_count_url, $freshdeskClient->organization_api_key);
            if ($avgTicketCount === 'fail'){
                return response()->json([
                    'response' => 'error',
                    'details' => 0
                ]);
            }
            $responseArr = array(
                'response' => 'success',
                'details' => 1
            );
            $responseArr['avgTicketCount'] = $avgTicketCount;
            foreach (config('plans.subscriptions.freshdesk.plans') as $key => $plan){
                $setup_fee = config('plans.subscriptions.freshdesk.setup_fee');
                $allowed_history_ticket_count = $plan['allowed_history_ticket_count'];
                $additional_setup_fee = config('plans.subscriptions.freshdesk.additional_setup_fee');
                if ($avgTicketCount>$allowed_history_ticket_count){
                    $setup_fee = $setup_fee + (round(($avgTicketCount-$allowed_history_ticket_count)/$allowed_history_ticket_count)*$additional_setup_fee);
                    $responseArr['setup_fee']['setup_fee_calc_'.$plan['plan_amount']] = $setup_fee;
                    $responseArr['setup_fee']['setup_fee_calc_'.$plan['annual_plan_amount']] = $setup_fee;
                }
                else {
                    $responseArr['setup_fee']['setup_fee_calc_'.$plan['plan_amount']] = $setup_fee;
                    $responseArr['setup_fee']['setup_fee_calc_'.$plan['annual_plan_amount']] = $setup_fee;
                }
            }
            return response()->json($responseArr);

        }
        else{
            return response()->json([
                'response' => 'success',
                'details' => 0
            ]);
        }
    }

    /**
     * Billing selection
     *
     * @return \Illuminate\Http\Response
     */
    public function getFreshdeskSubscribe(Request $request)
    {
        $user = $request->user();
        if ($user){
            $fdSubscription = $user->meta->subscription(config('plans.subscriptions.freshdesk.subscription_name'));
            return view('billing.freshdesk.subscribe')->with('fdSubscription',$fdSubscription);
        }
        return view('billing.freshdesk.subscribe')->with('fdSubscription',null);
    }

    /**
     * Billing selection
     *
     * @return \Illuminate\Http\Response
     */
    public function getFreshdeskTableauSubscribe(Request $request)
    {
        $user = $request->user();
        if ($user){
            $fdSubscription = $user->meta->subscription(config('plans.subscriptions.freshdesk.subscription_name'));

            return view('billing.freshdesk.subscribetableau')->with('fdSubscription',$fdSubscription);
        }
        return view('billing.freshdesk.subscribetableau')->with('fdSubscription',null);
    }


    /**
     * Billing selection
     *
     * @return \Illuminate\Http\Response
     */
    public function freshdeskSubscribeChoose(Request $request)
    {
        $user = $request->user();
        if ($user){
            $freshdeskClient = $user->freshdeskClients;
            $freeSubDetails = $user->meta->subscriptions()->where('stripe_plan','free')->where('name','freshdesk')->get();
            if (!$freshdeskClient) {
               // return redirect('register/freshdesk');
                return view('billing.freshdesk.subscribe');

            }
            if ($user->meta->subscribed(config('plans.subscriptions.freshdesk.subscription_name')) && !count($freeSubDetails)>0) {
                return redirect('user/myaccount');
            }
        }
        session_start();
        if($_SESSION['subdata'] == 'Tableau'){
            return view('billing.freshdesk-tableau.choose')->with('freeSubDetails', $freeSubDetails);

        }else {
            return view('billing.freshdesk.choose')->with('freeSubDetails', $freeSubDetails);
        }
    }


    /**
     * Billing selection
     *
     * @return \Illuminate\Http\Response
     */
    public function freshdeskTableauSubscribeChoose(Request $request)
    {
        $user = $request->user();
        if ($user){
            $freshdeskClient = $user->freshdeskClients;
            $freeSubDetails = $user->meta->subscriptions()->where('stripe_plan','free')->where('name','freshdesk')->get();
            if (!$freshdeskClient) {
                return view('billing.freshdesk.subscribetableau');

                //return redirect('register/freshdesk');
            }
            if ($user->meta->subscribed(config('plans.subscriptions.freshdesk.subscription_name')) && !count($freeSubDetails)>0) {
                return redirect('user/myaccount');
            }
        }
        return view('billing.freshdesk-tableau.choose')->with('freeSubDetails',$freeSubDetails);
    }

    /**
     * Billing selection
     *
     * @return \Illuminate\Http\Response
     */
    public function freshdeskSelectedPlan(Request $request)
    {
        $data = $request->all();
        Session::put('plan', $data);
        return response()->json([
            'response' => 'success',
            'url' => route('freshdeskSubscriptionConfirmation')
        ]);
    }

    /**
     * Billing selection
     *
     * @return \Illuminate\Http\Response
     */
    public function freshdeskTableauSelectedPlan(Request $request)
    {
        $data = $request->all();
        Session::put('plan', $data);
        return response()->json([
            'response' => 'success',
            'url' => route('freshdeskTableauSubscriptionConfirmation')
        ]);
    }

    /**
     * Billing selection
     *
     * @return \Illuminate\Http\Response
     */
    public function freshdeskSubscribeConfirm(Request $request)
    {
        $user = $request->user();
        if ($user){
            $freshdeskClient = $user->freshdeskClients;
            $freeSubDetails = $user->meta->subscriptions()->where('stripe_plan','free')->where('name','freshdesk')->get();
            if (!$freshdeskClient) {
                return redirect('register/freshdesk');
            }
            if ($user->meta->subscribed(config('plans.subscriptions.freshdesk.subscription_name')) && !$freeSubDetails) {
                return redirect('user/myaccount');
            }
        }
        if(!Session::has('plan'))
        {
            return redirect()->route('freshdeskSubscriptionChoose');
        }
        $data = Session::get('plan');
        Session::forget('plan');
        return view('billing.freshdesk.confirm')->with('data',$data);
    }


    /**
     * Billing selection
     *
     * @return \Illuminate\Http\Response
     */
    public function freshdeskTableauSubscribeConfirm(Request $request)
    {
        $user = $request->user();
        if ($user){
            $freshdeskClient = $user->freshdeskClients;
            $freeSubDetails = $user->meta->subscriptions()->where('stripe_plan','free')->where('name','freshdesk')->get();
            if (!$freshdeskClient) {
                return redirect('register/freshdesk');
            }
            if ($user->meta->subscribed(config('plans.subscriptions.freshdesk.subscription_name')) && !$freeSubDetails) {
                return redirect('user/myaccount');
            }
        }
        if(!Session::has('plan'))
        {
            return redirect()->route('freshdeskTableauSubscribeChoose');
        }
        $data = Session::get('plan');
        Session::forget('plan');
        return view('billing.freshdesk-tableau.confirm')->with('data',$data);
    }

    /**
     * Billing selection
     *
     * @return \Illuminate\Http\Response
     */
    public function freshdeskSubscribeFree(Request $request)
    {
        try {
            $payload = $request->all();
            $user = $request->user();
            $freshdeskClient = $user->freshdeskClients;
            if ($freshdeskClient){
                $plan = $payload['plan'];
                if ($user->meta->subscribed(config('plans.subscriptions.freshdesk.subscription_name'))){
                    return response()->json([
                        'response' => 'error',
                        'details' => 'Already subscribed to this subscription'
                    ]);
                }
                $freeSubDetails = $user->meta->subscriptions()->where('stripe_plan','free')->where('name','freshdesk')->get();
                if (count($freeSubDetails)>0){
                    return response()->json([
                        'response' => 'error',
                        'details' => 'Your have already used your free trial'
                    ]);
                }

                $subscription = new Subscription();
                $subscription->user_meta_id = $user->meta->id;
                $subscription->name = 'freshdesk';
                $subscription->stripe_id = 'free';
                $subscription->stripe_plan = 'free';
                $subscription->plan_name = 'free';
                $subscription->plan_interval = $payload['plan_interval'];
                $subscription->quantity = 0;
                $subscription->trial_ends_at = Carbon::now()->addDays(21);
                $subscription->ends_at = Carbon::now()->addDays(21);
                $subscription->save();

                $freshdeskClient->updated_on = Carbon::now()->subDays(1000);
                $freshdeskClient->status = true;
                $freshdeskClient->cron = 3;
                $freshdeskClient->save();

                event(new UserSubscribed($user, 'Free','Freshdesk'));

                return response()->json([
                    'response' => 'success',
                    'details' => 1,
                    'url' => route('userAccount')
                ]);
            }
            else{
                return response()->json([
                    'response' => 'error',
                    'details' => 'You have to register for Freshdesk product before subscribe.'
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'response' => 'error',
                'details' => $e->getMessage()
            ]);
        }
    }

    /**
     * Create a subscription
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function freshdeskSubscribe(BillingRequest $request)
    {
        try {
            $payload = $request->all();
            $user = $request->user();
            $freshdeskClient = $user->freshdeskClients;
            if ($freshdeskClient){
                $plan = $payload['plan'];
                $fdSubscription = $user->meta->subscription(config('plans.subscriptions.freshdesk.subscription_name'));
                if ($fdSubscription){
                    if ($user->meta->subscribed(config('plans.subscriptions.freshdesk.subscription_name')) && $fdSubscription->plan_name != 'free'){
                        return response()->json([
                            'response' => 'error',
                            'details' => 'Already subscribed to this subscription'
                        ]);
                    }else{
                        $fdSubscription->ends_at = Carbon::create();
                        $fdSubscription->trial_ends_at = Carbon::create();
                        $fdSubscription->save();
                    }
                }
                if (isset($user->meta->stripe_id) && $user->meta->stripe_id != null){
                    $plan = $payload['plan'];
                    $date = isset($payload['date'])?date('Y-m-d', strtotime($payload['date'])):date('Y-m-d', strtotime("-1000 days"));
                    $setup_fee = config('plans.subscriptions.freshdesk.setup_fee');
                    $allowed_history_ticket_count = config('plans.subscriptions.freshdesk.plans')[$plan]['allowed_history_ticket_count'];
                    $additional_setup_fee = config('plans.subscriptions.freshdesk.additional_setup_fee');
                    $avgTicketquery = "(status:2%20OR%20status:5%20OR%20status:3%20OR%20status:4)%20AND%20(created_at:>%27" . $date . "%27)";
                    $freshdesk_avg_ticket_count_url = $freshdeskClient->organization_url . '/api/v2/search/tickets?query="' . $avgTicketquery . '"';
                    $avgTicketCount = $this->get_freshdesk_data($freshdesk_avg_ticket_count_url, $freshdeskClient->organization_api_key);

                    if ($avgTicketCount === 'fail'){
                        return response()->json([
                            'response' => 'failure',
                            'details' => 0
                        ]);
                    }
                    if ($avgTicketCount>$allowed_history_ticket_count){
                        $setup_fee = $setup_fee + (round(($avgTicketCount-$allowed_history_ticket_count)/$allowed_history_ticket_count)*$additional_setup_fee);
                    }

                    if ($setup_fee > 0){
                        $user->meta->invoiceFor('One Time Fee', $setup_fee*100);
                    }
                    $user->meta->newMultisubscription(config('plans.subscriptions.freshdesk.subscription_name'), $payload['stripe_plan'], $plan, $payload['plan_interval'],1)
                        ->createSubscription();
                    $user->meta->newMultisubscription(config('plans.subscriptions.freshdesk.subscription_name'), config('plans.subscriptions.freshdesk.plans')[$plan]['additional_fee_plan_id'], $plan)
                        ->createSubscription();

                    $freshdeskClient->updated_on = $date;
                    $freshdeskClient->status = true;
                    $freshdeskClient->frequency_hours = config('plans.subscriptions.freshdesk.plans')[$plan]['frequency_hours'];
                    $freshdeskClient->frequency_days = config('plans.subscriptions.freshdesk.plans')[$plan]['frequency_days'];
                    $freshdeskClient->save();

                    event(new UserSubscribed($user, config('plans.subscriptions.freshdesk.plans')[$plan]['name'],'Freshdesk'));
                    system(env('FRESHDESK_PAID_APP_PATH'));
                    return response()->json([
                        'response' => 'success',
                        'details' => 1,
                        'url' => route('userAccount')
                    ]);
                }
                else{
                    if (isset($payload['stripeToken']) && !empty($payload['stripeToken'])){
                        $plan = $payload['plan'];
                        $date = isset($payload['date'])?date('Y-m-d', strtotime($payload['date'])):date('Y-m-d', strtotime("-1000 days"));
                        $setup_fee = config('plans.subscriptions.freshdesk.setup_fee');
                        $allowed_history_ticket_count = config('plans.subscriptions.freshdesk.plans')[$plan]['allowed_history_ticket_count'];
                        $additional_setup_fee = config('plans.subscriptions.freshdesk.additional_setup_fee');
                        $avgTicketquery = "(status:2%20OR%20status:5%20OR%20status:3%20OR%20status:4)%20AND%20(created_at:>%27" . $date . "%27)";
                        $freshdesk_avg_ticket_count_url = $freshdeskClient->organization_url . '/api/v2/search/tickets?query="' . $avgTicketquery . '"';
                        $avgTicketCount = $this->get_freshdesk_data($freshdesk_avg_ticket_count_url, $freshdeskClient->organization_api_key);

                        if ($avgTicketCount === 'fail'){
                            return response()->json([
                                'response' => 'failure',
                                'details' => 0
                            ]);
                        }
                        if ($avgTicketCount>$allowed_history_ticket_count){
                            $setup_fee = $setup_fee + (round(($avgTicketCount-$allowed_history_ticket_count)/1000)*$additional_setup_fee);
                        }

                        $user->meta->newMultisubscription(config('plans.subscriptions.freshdesk.subscription_name'), $payload['stripe_plan'], $plan, $payload['plan_interval'],1)
                            ->createSubscription($payload['stripeToken']);
                        $user->meta->newMultisubscription(config('plans.subscriptions.freshdesk.subscription_name'), config('plans.subscriptions.freshdesk.plans')[$plan]['additional_fee_plan_id'], $plan)
                            ->createSubscription();
                        if ($setup_fee > 0){
                            $user->meta->invoiceFor('One Time Fee', $setup_fee*100);
                        }

                        $freshdeskClient->updated_on = $date;
                        $freshdeskClient->status = true;
                        $freshdeskClient->frequency_hours = config('plans.subscriptions.freshdesk.plans')[$plan]['frequency_hours'];
                        $freshdeskClient->frequency_days = config('plans.subscriptions.freshdesk.plans')[$plan]['frequency_days'];
                        $freshdeskClient->save();

                        event(new UserSubscribed($user, config('plans.subscriptions.freshdesk.plans')[$plan]['name'],'Freshdesk'));
                        system(env('FRESHDESK_PAID_APP_PATH'));
                        return response()->json([
                            'response' => 'success',
                            'details' => 1,
                            'url' => route('userAccount')
                        ]);
                    }
                    else{
                        return response()->json([
                            'response' => 'success',
                            'details' => 0
                        ]);
                    }
                }
            }
            else{
                return response()->json([
                    'response' => 'error',
                    'details' => 'You have to register for Freshdesk product before subscribe.'
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'response' => 'error',
                'details' => $e->getMessage()
            ]);
        }
        return response()->json([
            'response' => 'error',
            'details' => 'Could not complete subscription, please try again.'
        ]);
    }

    /**
     * Cancel Subscription for user
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function cancelFDSubscriptionForUser(Request $request)
    {
        try {
            $user = $request->user();
            $freshdeskClient = $user->freshdeskClients;
            $fdSubscription = $user->meta->subscription(config('plans.subscriptions.freshdesk.subscription_name'));
            $date = Carbon::now();
            if ($fdSubscription->plan_name == 'free'){
                $fdSubscription->ends_at = $date;
                $fdSubscription->trial_ends_at = $date;
                $fdSubscription->save();
                Mail::to($user)
                    ->cc(env('FRESHDESK_SUPPORT_EMAIL'))
                    ->send(new SubscriptionCancelled());
                return redirect()->back()->with('message', 'The subscription has been cancelled!');
            }
            $subscriptions = $user->meta->getAllSubscription(config('plans.subscriptions.freshdesk.subscription_name'));
            if (count($subscriptions) != 2){
                return redirect()->back()->with('message', 'Something went wrong please try again later!');
            }
            foreach ($subscriptions as $subscription){
                $stripesubscription = $subscription->asStripeSubscription();
                $time = new \DateTime('now');
                if ($stripesubscription['plan']['usage_type'] == 'metered'){
                    $date = date('Y-m-d', strtotime('-1 day',$stripesubscription->current_period_start));
                    Config::set('database.connections.tenant.database', $freshdeskClient->organization_db_name);
                    try {
                        $avgTicketCount = DB::connection('tenant')->select('select count(ticket_id) as Ticket_Count from tickets where active=1 and created_at>= :date',['date' => $date])[0]->Ticket_Count;
                        DB::purge('tenant');
                        if (round($avgTicketCount/100)>0){
                            $avgMonthlyCount = config('plans.subscriptions.freshdesk.plans')[$subscription->plan_name]['allowed_ticket_count'];
                            $additionalFee = config('plans.subscriptions.freshdesk.plans')[$subscription->plan_name]['additional_fee'];
                            $charge_amount = $subscription->getMeteredBillingAmount(round($avgTicketCount/100),$avgMonthlyCount/100,$additionalFee, $time);
                            if ($charge_amount > 0){
                                $user->meta->invoiceFor('Charge for ticket count usage from '.$date, $charge_amount*100);
                            }
                        }
                    }
                    catch (\Exception $e) {

                    }
                }else{
                    $invoice = $user->meta->invoices()->first(function ($invoice) use ($subscription) {
                        return $subscription->stripe_id === $invoice->subscription;
                    });
                    $refund = $subscription->prorate($subscription, $time,$invoice->total);
                    $amount['amount'] = $refund;
                    $user->meta->refund($invoice->charge,$amount);
                }
                $subscription->cancelNow();
            }
            $freshdeskClient->status = false;
            $freshdeskClient->cron = 0;
            $freshdeskClient->save();
            Mail::to($user)
                ->cc(env('FRESHDESK_SUPPORT_EMAIL'))
                ->send(new SubscriptionCancelled());
            return redirect()->back()->with('message', 'The subscription has been cancelled! It will be availale until '.$date);
        } catch (Exception $e) {
            throw new Exception("Could not process the cancellation please try again.", 1);
        }

        return back()->withErrors(['Could not cancel billing, please try again.']);
    }

    /**
     * Enable Free Subscription
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function enableFreeSubscription(Request $request, $id)
    {
        try {
            $user = $request->user();
            $date = $request->get('fddatepicker');
            $date = Carbon::createFromTimestamp(strtotime($date));
            $current_user = $this->service->find($id);
            $freshdeskClient = $current_user->freshdeskClients;
            $fdSubscription = $current_user->meta->subscription(config('plans.subscriptions.freshdesk.subscription_name'));
            if ($fdSubscription->plan_name == 'free'){
                $fdSubscription->trial_ends_at = $date;
                $fdSubscription->ends_at = $date;
                $fdSubscription->save();
            }
            $freshdeskClient->status = true;
            $freshdeskClient->cron = 3;
            $freshdeskClient->save();
            return redirect()->back()->with('message', 'The subscription has been enabled! It will be availale until '.$date);
        } catch (Exception $e) {
            throw new Exception("Could not process the cancellation please try again.", 1);
        }

        return back()->withErrors(['Could not cancel billing, please try again.']);
    }

    /**
     * Cancel Free Subscription
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function cancelFreeSubscription(Request $request, $id)
    {
        try {
            $user = $request->user();
            $current_user = $this->service->find($id);
            $freshdeskClient = $current_user->freshdeskClients;
            $date = Carbon::now();
            $fdSubscription = $current_user->meta->subscription(config('plans.subscriptions.freshdesk.subscription_name'));
            if ($fdSubscription->plan_name == 'free'){
                $fdSubscription->ends_at = $date;
                $fdSubscription->trial_ends_at = $date;
                $fdSubscription->save();
            }
            $freshdeskClient->status = false;
            $freshdeskClient->cron = 0;
            $freshdeskClient->save();
            return redirect()->back()->with('message', 'The subscription has been cancelled!');
        } catch (Exception $e) {
            throw new Exception("Could not process the cancellation please try again.", 1);
        }

        return back()->withErrors(['Could not cancel billing, please try again.']);
    }

    /**
     * Modify Subscription
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function freshdeskModifyPlan(Request $request)
    {
        try {
            $plan = $request->get('plan')?$request->get('plan'):'';
            $user = $request->user();
            $fdSubscription = $user->meta->subscription(config('plans.subscriptions.freshdesk.subscription_name'));
            if ($fdSubscription || $plan){
                if ($fdSubscription->plan_name == 'free'){
                    return response()->json([
                        'response' => 'success',
                        'details' => 0
                    ]);
                }
                $freshdeskClient = $user->freshdeskClients;

                $plans = config('plans.subscriptions.freshdesk.plans');
                $lastPlan = end($plans);
                $lastPlanKey = key($plans);
                if ($lastPlanKey == $fdSubscription->plan_name){
                    return response()->json([
                        'response' => 'error',
                        'details' => 'Could not complete the request, please try again.'
                    ]);
                }
                else{
                    $key = array_search($fdSubscription->plan_name, array_keys($plans), true);
                    $change_plan_arr = array_slice($plans, $key+1, null, true);
                }
                if (array_key_exists($plan,$change_plan_arr))
                {
                    $stripesubscription = $fdSubscription->asStripeSubscription();

                    $date = date('Y-m-d', $stripesubscription->current_period_start);
                    $avgTicketquery = "(status:2%20OR%20status:5%20OR%20status:3%20OR%20status:4)%20AND%20(created_at:>%27" . $date . "%27)";
                    $freshdesk_avg_ticket_count_url = $freshdeskClient->organization_url . '/api/v2/search/tickets?query="' . $avgTicketquery . '"';
                    $avgTicketCount = $this->get_freshdesk_data($freshdesk_avg_ticket_count_url, $freshdeskClient->organization_api_key);

                    if ($avgTicketCount === 'fail'){
                        return response()->json([
                            'response' => 'error',
                            'details' => 'Something went wrong. Please try again later'
                        ]);
                    }

                    $addtional_sub_item = $fdSubscription->subscriptionItems()->where('stripe_plan', config('plans.subscriptions.freshdesk.plans')[$fdSubscription->plan_name]['additional_fee_plan_id'])->first();

                    if ($avgTicketCount>0){
                        $updateResponse = $addtional_sub_item->updateMeteredUsage(round($avgTicketCount/100));
                        if (!$updateResponse){
                            return response()->json([
                                'response' => 'error',
                                'details' => 'Something went wrong. Please try again later'
                            ]);
                        }
                    }

                    $fdSubscription->swapPlan($plan, config('plans.subscriptions.freshdesk.plans')[$plan]['stripe_id'], config('plans.subscriptions.freshdesk.plans')[$plan]['additional_fee_plan_id']);
                    $freshdeskClient->frequency_hours = config('plans.subscriptions.freshdesk.plans')[$plan]['frequency_hours'];
                    $freshdeskClient->frequency_days = config('plans.subscriptions.freshdesk.plans')[$plan]['frequency_days'];
                    $freshdeskClient->save();
                    Mail::to($user)
                        ->cc(env('FRESHDESK_SUPPORT_EMAIL'))
                        ->send(new SubscriptionUpgraded());
                    return response()->json([
                        'response' => 'success',
                        'details' => 1,
                        'plan_name' => config('plans.subscriptions.freshdesk.plans')[$plan]['name']
                    ]);
                }
                else
                {
                    return response()->json([
                        'response' => 'error',
                        'details' => 'Could not complete the request, please try again.'
                    ]);
                }
            }
        else{
                return response()->json([
                    'response' => 'error',
                    'details' => 'Could not complete the request, please try again.'
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'response' => 'error',
                'details' => $e->getMessage()
            ]);
        }

    }

    /**
     * Modify Free Subscription
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function freshdeskModifyFreePlan(Request $request)
    {
        try {
            $payload = $request->all();
            $plan = $request->get('plan')?$request->get('plan'):'';
            $user = $request->user();
            $fdSubscription = $user->meta->subscription(config('plans.subscriptions.freshdesk.subscription_name'));
            if ($fdSubscription || $plan) {
                if ($fdSubscription->plan_name == 'free' && $plan != 'free') {
                    $freshdeskClient = $user->freshdeskClients;
                    if (isset($payload['stripeToken']) && !empty($payload['stripeToken'])){
                        $date = isset($payload['date'])?date('Y-m-d', strtotime($payload['date'])):date('Y-m-d', strtotime("-1000 days"));
                        $setup_fee = config('plans.subscriptions.freshdesk.setup_fee');
                        $allowed_history_ticket_count = config('plans.subscriptions.freshdesk.plans')[$plan]['allowed_history_ticket_count'];
                        $additional_setup_fee = config('plans.subscriptions.freshdesk.additional_setup_fee');
                        $avgTicketquery = "(status:2%20OR%20status:5%20OR%20status:3%20OR%20status:4)%20AND%20(created_at:>%27" . $date . "%27)";
                        $freshdesk_avg_ticket_count_url = $freshdeskClient->organization_url . '/api/v2/search/tickets?query="' . $avgTicketquery . '"';
                        $avgTicketCount = $this->get_freshdesk_data($freshdesk_avg_ticket_count_url, $freshdeskClient->organization_api_key);

                        if ($avgTicketCount === 'fail'){
                            return response()->json([
                                'response' => 'failure',
                                'details' => 0
                            ]);
                        }
                        if ($avgTicketCount>$allowed_history_ticket_count){
                            $setup_fee = $setup_fee + (round(($avgTicketCount-$allowed_history_ticket_count)/1000)*$additional_setup_fee);
                        }

                        $user->meta->newMultisubscription(config('plans.subscriptions.freshdesk.subscription_name'), $plan)
                            ->addPlan(config('plans.subscriptions.freshdesk.plans')[$plan]['stripe_id'])
                            ->addPlan(config('plans.subscriptions.freshdesk.plans')[$plan]['additional_fee_plan_id'],null)
                            ->create($payload['stripeToken']);
                        if ($setup_fee > 0){
                            $user->meta->invoiceFor('One Time Fee', $setup_fee*100);
                        }

                        $freshdeskClient->cron = 0;
                        $freshdeskClient->status = true;
                        $freshdeskClient->updated_on = $date;
                        $freshdeskClient->frequency_hours = config('plans.subscriptions.freshdesk.plans')[$plan]['frequency_hours'];
                        $freshdeskClient->frequency_days = config('plans.subscriptions.freshdesk.plans')[$plan]['frequency_days'];
                        $freshdeskClient->save();

                        $fdSubscription->ends_at = Carbon::create();
                        $fdSubscription->save();

                        Mail::to($user)
                            ->cc(env('FRESHDESK_SUPPORT_EMAIL'))
                            ->send(new SubscriptionUpgraded());
                        system(env('FRESHDESK_PAID_APP_PATH'));
                        return response()->json([
                            'response' => 'success',
                            'details' => 1,
                            'plan_name' => config('plans.subscriptions.freshdesk.plans')[$plan]['name']
                        ]);
                    }
                    else{
                        return response()->json([
                            'response' => 'error',
                            'details' => 'Something went wrong. Please try again laterr'
                        ]);
                    }
                }
            }

        } catch (\Exception $e) {
            return response()->json([
                'response' => 'error',
                'details' => $e->getMessage()
            ]);
        }

    }

    protected function get_freshdesk_data($freshdesk_url, $api_key) {
//        echo $freshdesk_url;
        $chf = curl_init($freshdesk_url);
        curl_setopt($chf, CURLOPT_HEADER, true);
        curl_setopt($chf, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Connection: Keep-Alive'
        ));
        curl_setopt($chf, CURLOPT_USERPWD, "$api_key");
        curl_setopt($chf, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chf, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($chf, CURLOPT_VERBOSE, 1);
        $server_output = curl_exec($chf);
        $info = curl_getinfo($chf);
        $header_size = curl_getinfo($chf, CURLINFO_HEADER_SIZE);
        $headers = substr($server_output, 0, $header_size);
        $response = substr($server_output, $header_size);
        $header_exp = explode('X-RateLimit', $headers);
        if ($info['http_code'] == 200) {
            $response_array = json_decode($response, true);
            $response = $response_array['total'];
            return $response;
        } else {
            return 'fail';
        }
        curl_close($chf);
    }
}
