<?php

namespace App\Http\Controllers\User;

use App\CashierMultiplan\Subscription;
use App\Events\UserSubscribed;
use App\Mail\SubscriptionCancelled;
use App\Mail\SubscriptionUpgraded;
use App\Services\UserService;
use Carbon\Carbon;
use function Composer\Autoload\includeFile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\BillingRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use function PHPSTORM_META\type;

class FreshserviceBillingController extends Controller
{

    public function __construct(UserService $userService)
    {
        $this->service = $userService;
    }

    /**
     * Billing selection
     *
     * @return \Illuminate\Http\Response
     */
    public function getFreshserviceSubscribe(Request $request)
    {
        $user = $request->user();
        if ($user){
            $fsSubscription = $user->meta->subscription(config('plans.subscriptions.freshservice.subscription_name'));
            return view('billing.freshservice.subscribe')->with('fsSubscription',$fsSubscription);
        }
        return view('billing.freshservice.subscribe')->with('fsSubscription',null);
    }


     /**
     * Billing selection
     *
     * @return \Illuminate\Http\Response
     */
    public function getFreshserviceTableauSubscribe(Request $request)
    {
        $user = $request->user();
        if ($user){
            $fsSubscription = $user->meta->subscription(config('plans.subscriptions.freshservice.subscription_name'));
            return view('billing.freshservice.subscribetableau')->with('fsSubscription',$fsSubscription);
        }
        return view('billing.freshservice.subscribetableau')->with('fsSubscription',null);
    }

    /**
     * Freshservice subscription choose
     *
     * @return \Illuminate\Http\Response
     */
    public function freshserviceSubscribeChoose(Request $request)
    {
        $user = $request->user();
        if ($user){
            $freshserviceClient = $user->freshserviceClients;
            $freeSubDetails = $user->meta->subscriptions()->where('stripe_plan','free')->where('name','freshservice')->get();
            if (!$freshserviceClient) {
                return view('billing.freshservice.subscribe');

                //return redirect('register/freshservice');
            }
            if ($user->meta->subscribed(config('plans.subscriptions.freshservice.subscription_name')) && !count($freeSubDetails)>0) {
                return redirect('user/myaccount');
            }
        }
        session_start();
        if($_SESSION['subdata'] == 'Tableau'){
            return view('billing.freshservice-tableau.choose')->with('freeSubDetails',$freeSubDetails);

        }else {
            return view('billing.freshservice.choose')->with('freeSubDetails',$freeSubDetails);

        }
    }



    /**
     * Freshservice subscription choose
     *
     * @return \Illuminate\Http\Response
     */
    public function freshserviceTableauSubscribeChoose(Request $request)
    {
        $user = $request->user();
        if ($user){
            $freshserviceClient = $user->freshserviceClients;
            $freeSubDetails = $user->meta->subscriptions()->where('stripe_plan','free')->where('name','freshservice')->get();
            if (!$freshserviceClient) {
                return view('billing.freshservice.subscribetableau');

                //return redirect('register/freshservice');
            }
            if ($user->meta->subscribed(config('plans.subscriptions.freshservice.subscription_name')) && !count($freeSubDetails)>0) {
                return redirect('user/myaccount');
            }
        }
        return view('billing.freshservice-tableau.choose')->with('freeSubDetails',$freeSubDetails);
    }

    /**
     * Billing selection
     *
     * @return \Illuminate\Http\Response
     */
    public function freshserviceSelectedPlan(Request $request)
    {
        $data = $request->all();
        Session::put('plan', $data);
        return response()->json([
            'response' => 'success',
            'url' => route('freshserviceSubscriptionConfirmation')
        ]);
    }

    /**
     * Billing selection
     *
     * @return \Illuminate\Http\Response
     */
    public function freshserviceTableauSelectedPlan(Request $request)
    {
        $data = $request->all();
        Session::put('plan', $data);
        return response()->json([
            'response' => 'success',
            'url' => route('freshserviceTableauSubscriptionConfirmation')
        ]);
    }

    /**
     * Billing selection
     *
     * @return \Illuminate\Http\Response
     */
    public function freshserviceSubscribeConfirm(Request $request)
    {
        $user = $request->user();
        if ($user){
            $freshserviceClient = $user->freshserviceClients;
            $freeSubDetails = $user->meta->subscriptions()->where('stripe_plan','free')->where('name','freshservice')->get();
            if (!$freshserviceClient) {
                return view('billing.freshservice.subscribe');

                //return redirect('register/freshservice');
            }
            if ($user->meta->subscribed(config('plans.subscriptions.freshservice.subscription_name')) && !$freeSubDetails) {
                return redirect('user/myaccount');
            }
        }
        if(!Session::has('plan'))
        {
            return redirect()->route('freshserviceSubscriptionChoose');
        }
        $data = Session::get('plan');
        Session::forget('plan');
        return view('billing.freshservice.confirm')->with('data',$data);
    }

    /**
     * Billing selection
     *
     * @return \Illuminate\Http\Response
     */
    public function freshserviceTableauSubscribeConfirm(Request $request)
    {
        $user = $request->user();
        if ($user){
            $freshserviceClient = $user->freshserviceClients;
            $freeSubDetails = $user->meta->subscriptions()->where('stripe_plan','free')->where('name','freshservice')->get();
            if (!$freshserviceClient) {
                return view('billing.freshservice.subscribetableau');

                //return redirect('register/freshservice');
            }
            if ($user->meta->subscribed(config('plans.subscriptions.freshservice.subscription_name')) && !$freeSubDetails) {
                return redirect('user/myaccount');
            }
        }
        if(!Session::has('plan'))
        {
            return redirect()->route('freshserviceTableauSubscriptionChoose');
        }
        $data = Session::get('plan');
        Session::forget('plan');
        return view('billing.freshservice-tableau.confirm')->with('data',$data);
    }

    /**
     * Billing selection
     *
     * @return \Illuminate\Http\Response
     */
    public function freshserviceSubscribeFree(Request $request)
    {
        try {
            $payload = $request->all();
            $user = $request->user();
            $freshserviceClient = $user->freshserviceClients;
            if ($freshserviceClient){
                $plan = $payload['plan'];
                if ($user->meta->subscribed(config('plans.subscriptions.freshservice.subscription_name'))){
                    return response()->json([
                        'response' => 'error',
                        'details' => 'Already subscribed to this subscription'
                    ]);
                }
                $freeSubDetails = $user->meta->subscriptions()->where('stripe_plan','free')->where('name','freshservice')->get();
                if (count($freeSubDetails)>0){
                    return response()->json([
                        'response' => 'error',
                        'details' => 'Your have already used your free trial'
                    ]);
                }

                $subscription = new Subscription();
                $subscription->user_meta_id = $user->meta->id;
                $subscription->name = 'freshservice';
                $subscription->stripe_id = 'free';
                $subscription->stripe_plan = 'free';
                $subscription->plan_name = 'free';
                $subscription->plan_interval = $payload['plan_interval'];
                $subscription->quantity = 0;
                $subscription->trial_ends_at = Carbon::now()->addDays(21);
                $subscription->ends_at = Carbon::now()->addDays(21);
                $subscription->save();

                $freshserviceClient->updated_on = Carbon::now()->subDays(1000);
                $freshserviceClient->status = true;
                $freshserviceClient->cron = 3;
                $freshserviceClient->save();

                event(new UserSubscribed($user, 'Free','Freshservice'));

                return response()->json([
                    'response' => 'success',
                    'details' => 1,
                    'url' => route('userAccount')
                ]);
            }
            else{
                return response()->json([
                    'response' => 'error',
                    'details' => 'You have to register for Freshservice product before subscribe.'
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'response' => 'error',
                'details' => $e->getMessage()
            ]);
        }
    }

    /**
     * Create a subscription
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function freshserviceSubscribe(BillingRequest $request)
    {
        try {
            $payload = $request->all();
            $user = $request->user();
            $freshserviceClient = $user->freshserviceClients;
            if ($freshserviceClient){
                $plan = $payload['plan'];
                $fsSubscription = $user->meta->subscription(config('plans.subscriptions.freshservice.subscription_name'));
                if ($fsSubscription){
                    if ($user->meta->subscribed(config('plans.subscriptions.freshservice.subscription_name')) && $fsSubscription->plan_name != 'free'){
                        return response()->json([
                            'response' => 'error',
                            'details' => 'Already subscribed to this subscription'
                        ]);
                    }else{
                        $fsSubscription->ends_at = Carbon::create();
                        $fsSubscription->trial_ends_at = Carbon::create();
                        $fsSubscription->save();
                    }
                }
                if (isset($user->meta->stripe_id) && $user->meta->stripe_id != null){
                    $plan = $payload['plan'];
                    $date = isset($payload['date'])?date('Y-m-d', strtotime($payload['date'])):date('Y-m-d H:i:s', strtotime("-1000 days"));
                    $user->meta->newMultisubscription(config('plans.subscriptions.freshservice.subscription_name'), $payload['stripe_plan'], $plan, $payload['plan_interval'],1)
                        ->createSubscription();
                    $user->meta->newMultisubscription(config('plans.subscriptions.freshservice.subscription_name'), config('plans.subscriptions.freshservice.plans')[$plan]['additional_fee_plan_id'], $plan)
                        ->createSubscription();

                    $freshserviceClient->updated_on = $date;
                    $freshserviceClient->status = true;
                    $freshserviceClient->historical_payment = $payload['historical_payment']?1:0;
                    $freshserviceClient->historical_ticket_count = config('plans.subscriptions.freshservice.plans')[$plan]['allowed_history_ticket_count'];
                    $freshserviceClient->frequency_hours = config('plans.subscriptions.freshservice.plans')[$plan]['frequency_hours'];
                    $freshserviceClient->frequency_days = config('plans.subscriptions.freshservice.plans')[$plan]['frequency_days'];
                    $freshserviceClient->save();

                    event(new UserSubscribed($user, config('plans.subscriptions.freshservice.plans')[$plan]['name'],'Freshservice'));
                    system(env('FRESHSERVICE_PAID_APP_PATH'));
                    return response()->json([
                        'response' => 'success',
                        'details' => 1,
                        'url' => route('userAccount')
                    ]);
                }
                else{
                    if (isset($payload['stripeToken']) && !empty($payload['stripeToken'])){
                        $plan = $payload['plan'];
                        $date = isset($payload['date'])?date('Y-m-d', strtotime($payload['date'])):date('Y-m-d H:i:s', strtotime("-1000 days"));

                        $user->meta->newMultisubscription(config('plans.subscriptions.freshservice.subscription_name'), $payload['stripe_plan'], $plan, $payload['plan_interval'],1)
                            ->createSubscription($payload['stripeToken']);
                        $user->meta->newMultisubscription(config('plans.subscriptions.freshservice.subscription_name'), config('plans.subscriptions.freshservice.plans')[$plan]['additional_fee_plan_id'], $plan)
                            ->createSubscription();

                        $freshserviceClient->updated_on = $date;
                        $freshserviceClient->status = true;
                        $freshserviceClient->historical_payment = $payload['historical_payment']?1:0;
                        $freshserviceClient->historical_ticket_count = config('plans.subscriptions.freshservice.plans')[$plan]['allowed_history_ticket_count'];
                        $freshserviceClient->frequency_hours = config('plans.subscriptions.freshservice.plans')[$plan]['frequency_hours'];
                        $freshserviceClient->frequency_days = config('plans.subscriptions.freshservice.plans')[$plan]['frequency_days'];
                        $freshserviceClient->save();

                        event(new UserSubscribed($user, config('plans.subscriptions.freshservice.plans')[$plan]['name'],'Freshservice'));
                        system(env('FRESHSERVICE_PAID_APP_PATH'));
                        return response()->json([
                            'response' => 'success',
                            'details' => 1,
                            'url' => route('userAccount')
                        ]);
                    }
                    else{
                        return response()->json([
                            'response' => 'success',
                            'details' => 0
                        ]);
                    }
                }
            }
            else{
                return response()->json([
                    'response' => 'error',
                    'details' => 'You have to register for Freshservice product before subscribe.'
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'response' => 'error',
                'details' => $e->getMessage()
            ]);
        }
        return response()->json([
            'response' => 'error',
            'details' => 'Could not complete subscription, please try again.'
        ]);
    }


    /**
     * Enable Free Subscription
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function enableFreeFreshserviceSubscription(Request $request, $id)
    {
        try {
            $user = $request->user();
            $date = $request->get('fsdatepicker');
            $date = Carbon::createFromTimestamp(strtotime($date));
            $current_user = $this->service->find($id);
            $freshserviceClient = $current_user->freshserviceClients;
            $fsSubscription = $current_user->meta->subscription(config('plans.subscriptions.freshservice.subscription_name'));
            if ($fsSubscription->plan_name == 'free'){
                $fsSubscription->trial_ends_at = $date;
                $fsSubscription->ends_at = $date;
                $fsSubscription->save();
            }
            $freshserviceClient->status = true;
            $freshserviceClient->cron = true;
            $freshserviceClient->save();
            return redirect()->back()->with('message', 'The subscription has been enabled! It will be availale until '.$date);
        } catch (Exception $e) {
            throw new Exception("Could not process the cancellation please try again.", 1);
        }

        return back()->withErrors(['Could not cancel billing, please try again.']);
    }

    /**
     * Cancel Free Subscription
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function cancelFreeFreshserviceSubscription(Request $request, $id)
    {
        try {
            $user = $request->user();
            $current_user = $this->service->find($id);
            $freshserviceClient = $current_user->freshserviceClients;
            $date = Carbon::now();
            $fsSubscription = $current_user->meta->subscription(config('plans.subscriptions.freshservice.subscription_name'));
            if ($fsSubscription->plan_name == 'free'){
                $fsSubscription->ends_at = $date;
                $fsSubscription->trial_ends_at = $date;
                $fsSubscription->save();
            }
            $freshserviceClient->status = false;
            $freshserviceClient->cron = false;
            $freshserviceClient->save();
            return redirect()->back()->with('message', 'The subscription has been cancelled!');
        } catch (Exception $e) {
            throw new Exception("Could not process the cancellation please try again.", 1);
        }

        return back()->withErrors(['Could not cancel billing, please try again.']);
    }

    /**
     * Cancel Subscription for user
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function cancelFSSubscriptionForUser(Request $request)
    {
        try {
            $user = $request->user();
            $freshserviceClient = $user->freshserviceClients;
            $fsSubscription = $user->meta->subscription(config('plans.subscriptions.freshservice.subscription_name'));
            $date = Carbon::now();
            if ($fsSubscription->plan_name == 'free'){
                $fsSubscription->ends_at = $date;
                $fsSubscription->trial_ends_at = $date;
                $fsSubscription->save();
                Mail::to($user)
                    ->cc(env('FRESHDESK_SUPPORT_EMAIL'))
                    ->send(new SubscriptionCancelled());
                return redirect()->back()->with('message', 'The subscription has been cancelled!');
            }
            $subscriptions = $user->meta->getAllSubscription(config('plans.subscriptions.freshservice.subscription_name'));
            if (count($subscriptions) != 2){
                return redirect()->back()->with('message', 'Something went wrong please try again later!');
            }
            foreach ($subscriptions as $subscription){
                $stripesubscription = $subscription->asStripeSubscription();
                $time = new \DateTime('now');
                if ($stripesubscription['plan']['usage_type'] == 'metered'){
                    $date = date('Y-m-d', strtotime('-1 day',$stripesubscription->current_period_start));
                    Config::set('database.connections.tenant.database', $freshserviceClient->organization_db_name);
                    try{
                        $avgTicketCount = DB::connection('tenant')->select('select count(ticket_id) as Ticket_Count from tickets where active=1 and created_at>= :date',['date' => $date])[0]->Ticket_Count;
                        DB::purge('tenant');
                        if (round($avgTicketCount/100)>0){
                            $avgMonthlyCount = config('plans.subscriptions.freshservice.plans')[$subscription->plan_name]['allowed_ticket_count'];
                            $additionalFee = config('plans.subscriptions.freshservice.plans')[$subscription->plan_name]['additional_fee'];
                            $charge_amount = $subscription->getMeteredBillingAmount(round($avgTicketCount/100),$avgMonthlyCount/100,$additionalFee, $time);
                            if ($charge_amount > 0){
                                $user->meta->invoiceFor('Charge for ticket count usage from '.$date, $charge_amount*100);
                            }
                        }
                    }
                    catch (\Exception $e) {

                    }
                }else{
                    $invoice = $user->meta->invoices()->first(function ($invoice) use ($subscription) {
                        return $subscription->stripe_id === $invoice->subscription;
                    });
                    $refund = $subscription->prorate($subscription, $time,$invoice->total);
                    $amount['amount'] = $refund;
                    $user->meta->refund($invoice->charge,$amount);
                }
                $subscription->cancelNow();
            }
            $freshserviceClient->status = false;
            $freshserviceClient->cron = 0;
            $freshserviceClient->save();
            Mail::to($user)
                ->cc(env('FRESHDESK_SUPPORT_EMAIL'))
                ->send(new SubscriptionCancelled());
            return redirect()->back()->with('message', 'The subscription has been cancelled! It will be availale until '.$date);
        } catch (Exception $e) {
            throw new Exception("Could not process the cancellation please try again.", 1);
        }

        return back()->withErrors(['Could not cancel billing, please try again.']);
    }
}
