<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class PagesController extends Controller
{
    /**
     * Homepage
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        return view('welcome');
    }

    /**
     * Freshdesk Product Page
     *
     * @return \Illuminate\Http\Response
     */
    public function freshdeskPage()
    {
        return view('freshdesk');
    }

     /**
     * Freshdesk Product Page
     *
     * @return \Illuminate\Http\Response
     */
    public function freshdeskTableauPage()
    {
        return view('freshdesk-tableau');
    }

    /**
     * Freshservice Product Page
     *
     * @return \Illuminate\Http\Response
     */
    public function freshservicePage()
    {
        return view('freshservice');
    }


    /**
     * Freshservice Tableau Product Page
     *
     * @return \Illuminate\Http\Response
     */
    public function freshserviceTableauPage()
    {
        return view('freshservice-tableau');
    }

    /**
     * Other visualizations Product Page
     *
     * @return \Illuminate\Http\Response
     */
    public function otherPage()
    {
        return view('other');
    }

        /**
     * Freshdesk Tableau Product Page
     *
     * @return \Illuminate\Http\Response
     */
    public function freshsdeskTableauPage()
    {
        return view('freshdesk-tableau');
    }

    /**
     * Contact
     *
     * @return \Illuminate\Http\Response
     */
    public function contact(Request $request)
    {
        try{
            $data = json_decode($request->get('data'), true);
            $email = $data[1]['value'];
            $name = $data[0]['value'];
            $subject = $data[2]['value'];
            $body = $data[3]['value'];
            Mail::raw($body, function ($message) use ($name, $email, $subject, $body){
                $message->from($email, $name);
                $message->to(env('FRESHDESK_SUPPORT_EMAIL'), $name);
                $message->subject($subject);
            });
            return 'OK';
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * Terms and conditions Page
     *
     * @return \Illuminate\Http\Response
     */
    public function terms()
    {
        return view('terms');
    }

    /**
     * freshservice Tableau success
     *
     * @return \Illuminate\Http\Response
     */
    public function tableauSuccess()
    {
        return view('billing.freshservice-tableau.success');
    }
     /**
     * freshservice Tableau success
     *
     * @return \Illuminate\Http\Response
     */
    public function freshdesktableauSuccess()
    {
        return view('billing.freshdesk-tableau.success');
    }
}
