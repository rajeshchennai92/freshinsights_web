<?php

namespace App\Http\Controllers;

use App\Mail\SubscriptionCancelled;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\Response;
use Laravel\Cashier\Http\Controllers\WebhookController as CashierController;

class WebHookController extends CashierController
{
    /**
     * Handle a cancelled customer from a Stripe subscription.
     *
     * @param  array  $payload
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function handleCustomerSubscriptionDeleted(array $payload)
    {
        $user = $this->getUserByStripeId($payload['data']['object']['customer']);

        if ($user) {
            $userData = $user->user();
            $user->subscriptions->filter(function ($subscription) use ($payload) {
                return $subscription->stripe_id === $payload['data']['object']['id'];
            })->each(function ($subscription) {
                $subscription->markAsCancelled();
            });
            $user->getAllSubscription(config('plans.subscriptions.freshdesk.subscription_name'))->each(function ($subscription) {
                $subscription->markAsCancelled();
            });
            $freshdeskClient = $userData->freshdeskClients;
            $freshdeskClient->status = false;
            $freshdeskClient->cron = 0;
            $freshdeskClient->save();
            Mail::to($user)
                ->cc(env('FRESHDESK_SUPPORT_EMAIL'))
                ->send(new SubscriptionCancelled());
        }

        return new Response('Webhook Handled', 200);
    }
}
