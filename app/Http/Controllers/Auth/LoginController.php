<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide this functionality to your appliations.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = 'user/myaccount';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Check user's role and redirect user based on their role
     * @return
     */
    public function authenticated()
    {
        if (auth()->user()->hasRole('admin')) {
            return redirect('/admin/dashboard');
        }
        return redirect()->intended(route('userAccount'));
    }

    /**
     * Handle a registration request for the freshdesk.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showLoginPlanForm(Request $request)
    {
        return view('auth.freshdesk.login');
    }

    /**
     * Handle a registration request for the freshdesk.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function loginPlanForm(Request $request)
    {
        $this->validateLogin($request);

        if ($this->attemptLogin($request)) {
            $request->session()->regenerate();
            $this->clearLoginAttempts($request);
            return redirect()->route('freshdeskSubscriptionChoose');
        }
        else{
            return redirect()->back()->withErrors([
                $this->username() => [trans('auth.failed')],
            ])->withInput();
        }
    }

    /**
     * Handle a registration request for the freshservice.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showLoginFreshserviceForm(Request $request)
    {
        return view('auth.freshservice.login');
    }

    /**
     * Handle a registration request for the freshservice.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function loginFreshserviceForm(Request $request)
    {
        $this->validateLogin($request);

        if ($this->attemptLogin($request)) {
            $request->session()->regenerate();
            $this->clearLoginAttempts($request);
            return redirect()->route('freshserviceSubscriptionChoose');
        }
        else{
            return redirect()->back()->withErrors([
                $this->username() => [trans('auth.failed')],
            ])->withInput();
        }
    }

    /**
     * Handle a login view request for the freshservice tableau.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showLoginFreshserviceTableauForm(Request $request)
    {
        return view('auth.freshservice-tableau.login');
    }

    /**
     * Handle a login view request for the freshdesk tableau.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showLoginFreshdeskTableauForm(Request $request)
    {
        return view('auth.freshdesk-tableau.login');
    }

    /**
     * Handle a login request for the freshservice tableau.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function loginFreshserviceTableauForm(Request $request)
    {
        $this->validateLogin($request);

        if ($this->attemptLogin($request)) {
            $this->guard()->logout();
            $request->session()->invalidate();
            return redirect()->route('freshserviceTableauSuccess');
        }
        else{
            return redirect()->back()->withErrors([
                $this->username() => [trans('auth.failed')],
            ])->withInput();
        }
    }

    /**
     * Handle a login request for the freshdesk tableau.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function loginFreshdeskTableauForm(Request $request)
    {
        $this->validateLogin($request);

        if ($this->attemptLogin($request)) {
            $this->guard()->logout();
            $request->session()->invalidate();
            return redirect()->route('freshdeskTableauSuccess');
        }
        else{
            return redirect()->back()->withErrors([
                $this->username() => [trans('auth.failed')],
            ])->withInput();
        }
    }
}