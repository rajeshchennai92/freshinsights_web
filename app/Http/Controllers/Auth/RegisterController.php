<?php 

namespace App\Http\Controllers\Auth;

use App\CashierMultiplan\Subscription;
use App\Events\UserSubscribed;
use App\Models\FreshdeskClient;
use App\Models\FreshserviceClient;
use Carbon\Carbon;
use DB;
use Illuminate\Auth\Events\Registered;
use Validator;
use App\Services\UserService;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/freshdesk/select_subscription';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserService $userService)
    {
        //$this->middleware('guest');
        $this->service = $userService;
    }

    /**
     * Handle a registration request for the freshdesk.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function freshdeskRegisterView(Request $request)
    {
        $user = $request->user();
        if ($user){
            $freshdeskClient = $user->freshdeskClients;
            if ($user->meta->subscribed(config('plans.subscriptions.freshdesk.subscription_name'))) {
                return redirect('user/myaccount');
            }
            if ($freshdeskClient){
                return redirect()->route('freshdeskSubscriptionChoose');
            }
        }
        return view('auth.freshdesk.register');
    }

    /**
     * Handle a registration request for the freshservice.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function freshserviceRegisterView(Request $request)
    {
        $user = $request->user();
        if ($user){
            $freshserviceClient = $user->freshserviceClients;
            if ($user->meta->subscribed(config('plans.subscriptions.freshservice.subscription_name'))) {
                return redirect('user/myaccount');
            }
            if ($freshserviceClient){
                return redirect('user/myaccount');
            }
        }
        return view('auth.freshservice.register');
    }

    /**
     * Handle a registration request for the freshservice.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function freshserviceTableauRegisterView(Request $request)
    {
        $user = $request->user();
        if ($user){
            $freshserviceClient = $user->freshserviceClients;
            if ($freshserviceClient){
                return redirect()->route('freshserviceTableauPage');
            }
        }
        return view('auth.freshservice-tableau.register');
    }

      /**
     * Handle a registration request for the freshservice.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function freshdeskTableauRegisterView(Request $request)
    {
        $user = $request->user();
        if ($user){
            $freshserviceClient = $user->freshdeskClients;
            if ($freshserviceClient){
                return redirect()->route('freshdeskTableauPage');
            }
        }
        return view('auth.freshdesk-tableau.register');
    }
    /**
     * Get a validator for an incoming freshdesk registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorFreshdesk(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'powerbi_email' => 'required|email|max:255',
            'powerbi_pro_account' => 'required',
            'password' => 'required|min:6|confirmed',
            'organization_url' => 'required|max:255|regex:/^(?:https:\/\/)[\w.-]+[.]+freshdesk.com$/|unique:freshdesk_clients',
            'organization_api_key' => 'required|max:255|unique:freshdesk_clients',
        ]);
    }

    /**
     * Get a validator for an incoming freshdesk create request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorFreshdeskCreate(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'organization_url' => 'required|max:255|regex:/^(?:https:\/\/)[\w.-]+[.]+freshdesk.com$/|unique:freshdesk_clients',
            'organization_api_key' => 'required|max:255|unique:freshdesk_clients',
        ]);
    }


    /**
     * Get a validator for an incoming freshdesk registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorFreshservice(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'powerbi_email' => 'required|email|max:255',
            'powerbi_pro_account' => 'required',
            'password' => 'required|min:6|confirmed',
            'organization_url' => 'required|max:255|regex:/^(?:https:\/\/)[\w.-]+[.]+freshservice.com$/|unique:freshservice_clients',
            'organization_api_key' => 'required|max:255|unique:freshservice_clients',
        ]);
    }

    /**
     * Get a validator for an incoming freshservice registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorFreshserviceTableau(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'organization_url' => 'required|max:255|regex:/^(?:https:\/\/)[\w.-]+[.]+freshservice.com$/|unique:freshservice_clients',
            'organization_api_key' => 'required|max:255|unique:freshservice_clients',
        ]);
    }

    /**
     * Get a validator for an incoming freshservice registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorFreshdeskTableau(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'organization_url' => 'required|max:255|regex:/^(?:https:\/\/)[\w.-]+[.]+freshdesk.com$/|unique:freshdesk_clients',
            'organization_api_key' => 'required|max:255|unique:freshdesk_clients',
        ]);
    }


    /**
     * Get a validator for an incoming freshdesk create request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorFreshserviceCreate(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'organization_url' => 'required|max:255|regex:/^(?:https:\/\/)[\w.-]+[.]+freshservice.com$/|unique:freshservice_clients',
            'organization_api_key' => 'required|max:255|unique:freshservice_clients',
        ]);
    }

    /**
     * Get a validator for an test connection request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorTestConnection(array $data)
    {
        $rules = array(
            'organization_api_key' => 'required|max:255',
            'org' => 'required|integer|min:1|digits_between: 1,2',
            'freshdesk_url' => 'required_if:org,1|regex:/^(?:https:\/\/)[\w.-]+[.]+freshdesk.com$/',
            'freshservice_url' => 'required_if:org,2|regex:/^(?:https:\/\/)[\w.-]+[.]+freshservice.com$/',
        );
        return Validator::make($data, $rules);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return DB::transaction(function() use ($data) {
            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password'])
            ]);

            return $this->service->create($user, $data['password'], $data);
        });
    }

    /**
     * Handle a registration request for the freshdesk.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function freshdeskRegister(Request $request)
    {

        if($request->get('user_plan') == 1){
               session_start();
                $_SESSION["subdata"] ='Power BI';

        }else{
               session_start();
               $_SESSION["subdata"] ='Tableau';

        }
        $user = $request->user();
        if ($user){
            $validator = $this->validatorFreshdeskCreate($request->all());

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $api_key = $request->get('organization_api_key');
            $basic_url = $request->get('organization_url');
            $url_string=$basic_url.'/api/v2/agents';
            $connection  = $this->curlPostRequest($url_string,$api_key);

            if ($connection == 'Failure'){
                $validator->errors()->add('organization_api_key', 'The api key is invalid');
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $freshdeskClient = $user->freshdeskClients;
            if ($freshdeskClient){
                $validator->errors()->add('Name', 'You are already registered for the product');
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $name = $request->get('name');
            $parsedUrl = parse_url($request->get('organization_url'));
            $host = explode('.', $parsedUrl['host']);
            $db_name = strtolower($host[0]);

            DB::transaction(function() use ($request,$user,$name, $db_name) {
                $freshdeskClient = FreshdeskClient::firstOrCreate([
                    'user_id' => $user->id,
                    'organization_name' => $name,
                    'organization_url' => $request->get('organization_url'),
                    'organization_api_key' => $request->get('organization_api_key'),
                    'organization_db_name' => $db_name.'_fd_db',
                    'organization_folder_name' => $db_name.'_fd_folder',
                ]);
            });
            return redirect()->route('freshdeskSubscriptionChoose');
        }
        else{
            if($request->get('user_plan') == 2){
                $validator = $this->validatorFreshdeskTableau($request->all());
            }else {
                $validator = $this->validatorFreshdesk($request->all());
            }

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $api_key = $request->get('organization_api_key');
            $basic_url = $request->get('organization_url');
            $url_string=$basic_url.'/api/v2/agents';
            $connection  = $this->curlPostRequest($url_string,$api_key);

            if ($connection == 'Failure'){
                $validator->errors()->add('organization_api_key', 'The api key is invalid');
                return redirect()->back()->withErrors($validator)->withInput();
            }

            event(new Registered($user = $this->create($request->all())));

            $name = $request->get('name');

            $parsedUrl = parse_url($request->get('organization_url'));
            $host = explode('.', $parsedUrl['host']);
            $db_name = strtolower($host[0]);

            DB::transaction(function() use ($request,$user, $name, $db_name) {
                $freshdeskClient = FreshdeskClient::firstOrCreate([
                    'user_id' => $user->id,
                    'organization_name' => $name,
                    'organization_url' => $request->get('organization_url'),
                    'organization_api_key' => $request->get('organization_api_key'),
                    'organization_db_name' => $db_name.'_fd_db',
                    'organization_folder_name' => $db_name.'_fd_folder',
                ]);
            });

            $this->guard()->login($user);
            if($request->get('user_plan') == 1) {

                return redirect()->route('freshdeskSubscriptionChoose');
            }
            if($request->get('user_plan') == 2) {
                return redirect()->route('freshdeskTableauSubscribeChoose');
            }

        }
    }

    /**
     * Handle a registration request for the freshdesk.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function freshserviceRegister(Request $request)
    {
        if($request->get('user_plan') == 1){
                session_start();
                $_SESSION["subdata"] ='Power BI';

        }else {
            session_start();
            $_SESSION["subdata"] = 'Tableau';
        }

            $user = $request->user();
        if ($user){
            $validator = $this->validatorFreshserviceCreate($request->all());

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $api_key = $request->get('organization_api_key');
            $basic_url = $request->get('organization_url');
            $url_string=$basic_url.'/itil/releases.json';
            $connection  = $this->curlPostRequest($url_string,$api_key);

            if ($connection == 'Failure'){
                $validator->errors()->add('organization_api_key', 'The api key is invalid');
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $freshserviceClient = $user->freshserviceClients;
            if ($freshserviceClient){
                $validator->errors()->add('Name', 'You are already registered for the product');
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $name = $request->get('name');

            $parsedUrl = parse_url($request->get('organization_url'));
            $host = explode('.', $parsedUrl['host']);
            $db_name = strtolower($host[0]);

            DB::transaction(function() use ($request,$user,$name, $db_name) {
                $freshserviceClient = FreshserviceClient::firstOrCreate([
                    'user_id' => $user->id,
                    'organization_name' => $name,
                    'organization_url' => $request->get('organization_url'),
                    'organization_api_key' => $request->get('organization_api_key'),
                    'organization_folder_name' => $db_name.'_fs_folder',
                    'organization_db_name' => $db_name.'_fs_db',
                ]);
            });
        }
        else{
            if($request->get('user_plan') == 2){
                $validator = $this->validatorFreshserviceTableau($request->all());
            }else {

                $validator = $this->validatorFreshservice($request->all());
            }

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $api_key = $request->get('organization_api_key');
            $basic_url = $request->get('organization_url');
            $url_string=$basic_url.'/itil/releases.json';
            $connection  = $this->curlPostRequest($url_string,$api_key);

            if ($connection == 'Failure'){
                $validator->errors()->add('organization_api_key', 'The api key is invalid');
                return redirect()->back()->withErrors($validator)->withInput();
            }

            event(new Registered($user = $this->create($request->all())));

            $name = $request->get('name');

            $parsedUrl = parse_url($request->get('organization_url'));
            $host = explode('.', $parsedUrl['host']);
            $db_name = strtolower($host[0]);

            DB::transaction(function() use ($request,$user, $name, $db_name) {
                $freshserviceClient = FreshserviceClient::firstOrCreate([
                    'user_id' => $user->id,
                    'organization_name' => $name,
                    'organization_url' => $request->get('organization_url'),
                    'organization_api_key' => $request->get('organization_api_key'),
                    'organization_folder_name' => $db_name.'_fs_folder',
                    'organization_db_name' => $db_name.'_fs_db',
                ]);
            });

            $this->guard()->login($user);
        }
        if($request->get('user_plan') == 1) {

            return redirect()->route('freshserviceSubscriptionChoose');
        }
        if($request->get('user_plan') == 2) {
            return redirect()->route('freshserviceTableauSubscriptionChoose');
        }
    }

    /**
     * Handle a registration request for the freshdesk.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function freshserviceTableauRegister(Request $request)
    {
        if($request->get('powerbi_email')){
            $powerbi = $request->get('powerbi_email');
            if(isset($powerbi) && !empty($powerbi)){
                session_start();
                $_SESSION["subdata"] ='Power BI';
            }
        }else {
            session_start();
            $_SESSION["subdata"] = 'Tableau';
        }

        $user = $request->user(); 
        if ($user){
            $validator = $this->validatorFreshserviceCreate($request->all());

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $api_key = $request->get('organization_api_key');
            $basic_url = $request->get('organization_url');
            $url_string=$basic_url.'/itil/releases.json';
            $connection  = $this->curlPostRequest($url_string,$api_key);

            if ($connection == 'Failure'){
                $validator->errors()->add('organization_api_key', 'The api key is invalid');
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $freshserviceClient = $user->freshserviceClients;
            if ($freshserviceClient){
                $validator->errors()->add('Name', 'You are already registered for the product');
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $name = $request->get('name');

            $parsedUrl = parse_url($request->get('organization_url'));
            $host = explode('.', $parsedUrl['host']);
            $db_name = strtolower($host[0]);

            DB::transaction(function() use ($request,$user,$name, $db_name) {
                $freshserviceClient = FreshserviceClient::firstOrCreate([
                    'user_id' => $user->id,
                    'organization_name' => $name,
                    'organization_url' => $request->get('organization_url'),
                    'organization_api_key' => $request->get('organization_api_key'),
                    'organization_folder_name' => $db_name.'_fs_folder',
                    'organization_db_name' => $db_name.'_fs_db',
                ]);
            });
        }
        else{
            $validator = $this->validatorFreshserviceTableau($request->all());

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $api_key = $request->get('organization_api_key');
            $basic_url = $request->get('organization_url');
            $url_string=$basic_url.'/itil/releases.json';
            $connection  = $this->curlPostRequest($url_string,$api_key);

            if ($connection == 'Failure'){
                $validator->errors()->add('organization_api_key', 'The api key is invalid');
                return redirect()->back()->withErrors($validator)->withInput();
            }

            event(new Registered($user = $this->create($request->all())));

            $name = $request->get('name');

            $parsedUrl = parse_url($request->get('organization_url'));
            $host = explode('.', $parsedUrl['host']);
            $db_name = strtolower($host[0]);

            DB::transaction(function() use ($request,$user, $name, $db_name) {
                $freshserviceClient = FreshserviceClient::firstOrCreate([
                    'user_id' => $user->id,
                    'organization_name' => $name,
                    'organization_url' => $request->get('organization_url'),
                    'organization_api_key' => $request->get('organization_api_key'),
                    'organization_folder_name' => $db_name.'_fs_folder',
                    'organization_db_name' => $db_name.'_fs_db',
                ]);
            });

        }
        return redirect()->route('freshserviceTableauSuccess');
    }


    /**
     * Handle a registration request for the freshdesk.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function freshdeskTableauRegister(Request $request)
    {
        if($request->get('powerbi_email')){
            $powerbi = $request->get('powerbi_email');
            if(isset($powerbi) && !empty($powerbi)){
                session_start();
                $_SESSION["subdata"] ='Power BI';
            }
        }else {
            session_start();
            $_SESSION["subdata"] = 'Tableau';
        }
        $user = $request->user();
        if ($user){
            $validator = $this->validatorFreshdeskCreate($request->all());

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $api_key = $request->get('organization_api_key');
            $basic_url = $request->get('organization_url');
            $url_string=$basic_url.'/itil/releases.json';
            $connection  = $this->curlPostRequest($url_string,$api_key);

            if ($connection == 'Failure'){
                $validator->errors()->add('organization_api_key', 'The api key is invalid');
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $freshdeskClient = $user->freshdeskClients;
            if ($freshdeskClient){
                $validator->errors()->add('Name', 'You are already registered for the product');
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $name = $request->get('name');

            $parsedUrl = parse_url($request->get('organization_url'));
            $host = explode('.', $parsedUrl['host']);
            $db_name = strtolower($host[0]);

            DB::transaction(function() use ($request,$user,$name, $db_name) {
                $freshdeskClient = FreshdeskClient::firstOrCreate([
                    'user_id' => $user->id,
                    'organization_name' => $name,
                    'organization_url' => $request->get('organization_url'),
                    'organization_api_key' => $request->get('organization_api_key'),
                    'organization_folder_name' => $db_name.'_fs_folder',
                    'organization_db_name' => $db_name.'_fs_db',
                ]);
            });
        }
        else{
            $validator = $this->validatorFreshdeskTableau($request->all());
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $api_key = $request->get('organization_api_key');
            $basic_url = $request->get('organization_url');
            $url_string=$basic_url.'/itil/releases.json';
            $connection  = $this->curlPostRequest($url_string,$api_key);

            if ($connection == 'Failure'){
                $validator->errors()->add('organization_api_key', 'The api key is invalid');
                return redirect()->back()->withErrors($validator)->withInput();
            }

            event(new Registered($user = $this->create($request->all())));

            $name = $request->get('name');

            $parsedUrl = parse_url($request->get('organization_url'));
            $host = explode('.', $parsedUrl['host']);
            $db_name = strtolower($host[0]);

            DB::transaction(function() use ($request,$user, $name, $db_name) {
                $freshdeskClient = FreshdeskClient::firstOrCreate([
                    'user_id' => $user->id,
                    'organization_name' => $name,
                    'organization_url' => $request->get('organization_url'),
                    'organization_api_key' => $request->get('organization_api_key'),
                    'organization_folder_name' => $db_name.'_fs_folder',
                    'organization_db_name' => $db_name.'_fs_db',
                ]);
            });

        }
        return redirect()->route('freshdeskTableauSuccess');
    }


    /**
     * Handle a given url is correct or not
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function testConnection(Request $request)
    {
        $validator = $this->validatorTestConnection($request->all());

        if ($validator->fails()) {
            return response()->json([
                'response' => 'error',
                'details' => $validator->errors()
            ]);
        }
        $api_key = $request->get('organization_api_key');
        if ($request->get('org') == 1){
            $basic_url = $request->get('freshdesk_url');
            $url_string=$basic_url.'/api/v2/agents';
        }
        elseif ($request->get('org') == 2){
            $basic_url = $request->get('freshservice_url');
            $url_string=$basic_url.'/api/v2/tickets';
        }
        $response =  $this->curlPostRequest($url_string,$api_key);
        if ($response == "Success"){
            return response()->json([
                'response' => 'success',
                'details' => 1
            ]);
        }
        return response()->json([
            'response' => 'error',
            'details' => 0
        ]);
    }

    //function for the curl request
    private function curlPostRequest($url_string, $api_key){
        $ch = curl_init($url_string);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Connection: Keep-Alive'
        ));
        curl_setopt($ch, CURLOPT_USERPWD, $api_key);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $server_output = curl_exec($ch);
        $info = curl_getinfo($ch);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);

        $headers = substr($server_output, 0, $header_size);
        $response = substr($server_output, $header_size);
        if ($info['http_code'] == 200) {
            return 'Success';
//            $response_array = json_decode($response, true);
//            if (array_key_exists("require_login",$response_array)){
//                return 'Failure';
//            }
//            else{
//                return 'Success';
//            }

        }else{
            return 'Failure';
        }
        curl_close($ch);
    }
}
