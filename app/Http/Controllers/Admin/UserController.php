<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserInviteRequest;

class UserController extends Controller
{
    public function __construct(UserService $userService)
    {
        $this->service = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->service->all();
        return view('admin.users.index')->with('users', $users);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        if (! $request->search) {
            return redirect('admin/users');
        }

        $users = $this->service->search($request->search);
        return view('admin.users.index')->with('users', $users);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->service->find($id);
        return view('admin.users.edit')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $result = $this->service->update($id, $request->except(['_token', '_method']));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->with('errors', ['Failed to update']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect('admin/users')->with('message', 'Successfully deleted');
        }

        return redirect('admin/users')->with('errors', ['Failed to delete']);
    }

    /**
     * Display a user details.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->service->find($id);
        $fdSubscription = $user->meta->subscription(config('plans.subscriptions.freshdesk.subscription_name'));
        $fsSubscription = $user->meta->subscription(config('plans.subscriptions.freshservice.subscription_name'));
        $fdAllSubscriptions = $user->meta->subscriptions()->where('name',config('plans.subscriptions.freshdesk.subscription_name'))->get();
        $fsAllSubscriptions = $user->meta->subscriptions()->where('name',config('plans.subscriptions.freshservice.subscription_name'))->get();
        return view('admin.users.show')->with([
            'user' => $user,
            'fdSubscription' => $fdSubscription,
            'fsSubscription' => $fsSubscription,
            'fdAllSubscriptions' => $fdAllSubscriptions,
            'fsAllSubscriptions' => $fsAllSubscriptions,
        ]);
    }
}
