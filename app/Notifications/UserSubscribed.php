<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserSubscribed extends Notification
{
    use Queueable;

    public $subscription;

    public $product;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($subscription, $product)
    {
        $this->subscription = $subscription;
        $this->product = $product;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        session_start();

        return (new MailMessage)
            ->subject('Subscription Confirmed')
            ->cc(env('FRESHDESK_SUPPORT_EMAIL'))
            ->markdown('email.subscribed', ['subscription' => $this->subscription, 'product' => $this->product, 'url' => url('login'),'data'=> $_SESSION["subdata"]]);
    }

}
