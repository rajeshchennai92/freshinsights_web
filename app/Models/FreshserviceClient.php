<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FreshserviceClient extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'freshservice_clients';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'organization_name',
        'organization_url',
        'organization_api_key',
        'organization_folder_name',
        'organization_db_name',
        'historical_payment',
        'historical_ticket_count',
    ];

    /**
     * User
     *
     * @return Relationship
     */
    public function user()
    {
        return User::where('id', $this->user_id)->first();
    }
}
