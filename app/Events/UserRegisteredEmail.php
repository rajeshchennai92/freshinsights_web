<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserRegisteredEmail
{
    use InteractsWithSockets, SerializesModels;

    public $user;
    public $password;
    public $verification_code;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $password, $verification_code)
    {
        $this->user = $user;
        $this->password = $password;
        $this->verification_code = $verification_code;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
