<?php

namespace App\Mail;

use App\CashierMultiplan\Subscription;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FreeSubscriptionNotification extends Mailable
{
    use Queueable, SerializesModels;

    private $subscription;
    private $days_after;
    private $route_name;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Subscription $subscription, $days_after, $route_name)
    {
        $this->subscription = $subscription;
        $this->days_after= $days_after;
        $this->route_name= $route_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.subscription.freenotification',['url' => route($this->route_name), 'date' => $this->subscription->ends_at, 'day' => $this->days_after]);
    }
}
