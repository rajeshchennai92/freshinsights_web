<?php

namespace App\CashierMultiplan;

use Carbon\Carbon;
use LogicException;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Subscription as CashierSubscription;

class Subscription extends CashierSubscription
{
    /**
     * Resume the cancelled subscription.
     *
     * @return $this
     *
     * @throws \LogicException
     */
    public function resume()
    {
        if (! $this->onGracePeriod()) {
            throw new LogicException('Unable to resume subscription that is not within grace period.');
        }

        if (!$this->subscriptionItems->isEmpty()) {
            throw new LogicException("Cannot update using plan parameter when multiple plans exist on the subscription. Updates must be made to individual items instead.");
        }

        $subscription = $this->asStripeSubscription();

        // To resume the subscription we need to set the plan parameter on the Stripe
        // subscription object. This will force Stripe to resume this subscription
        // where we left off. Then, we'll set the proper trial ending timestamp.
        $subscription->plan = $this->stripe_plan;

        if ($this->onTrial()) {
            $subscription->trial_end = $this->trial_ends_at->getTimestamp();
        } else {
            $subscription->trial_end = 'now';
        }

        $subscription->save();

        // Finally, we will remove the ending timestamp from the user's record in the
        // local database to indicate that the subscription is active again and is
        // no longer "cancelled". Then we will save this record in the database.
        $this->fill(['ends_at' => null])->save();

        return $this;
    }

    /**
     * Get the subscription items for the model.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function subscriptionItems()
    {
        return $this->hasMany(SubscriptionItem::class, $this->getForeignKey())->orderBy('created_at', 'desc');
    }

    /**
     * Adds a plan to the subscription
     *
     * @param string $plan The added plan's ID
     * @param integer $quantity The quantity to be added
     * @return $this
     */
    public function addItem($plan, $prorate = true, $quantity = 1)
    {
        // retrieves the subscription stored at Stripe
        $stripeSubscription = $this->asStripeSubscription();

        // adds the new item at Stripe
        $stripeSubscriptionItem = $stripeSubscription->items->create([
            'plan' => $plan,
            'prorate' => $prorate,
            'quantity' => $quantity,
        ]);

        // saves the new item in the database
        $this->subscriptionItems()->create([
            'stripe_id' => $stripeSubscriptionItem->id,
            'stripe_plan' => $plan,
            'quantity' => $quantity,
        ]);

        return $this;
    }

    /**
     * Adds a plan from the subscription
     *
     * @param string $plan The removed plan's ID
     * @return $this
     */
    public function removeItem($plan, $prorate = true)
    {
        $item = $this->subscriptionItems()->where('stripe_plan', $plan)->first();

        if (is_null($item)) {
            // item not found
            return $this;
        }

        // retrieves the item stored at Stripe
        $stripeItem = $item->asStripeSubscriptionItem();

        // deletes the item at Stripe
        $stripeItem->delete([
            'prorate' => $prorate,
        ]);

        // removes the item from the database
        $this->subscriptionItems()->where('stripe_plan', $plan)->delete();

        return $this;
    }

    /**
     * Gets the item by name
     *
     * @param string $plan The plan's ID
     * @return SubscriptionItem|null
     */
    public function subscriptionItem($plan)
    {
        return $this->subscriptionItems()->where('stripe_plan', $plan)->first();
    }

    /**
     * Determines if the subscription contains the given plan
     *
     * @param string $plan The plan's ID
     * @return bool
     */
    public function hasItem($plan)
    {
        return !!$this->subscriptionItem($plan);
    }

    /**
     * Swap the subscription to a new Stripe plan.
     *
     * @param  string  $plan
     * @return $this
     */
    public function swapPlan($plan, $stripe_plan_id, $stripe_add_id)
    {
        $subscription = $this->asStripeSubscription();

        $subscription->prorate = $this->prorate;

        $itemArr = array();
        foreach ($subscription->items->data as $item){
            if ($item['plan']['usage_type'] == 'metered'){
                $itemArr[] = array(
                    'id' => $item['id'],
                    'plan' => $stripe_add_id,
                );
            }
            else{
                $itemArr[] = array(
                    'id' => $item['id'],
                    'plan' => $stripe_plan_id,
                );
            }
        }

        $subscription->items = $itemArr;

        $subscription->billing_cycle_anchor = 'now';

        $subscription->save();

        foreach ($this->subscriptionItems as $subscriptionItem){
            $subscriptionItem->delete();
        }

        // registers the subscription's items
        foreach ($subscription->items->data as $item) {
            $this->subscriptionItems()->create([
                'stripe_id' => $item['id'],
                'stripe_plan' => $item['plan']['id'],
                'quantity' => $item['quantity']?$item['quantity']:0,
            ]);
        }

        $this->fill([
            'stripe_plan' => '',
            'plan_name' => $plan,
            'ends_at' => null,
        ])->save();

        return $this;
    }

    /**
     * Update the quantity of the subscription.
     *
     * @param  int  $quantity
     * @param  bool  $prorate
     * @return $this
     */
    public function updateMeteredUsage($quantity)
    {
        $subscription = $this->asStripeSubscription();
        $data = array(
            "quantity" => $quantity,
            "timestamp" => time(),
            "action" => "set"
        );

        $url = 'https://api.stripe.com/v1/subscription_items/'.$subscription->items->data[0]->id.'/usage_records';

        // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, config('services.stripe.secret') . ":" . "");

        $headers = array();
        $headers[] = "Content-Type: application/x-www-form-urlencoded";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

        $info = curl_getinfo($ch);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $headers = substr($result, 0, $header_size);
        $response = substr($result, $header_size);
        if ($info['http_code'] == 200) {
            curl_close($ch);
            return true;
        } else {
            curl_close($ch);
            return false;
        }
    }

    /**
     * Calculate refund amount on an unsused part of the subscription
     *
     * @param $subscription Stripe Subscription
     * @param \DateTime             $time         Time of the cancellation
     *
     * @return int
     */
    public function prorate($subscription, \DateTime $time, $amount) {
        $subscription = $subscription->asStripeSubscription();
        $period_start = $subscription->current_period_start;
        $period_end = $subscription->current_period_end;
        $period_length = $period_end - $period_start;
        $elapsed_since_start = $time->getTimestamp() - $period_start;
        $refund = $amount - floor(($elapsed_since_start / $period_length) * $amount);
        return $refund > 0 ? (int) $refund : 0;
    }

    /**
     * Calculate used ticket count for he current subscriptions
     *
     * @param $subscription Stripe Subscription
     * @param \DateTime             $time         Time of the cancellation
     *
     * @return int
     */
    public function getMeteredBillingAmount($ticket_count, $avgMonthlyCount, $additionalFee,\DateTime $time) {
        $subscription = $this->asStripeSubscription();
        $period_start = $subscription->current_period_start;
        $period_end = $subscription->current_period_end;
        $period_length = $period_end - $period_start;
        $elapsed_since_start = $time->getTimestamp() - $period_start;
        $allowedCount = floor(($elapsed_since_start / $period_length) * $avgMonthlyCount);
        if ($ticket_count>$allowedCount){
            return ($ticket_count-$allowedCount)*$additionalFee;
        }
        return 0;
    }
}