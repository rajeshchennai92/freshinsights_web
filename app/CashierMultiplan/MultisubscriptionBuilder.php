<?php

namespace App\CashierMultiplan;

use Carbon\Carbon;
use Laravel\Cashier\SubscriptionBuilder;

class MultisubscriptionBuilder extends SubscriptionBuilder
{
    /**
     * The plans being subscribed to.
     *
     * @var array
     */
    protected $plans = [];

    /**
     * The name of the plan in stripe being subscribed to.
     *
     * @var string
     */
    protected $stripe_plan;

    /**
     * The name of the common plan being subscribed to.
     *
     * @var string
     */
    protected $plan_name;

    /**
     * The name of the plan interval period being subscribed to.
     *
     * @var string
     */
    protected $plan_interval;

    /**
     * The name of the quantity of the plan being subscribed to.
     *
     * @var integer
     */
    protected $quantity;

    /**
     * Create a new subscription builder instance.
     *
     * @param  mixed  $owner
     * @param  string  $name
     * @param  string  $plan_name
     * @return void
     */
    public function __construct($owner, $name = 'default', $stripe_plan, $plan_name, $plan_interval=null, $quantity =null)
    {
        $this->owner = $owner;
        $this->name = $name;
        $this->stripe_plan = $stripe_plan;
        $this->plan_name = $plan_name;
        $this->plan_interval = $plan_interval;
        $this->quantity = $quantity;
    }

    /**
     * Add a new Stripe subscription to the Stripe model.
     *
     * @param  string  $code
     * @param  int  $quantity
     */
    public function addPlan($code, $quantity = 1)
    {
        $this->plans[$code] = $quantity;
        return $this;
    }

    /**
     * Creates a new Stripe subscription with multiple plans.
     *
     * @param  string|null  $token
     * @param  array  $options
     * @return \Laravel\Cashier\Subscription
     */
    public function create($token = null, array $options = [])
    {
        $customer = $this->getStripeCustomer($token, $options);

        $stripeSubscription = $customer->subscriptions->create($this->buildPayload());

        if ($this->skipTrial) {
            $trialEndsAt = null;
        } else {
            $trialEndsAt = $this->trialExpires;
        }

        // registers the subscription
        $subscription = $this->owner->subscriptions()->create([
            'name' => $this->name,
            'stripe_id' => $stripeSubscription->id,
            'stripe_plan' => '',
            'plan_name' => $this->plan_name,
            'quantity' => 0,
            'trial_ends_at' => $trialEndsAt,
            'ends_at' => null,
        ]);
        // registers the subscription's items
        foreach ($stripeSubscription->items->data as $item) {
            $subscription->subscriptionItems()->create([
                'stripe_id' => $item['id'],
                'stripe_plan' => $item['plan']['id'],
                'quantity' => $item['quantity']?$item['quantity']:0,
            ]);
        }

        return $subscription;
    }

    /**
     * Build the payload for subscription creation.
     *
     * @return array
     */
    protected function buildPayload()
    {
        return array_filter([
            'items' => $this->buildPayloadItems(),
            'coupon' => $this->coupon,
            'trial_end' => $this->getTrialEndForPayload(),
            'tax_percent' => $this->getTaxPercentageForPayload(),
            'metadata' => $this->metadata,
        ]);
    }

    protected function buildPayloadItems()
    {
        $items = [];
        foreach ($this->plans as $plan => $quantity) {
            array_push($items,[
                'plan' => $plan,
                'quantity' => $quantity,
            ]);
        }
        return $items;
    }

    /**
     * Create a new Stripe subscription.
     *
     * @param  string|null  $token
     * @param  array  $options
     * @return \Laravel\Cashier\Subscription
     */
    public function createSubscription($token = null, array $options = [])
    {
        $customer = $this->getStripeCustomer($token, $options);

        $start = new Carbon('first day of next month');
        $this->anchorBillingCycleOn($start->timestamp);

        $subscription = $customer->subscriptions->create($this->buildPayloadForSubscriion());

        if ($this->skipTrial) {
            $trialEndsAt = null;
        } else {
            $trialEndsAt = $this->trialExpires;
        }

        return $this->owner->subscriptions()->create([
            'name' => $this->name,
            'stripe_id' => $subscription->id,
            'stripe_plan' => $this->stripe_plan,
            'plan_name' => $this->plan_name,
            'plan_interval' => $this->plan_interval,
            'quantity' => $this->quantity?$this->quantity:0,
            'trial_ends_at' => $trialEndsAt,
            'ends_at' => null,
        ]);
    }

    /**
     * Build the payload for subscription creation.
     *
     * @return array
     */
    protected function buildPayloadForSubscriion()
    {
        return array_filter([
            'billing_cycle_anchor' => $this->billingCycleAnchor,
            'coupon' => $this->coupon,
            'metadata' => $this->metadata,
            'plan' => $this->stripe_plan,
            'quantity' => $this->quantity,
            'tax_percent' => $this->getTaxPercentageForPayload(),
            'trial_end' => $this->getTrialEndForPayload(),
        ]);
    }
}