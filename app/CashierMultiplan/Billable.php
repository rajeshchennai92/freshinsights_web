<?php

namespace App\CashierMultiplan;

trait Billable
{
    use \Laravel\Cashier\Billable;

    /**
     * Determine if the Stripe model has a given subscription.
     *
     * @param  string  $subscription
     * @param  string|null  $plan
     * @return bool
     */
    public function subscribed($subscription = 'default', $plan = null)
    {
        $subscription = $this->subscription($subscription);

        if (is_null($subscription)) {
            return false;
        }

        if (is_null($plan)) {
            return $subscription->valid();
        }

        if ($subscription->valid() && $subscription->stripe_plan === $plan) {
            return true;
        }

        $plan = $this->subscriptionItem($plan);

        if (!is_null($plan) && $subscription->valid()) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the Stripe model is actively subscribed to one of the given plans.
     *
     * @param  array|string  $plans
     * @param  string  $subscription
     * @return bool
     */
    public function subscribedToPlan($plans, $subscription = 'default')
    {
        $subscription = $this->subscription($subscription);

        if (! $subscription || ! $subscription->valid()) {
            return false;
        }

        foreach ((array) $plans as $plan) {
            if ($subscription->stripe_plan === $plan) {
                return true;
            }
            if ($subscription->hasItem($plan)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine if the entity is on the given plan.
     *
     * @param  string  $plan
     * @return bool
     */
    public function onPlan($plan)
    {
        $subscription = $this->subscriptionByPlan($plan);

        if (!is_null($subscription)) {
            return $subscription->valid();
        }

        return false;
    }

    /**
     * Begin creating a new multisubscription.
     *
     * @param  string  $subscription
     * @param  string  $plan
     * @return \Laravel\Cashier\SubscriptionBuilder
     */
    public function newMultisubscription($subscription, $stripe_plan_name, $plan_name, $plan_interval=null, $quantity=null)
    {
        return new MultisubscriptionBuilder($this, $subscription, $stripe_plan_name, $plan_name, $plan_interval, $quantity);
    }

    /**
     * Get all the subscription items for the user
     */
    public function subscriptionItems()
    {
        return $this->hasManyThrough(SubscriptionItem::class, Subscription::class)->orderBy('created_at', 'desc');
    }

    /**
     * Gets a subscription item instance by name.
     *
     * @param  string  $plan
     * @return SubscriptionItem|null
     */
    public function subscriptionItem($plan)
    {
        $itemsTable = (new SubscriptionItem)->getTable();
        return $this->subscriptionItems()->where($itemsTable.'.stripe_plan', $plan)->orderBy($itemsTable.'.created_at', 'desc')->first();
    }

    /**
     * Adds a plan to the model's subscription
     *
     * @param string $plan The plan's ID
     * @param integer $quantity The plan's quantity
     * @param string $subscription The subscription's name
     * @return \Laravel\Cashier\Subscription
     */
    public function addPlan($plan, $prorate = true, $quantity = 1, $subscription = 'default')
    {
        $subscription = $this->subscription($subscription);

        if (!is_null($subscription)) {
            return $subscription->addItem($plan, $prorate, $quantity);
        }

        return $this->newMultisubscription($subscription)->addPlan($plan, $prorate, $quantity)->create();
    }

    /**
     * Removes a plan from the model's subscription
     *
     * @param string $plan The plan's ID
     * @return \Laravel\Cashier\Subscription|null
     */
    public function removePlan($plan, $prorate = true, $subscription = 'default')
    {
        $subscription = $this->subscription($subscription);

        if (is_null($subscription)) {
            return null;
        }

        return $subscription->removeItem($plan, $prorate);
    }

    /**
     * Gets the subscription that contains the given plan
     *
     * @param string $plan The plan's ID
     * @return \Laravel\Cashier\Subscription|null
     */
    public function subscriptionByPlan($plan)
    {
        $subscription = $this->subscriptions()->where('stripe_plan', $plan)->first();

        if (!is_null($subscription)) {
            return $subscription;
        }

        $item = $this->subscriptionItem($plan);

        if (is_null($item)) {
            return null;
        }

        return $item->subscription;
    }

    /**
     * Get all of the subscriptions for the Stripe model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subscriptions()
    {
        return $this->hasMany(Subscription::class, $this->getForeignKey())->orderBy('created_at', 'desc');
    }

    /**
     * Get a subscription instance by name.
     *
     * @param  string  $subscription
     * @return \Laravel\Cashier\Subscription|null
     */
    public function subscription($subscription = 'default')
    {
        return $this->subscriptions->sortByDesc(function ($value) {
            return $value->created_at->getTimestamp();
        })
            ->first(function ($value) use ($subscription) {
                return $value->name === $subscription && $value->plan_interval!=null;
            });
    }

    /**
     * Get a subscription instance by name.
     *
     * @param  string  $subscription
     * @return \Laravel\Cashier\Subscription|null
     */
    public function getAllSubscription($subscription = 'default')
    {
        return $this->subscriptions()->where('name',$subscription)
            ->get()
            ->filter(function ($value) {
                return $value->valid();
            });
    }
}