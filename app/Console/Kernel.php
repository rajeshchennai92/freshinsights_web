<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('monthly:ticketcount')->cron('30 23 30 4,6,9,11 *');
        $schedule->command('monthly:ticketcount')->cron('30 23 31 1,3,5,7,8,10,12 *');
        $schedule->command('monthly:ticketcount')->cron('55 23 28 2 *');
        $schedule->command('fsmonthly:ticketcount')->cron('30 23 30 4,6,9,11 *');
        $schedule->command('fsmonthly:ticketcount')->cron('30 23 31 1,3,5,7,8,10,12 *');
        $schedule->command('fsmonthly:ticketcount')->cron('55 23 28 2 *');
        $schedule->command('freefd:notification')->dailyAt('05:00');
        $schedule->command('freefs:notification')->dailyAt('06:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
