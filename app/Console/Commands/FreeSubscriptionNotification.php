<?php

namespace App\Console\Commands;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class FreeSubscriptionNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'freefd:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Freshdesk free subscription notification mail';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();
        foreach ($users as $user){
            if (!$user->hasRole('admin')){
                if ($user->meta->subscribed(config('plans.subscriptions.freshdesk.subscription_name'))){
                    $fdSubscription = $user->meta->subscription(config('plans.subscriptions.freshdesk.subscription_name'));
                    if ($fdSubscription->plan_name == 'free'){
                        $date = Carbon::parse($fdSubscription->ends_at);
                        $current_date = Carbon::now();
                        $one_day = $current_date->copy()->addDays(1);
                        $route_name = 'freshdeskSubscriptionChoose';
                        $three_days = $current_date->copy()->addDays(5);
                        $seven_days = $current_date->copy()->addDays(9);
                        if ($one_day->isSameDay($date)){
                            Mail::to($user)->send(new \App\Mail\FreeSubscriptionNotification($fdSubscription, 1, $route_name));
                        }
                        if ($three_days->isSameDay($date)){
                            Mail::to($user)->send(new \App\Mail\FreeSubscriptionNotification($fdSubscription, 5, $route_name));
                        }
                        if ($seven_days->isSameDay($date)){
                            Mail::to($user)->send(new \App\Mail\FreeSubscriptionNotification($fdSubscription, 9, $route_name));
                        }
                    }
                }
            }
        }
    }
}
