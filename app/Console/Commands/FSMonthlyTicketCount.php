<?php

namespace App\Console\Commands;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class FSMonthlyTicketCount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fsmonthly:ticketcount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To update the monthly ticket count of freshservice subscribed users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();
        foreach ($users as $user){
            if (!$user->hasRole('admin')){
                if ($user->meta->subscribed(config('plans.subscriptions.freshservice.subscription_name'))){
                    $freshserviceClient= $user->freshserviceClients;
                    $fsSubscription = $user->meta->subscription(config('plans.subscriptions.freshservice.subscription_name'));
                    if ($fsSubscription->plan_name != 'free'){
                        $subscriptions = $user->meta->getAllSubscription(config('plans.subscriptions.freshservice.subscription_name'));
                        if (count($subscriptions) != 2){
                            continue;
                        }
                        foreach ($subscriptions as $subscription){
                            $stripesubscription = $subscription->asStripeSubscription();
                            if ($stripesubscription['plan']['usage_type'] == 'metered'){
                                $date = date('Y-m-d', strtotime('-1 day',$stripesubscription->current_period_start));
                                Config::set('database.connections.tenant.database', $freshserviceClient->organization_db_name);
                                $avgTicketCount = DB::connection('tenant')->select('select count(ticket_id) as Ticket_Count from tickets where active=1 and created_at>= :date',['date' => $date])[0]->Ticket_Count;
                                DB::purge('tenant');

                                if ($avgTicketCount>0){
                                    $subscription->updateMeteredUsage(round($avgTicketCount/100));
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
