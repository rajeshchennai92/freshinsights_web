<?php

namespace App\Console\Commands;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class FreeFSSubscriptionNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'freefs:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Freshservice free subscription notification mail';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();
        foreach ($users as $user){
            if (!$user->hasRole('admin')){
                if ($user->meta->subscribed(config('plans.subscriptions.freshservice.subscription_name'))){
                    $fsSubscription = $user->meta->subscription(config('plans.subscriptions.freshservice.subscription_name'));
                    if ($fsSubscription->plan_name == 'free'){
                        $date = Carbon::parse($fsSubscription->ends_at);
                        $current_date = Carbon::now();
                        $route_name = 'freshserviceSubscriptionChoose';
                        $one_day = $current_date->copy()->addDays(1);
                        $three_days = $current_date->copy()->addDays(5);
                        $seven_days = $current_date->copy()->addDays(9);
                        if ($one_day->isSameDay($date)){
                            Mail::to($user)->send(new \App\Mail\FreeSubscriptionNotification($fsSubscription,1, $route_name));
                        }
                        if ($three_days->isSameDay($date)){
                            Mail::to($user)->send(new \App\Mail\FreeSubscriptionNotification($fsSubscription,5, $route_name));
                        }
                        if ($seven_days->isSameDay($date)){
                            Mail::to($user)->send(new \App\Mail\FreeSubscriptionNotification($fsSubscription,9, $route_name));
                        }
                    }
                }
            }
        }
    }
}
