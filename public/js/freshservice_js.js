$(document).ready(function(){
    var today = new Date();
    $("#registerFsButton").prop("disabled",true);
    $("#registerFreshservice input[name='organization_url'],#registerFreshservice input[name='organization_api_key']").keyup(function(){
        $("#registerFsButton").prop("disabled",true);
        $('[data-toggle="tooltip"]').tooltip('enable');
    });
    $('#datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
        endDate: today,
    });
    //$('#datepicker').datepicker('setDate','now');

    $('#fs-monthly .pricing-choose').click(function() {
        $('#fs-monthly .pricing-choose').removeClass('transform_div');
        $('#fs-monthly .pricing-choose').addClass('highlight_div');
        $(this).addClass('transform_div');
        $(this).removeClass('highlight_div');
    });
    $('#fs-annually .pricing-choose').click(function() {
        $('#fs-annually .pricing-choose').removeClass('transform_div');
        $('#fs-annually .pricing-choose').addClass('highlight_div');
        $(this).addClass('transform_div');
        $(this).removeClass('highlight_div');
    });

    $('#fs-monthly-tab').click(function() {
        $('#fs-monthly .pricing-choose').removeClass('transform_div');
        $('#fs-monthly .pricing-choose').addClass('highlight_div');
        $('#fs_plan_id').val('garden');
        $('#fs_stripe_plan_id').val('garden');
        $('#fs_plan_interval').val('month');
        $('#fs_plan_amount').val('159');
        $('#fs_subscribe_confirm').text('Subscribe to Garden');
        $('#fs-monthly .garden').addClass('transform_div');
        $('#fs-monthly .garden').removeClass('highlight_div');
    });
    $('#fs-annually-tab').click(function() {
        $('#fs-annually .pricing-choose').removeClass('transform_div');
        $('#fs-annually .pricing-choose').addClass('highlight_div');
        $('#fs_plan_id').val('garden');
        $('#fs_stripe_plan_id').val('annual_garden');
        $('#fs_plan_amount').val('1749');
        $('#fs_plan_interval').val('annual');
        $('#fs_subscribe_confirm').text('Subscribe to Garden');
        $('#fs-annually .garden').addClass('transform_div');
        $('#fs-annually .garden').removeClass('highlight_div');
    });

});

$(function () {
    if ($('#registerFsButton').is(':disabled')){
        $('[data-toggle="tooltip"]').tooltip('enable');
    }
    $('[data-toggle="tooltip"]').tooltip('enable');
})

function testFsConnection(e) {
    e.preventDefault();
    var freshservice_url = $('#registerFreshservice input[name = "organization_url"]').val();
    var organization_api_key = $('#registerFreshservice input[name = "organization_api_key"]').val();
    $('#registerFreshservice .org_url_error').text('');
    $('#registerFreshservice .org_api_error').text('');
    $('#registerFreshservice .org_api_success').text('');
    $('#spinner').show();
    if (freshservice_url != '' && organization_api_key != ''){
        $.ajaxSetup({
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content') );
            }
        });
        var saveData = $.ajax({
            type: 'POST',
            url: "/test-connection",
            data: {'freshservice_url':freshservice_url,'organization_api_key':organization_api_key,'org':2},
            dataType: "text",
            success: function(resultData) {
                $('#spinner').hide();
                var result = JSON.parse(resultData);
                if (result.response == 'success' && result.details == 1){
                    $("#registerFsButton").prop("disabled",false);
                    $('#registerFreshservice .org_api_success').text('connection succeeded');
                    $('[data-toggle="tooltip"]').tooltip('disable');
                }
                else if(result.response == 'error' && result.details == 0){
                    $('#registerFreshservice .org_api_error').text('The API key is invalid');
                }
                else{
//$("#registerFsButton").prop("disabled",false);
 //$('#registerFreshservice .org_api_success').text('The connection is success');
                    $('#registerFreshservice .org_url_error').text(result.details.freshservice_url[0]?result.details.freshservice_url[0]:'');
                    $('#registerFreshservice .org_api_error').text(result.details.organization_api_key[0]? result.details.organization_api_key[0]:'');
                }
            }
        });
    }
    else {
        $('#spinner').hide();
        $('#registerFreshservice .org_url_error').text(freshservice_url != ''? '':'Pleae enter the Helpdesk URL');
        $('#registerFreshservice .org_api_error').text(organization_api_key != ''? '':'Pleae enter the API key');
    }
}

function setFSPlantoId(plan, plan_name,plan_amount, stripe_plan) {
    $('#fs_plan_id').val(plan);
    $('#fs_plan_amount').val(plan_amount);
    $('#fs_stripe_plan_id').val(stripe_plan);
    $('#fs_subscribe_confirm').text('Subscribe to '+plan_name);
}

function setFSPlanDetails() {
    var plan = $('#fs_plan_id').val();
    var stripe_plan = $('#fs_stripe_plan_id').val();
    var plan_amount = $('#fs_plan_amount').val();
    var plan_interval = $('#fs_plan_interval').val();
    var setup_fee = $('.setup_fee_calc_'+plan_amount+':first').html();
    $('#spinner1').show();
    if (plan != '' && setup_fee != ''){
        $.ajaxSetup({
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content') );
            }
        });
        var saveData = $.ajax({
            type: 'POST',
            url: "/freshservice/selected_plan_details",
            data: {'plan':plan, 'setup_fee': setup_fee, 'plan_amount' : plan_amount, 'stripe_plan' : stripe_plan, 'plan_interval':plan_interval},
            dataType: "text",
            success: function(resultData) {
                $('#spinner1').hide();
                var result = JSON.parse(resultData);
                window.location.href = result.url;
            }
        });
    }
    else {
        $('#spinner1').hide();
        alert('You have to select the plan');
    }
}
function setFSPlanDetailsTableau() {
    var plan = $('#fs_plan_id').val();
    var stripe_plan = $('#fs_stripe_plan_id').val();
    var plan_amount = $('#fs_plan_amount').val();
    var plan_interval = $('#fs_plan_interval').val();
    var setup_fee = $('.setup_fee_calc_'+plan_amount+':first').html();
    $('#spinner1').show();
    if (plan != '' && setup_fee != ''){
        $.ajaxSetup({
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content') );
            }
        });
        var saveData = $.ajax({
            type: 'POST',
            url: "/freshservice/selected_plan_details_tableau",
            data: {'plan':plan, 'setup_fee': setup_fee, 'plan_amount' : plan_amount, 'stripe_plan' : stripe_plan, 'plan_interval':plan_interval},
            dataType: "text",
            success: function(resultData) {
                $('#spinner1').hide();
                var result = JSON.parse(resultData);
                window.location.href = result.url;
            }
        });
    }
    else {
        $('#spinner1').hide();
        alert('You have to select the plan');
    }
}

function subscribeFreshserviceFreePlan(e) {
    e.preventDefault();
    $('.subscribe_btn').prop('disabled', true);
    var plan = $('#plan_id').val();
    var plan_interval = $('#plan_interval').val();
    $('#spinner').show();
    $.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content') );
        }
    });
    var saveData = $.ajax({
        type: 'POST',
        url: "/subscribe/free/freshservice",
        data: {'plan':plan, 'plan_interval' : plan_interval},
        dataType: "text",
        success: function(resultData) {
            $('#spinner').hide();
            var result = JSON.parse(resultData);
            console.log(result);
            if (result.response == 'success' && result.details == 1){
                console.log('success');
                window.location.href = result.url;
            }
            else if(result.response == 'error'){
                alert(result.details);
            }
            else{
                alert('Something went wrong. Please try again later')
            }
        }
    });
}

function subscribeFreshservicePlan(e) {
    e.preventDefault();
    var terms = $("#terms_check").is(":checked");
    if (!terms){
        alert('Please accept the Terms of Service');
        return false;
    }
    $('#spinner').show();
    $('.subscribe_btn').prop('disabled', true);
    $form = $('form.card-form');
    $form.find('.payment-errors').text('');

    var _month = $("#expiry_date").val().substring(0, 2);
    var _year = $("#expiry_date").val().substring($("#expiry_date").val().indexOf("/")+2, $("#expiry_date").val().length);

    $('#exp_month').val(_month);
    $('#exp_year').val(_year);

    Stripe.card.createToken($form, stripeFreshserviceResponseHandler);
}

function subscribeFreshserviceCallack() {
    var plan = $('#plan_id').val();
    var stripe_plan = $('#stripe_plan_id').val();
    var stripeToken = $('#stripeToken').val();
    var plan_interval = $('#plan_interval').val();
    var historical_payment = $("#historical_payment").is(":checked");
    if (stripeToken){
        $.ajaxSetup({
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content') );
            }
        });
        var saveData = $.ajax({
            type: 'POST',
            url: "/subscribe/freshservice",
            data: {'plan':plan,'stripeToken':stripeToken, 'stripe_plan' :stripe_plan, 'plan_interval': plan_interval,'historical_payment':historical_payment},
            dataType: "text",
            success: function(resultData) {
                $('#spinner').hide();
                var result = JSON.parse(resultData);
                console.log(result);
                if (result.response == 'success' && result.details == 1){
                    console.log('success');
                    window.location.href = result.url;
                }
                else if(result.response == 'success' && result.details == 0){
                    $('#cardModal').modal({
                        keyboard: false,
                        backdrop: 'static'
                    });
                    $('.subscribe_btn').prop('disabled', false);
                }
                else if(result.response == 'error'){
                    alert(result.details);
                }
                else{
                    alert('Something went wrong. Please try again later')
                }
            }
        });
    }
    else {
        $('.subscribe_btn').prop('disabled', false);
    }
}

var stripeFreshserviceResponseHandler = function(status, response) {
    var $form = $('form.card-form');

    if (response.error) {
        $form.find('.payment-errors').text(response.error.message);
        $('.subscribe_btn').prop('disabled', false);
        $('#spinner').hide();
    } else {
        var token = response.id;
        $form.append($('<input type="hidden" id="stripeToken" />').val(token));
        subscribeFreshserviceCallack();
    }
};
