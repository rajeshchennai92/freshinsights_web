$(document).ready(function(){
    var today = new Date();
    $("#registerButton").prop("disabled",true);
    $("#registerFreshdesk input[name='organization_url'],#registerFreshdesk input[name='organization_api_key']").keyup(function(){
        $("#registerButton").prop("disabled",true);
        $('[data-toggle="tooltip"]').tooltip('enable');
    });
    $('#datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
        endDate: today,
    });
    //$('#datepicker').datepicker('setDate','now');

    $(function() {
        var $a = $(".tab-li");
        $a.click(function() {
            $a.removeClass("active");
            $(this).addClass("active");
            $(this).find("a")[0].click();
        });
    });

    $('#monthly .pricing-choose').click(function() {
        $('#monthly .pricing-choose').removeClass('transform_div');
        $('#monthly .pricing-choose').addClass('highlight_div');
        $(this).addClass('transform_div');
        $(this).removeClass('highlight_div');
    });
    $('#annually .pricing-choose').click(function() {
        $('#annually .pricing-choose').removeClass('transform_div');
        $('#annually .pricing-choose').addClass('highlight_div');
        $(this).addClass('transform_div');
        $(this).removeClass('highlight_div');
    });

    $('#monthly-tab').click(function() {
        $('#monthly .pricing-choose').removeClass('transform_div');
        $('#monthly .pricing-choose').addClass('highlight_div');
        $('#plan_id').val('premium');
        $('#stripe_plan_id').val('premium');
        $('#plan_interval').val('month');
        $('#plan_amount').val('69');
        $('#subscribe_confirm').text('Subscribe to Premium');
        $('#monthly .premium').addClass('transform_div');
        $('#monthly .premium').removeClass('highlight_div');
    });
    $('#annually-tab').click(function() {
        $('#annually .pricing-choose').removeClass('transform_div');
        $('#annually .pricing-choose').addClass('highlight_div');
        $('#plan_id').val('premium');
        $('#stripe_plan_id').val('annual_premium');
        $('#plan_amount').val('269');
        $('#plan_interval').val('annual');
        $('#subscribe_confirm').text('Subscribe to Premium');
        $('#annually .premium').addClass('transform_div');
        $('#annually .premium').removeClass('highlight_div');
    });

});

$(function () {
    if ($('#registerButton').is(':disabled')){
        $('[data-toggle="tooltip"]').tooltip('enable');
    }
    $('[data-toggle="tooltip"]').tooltip('enable');
})

function testConnection(e) {
    e.preventDefault();
    var freshdesk_url = $('#registerFreshdesk input[name = "organization_url"]').val();
    var organization_api_key = $('#registerFreshdesk input[name = "organization_api_key"]').val();
    $('#registerFreshdesk .org_url_error').text('');
    $('#registerFreshdesk .org_api_error').text('');
    $('#registerFreshdesk .org_api_success').text('');
    $('#spinner').show();
    if (freshdesk_url != '' && organization_api_key != ''){
        $.ajaxSetup({
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content') );
            }
        });
        var saveData = $.ajax({
            type: 'POST',
            url: "/test-connection",
            data: {'freshdesk_url':freshdesk_url,'organization_api_key':organization_api_key,'org':1},
            dataType: "text",
            success: function(resultData) {
                $('#spinner').hide();
                var result = JSON.parse(resultData);
                if (result.response == 'success' && result.details == 1){
                    $("#registerButton").prop("disabled",false);
                    $('#registerFreshdesk .org_api_success').text('connection succeeded');
                    $('[data-toggle="tooltip"]').tooltip('disable');
                }
                else if(result.response == 'error' && result.details == 0){
                    $('#registerFreshdesk .org_api_error').text('The API key is invalid');
                }
                else{
                    $('#registerFreshdesk .org_url_error').text(result.details.freshdesk_url[0]?result.details.freshdesk_url[0]:'');
                    $('#registerFreshdesk .org_api_error').text(result.details.organization_api_key[0]? result.details.organization_api_key[0]:'');
                }
            }
        });
    }
    else {
        $('#spinner').hide();
        $('#registerFreshdesk .org_url_error').text(freshdesk_url != ''? '':'Pleae enter the Helpdesk URL');
        $('#registerFreshdesk .org_api_error').text(organization_api_key != ''? '':'Pleae enter the API key');
    }
}

function calculatePriceOnConfirm() {
    var date = $('#datepicker').val();
    if (date != ''){
        $('#spinner').show();
        $.ajaxSetup({
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content') );
            }
        });
        var saveData = $.ajax({
            type: 'POST',
            url: "/freshdesk/ticket_count",
            data: {'date':date},
            dataType: "text",
            success: function(resultData) {
                var result = JSON.parse(resultData);
                if (result.response == 'success' && result.details == 1){
                    $('#avgTicketCount').val(result.avgTicketCount);
                    for (var key in result.setup_fee) {
                        $('.'+key).html(result.setup_fee[key]);
                    }
                    $('.setup_fee_calc_0').html(0);

                }
                else if(result.response == 'success' && result.details == 0){
                    alert('You have to register before subscribe')
                }
                else{
                    alert('Error processsing the request Please try again later')
                }
                $('#spinner').hide();
            }
        });
    }
}

function setPlanDetails() {
    var date = $('#datepicker').val();
    var avgTicketCount = $('#avgTicketCount').val();
    var plan = $('#plan_id').val();
    var stripe_plan = $('#stripe_plan_id').val();
    var plan_amount = $('#plan_amount').val();
    var plan_interval = $('#plan_interval').val();
    if (plan == 'free'){
        date = new Date().toLocaleDateString();
    }
    var setup_fee = $('.setup_fee_calc_'+plan_amount+':first').html();
    $('#spinner1').show();
    if (date != '' && avgTicketCount != '' && plan != '' && setup_fee != ''){
        $.ajaxSetup({
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content') );
            }
        });
        var saveData = $.ajax({
            type: 'POST',
            url: "/freshdesk/selected_plan_details",
            data: {'date':date, 'avgTicketCount':avgTicketCount, 'plan':plan, 'setup_fee': setup_fee, 'plan_amount' : plan_amount, 'stripe_plan' : stripe_plan, 'plan_interval':plan_interval},
            dataType: "text",
            success: function(resultData) {
                $('#spinner1').hide();
                var result = JSON.parse(resultData);
                window.location.href = result.url;
            }
        });
    }
    else {
        $('#spinner1').hide();
        alert('You have to select the date and plan');
    }
}

function setPlanDetailsTableau() {
    var date = $('#datepicker').val();
    var avgTicketCount = $('#avgTicketCount').val();
    var plan = $('#plan_id').val();
    var stripe_plan = $('#stripe_plan_id').val();
    var plan_amount = $('#plan_amount').val();
    var plan_interval = $('#plan_interval').val();
    if (plan == 'free'){
        date = new Date().toLocaleDateString();
    }
    var setup_fee = $('.setup_fee_calc_'+plan_amount+':first').html();
    $('#spinner1').show();
    if (date != '' && avgTicketCount != '' && plan != '' && setup_fee != ''){
        $.ajaxSetup({
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content') );
            }
        });
        var saveData = $.ajax({
            type: 'POST',
            url: "/freshdesk/selected_plan_details_tableau",
            data: {'date':date, 'avgTicketCount':avgTicketCount, 'plan':plan, 'setup_fee': setup_fee, 'plan_amount' : plan_amount, 'stripe_plan' : stripe_plan, 'plan_interval':plan_interval},
            dataType: "text",
            success: function(resultData) {
                $('#spinner1').hide();
                var result = JSON.parse(resultData);
                window.location.href = result.url;
            }
        });
    }
    else {
        $('#spinner1').hide();
        alert('You have to select the date and plan');
    }
}

function showFreshdeskPrice(plan) {
    $('.pricing_btn').prop('disabled', true);
    $('.subscribe_btn').prop('disabled', true);
    $.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content') );
        }
    });
    var saveData = $.ajax({
        type: 'POST',
        url: "/freshdesk/subscription-fees",
        data: {'plan':plan},
        dataType: "text",
        success: function(resultData) {
            var result = JSON.parse(resultData);
            console.log(result);
            if (result.response == 'success' && result.details == 1){
                $('#pricingForm').show();
                $('#pricingForm .plan_id').val(plan);
                $('#pricingForm #avgTicketCount').val(result.avgTicketCount);
                $('#pricingForm #monthlyTicketCount').val(result.monthlyTicketCount);
                $('#pricingForm #setup_fee_calc').html(result.setup_fee);
                $('#pricingForm #monthly_fee_calc').html(result.plan_amount);
            }
            else if(result.response == 'success' && result.details == 0){
                $('#feshdeskDetailsForm').show();
            }
            else{
                alert('problem wih api request try after sometimes');
            }
            $('.pricing_btn').prop('disabled', false);
            $('.subscribe_btn').prop('disabled', false);
        }
    });
}



// function showPriceCalc(plan, plan_amount, setup_fee, additional_fee, allowed_ticket_count) {
//     $('#pricingCalcForm').show();
//     $('#pricingCalcForm .plan_id').val(plan);
//     $('#pricingCalcForm .plan_amount').val(plan_amount);
//     $('#pricingCalcForm .setup_fee').val(setup_fee);
//     $('#pricingCalcForm .additional_fee').val(additional_fee);
//     $('#pricingCalcForm .allowed_ticket_count').val(allowed_ticket_count);
//     $('#pricingCalcForm #setup_fee_calc').html(setup_fee);
//     $('#pricingCalcForm #monthly_fee_calc').html(plan_amount);
//     $('#pricingCalcForm #avgTicketCount').val(10000);
//     $('#pricingCalcForm #monthlyTicketCount').val(allowed_ticket_count);
// }

// function calculatePrice() {
// //     var plan_amount = $('#pricingCalcForm .plan_amount').val();
// //     var setup_fee = $('#pricingCalcForm .setup_fee').val();
// //     var additional_fee = $('#pricingCalcForm .additional_fee').val();
// //     var allowed_ticket_count = $('#pricingCalcForm .allowed_ticket_count').val();
// //     var avgTicketCount = $('#pricingCalcForm #avgTicketCount').val();
// //     var monthlyTicketCount = $('#pricingCalcForm #monthlyTicketCount').val();
// //     if (avgTicketCount>=10000){
// //         setup_fee = +setup_fee + +(Math.round((avgTicketCount-10000)/10000)*10);
// //         $('#pricingCalcForm #setup_fee_calc').html(setup_fee);
// //     }
// //     else {
// //         $('#pricingCalcForm #setup_fee_calc').html(setup_fee);
// //     }
// //     if (+monthlyTicketCount>=+allowed_ticket_count){
// //         plan_amount = +plan_amount + +(Math.round((monthlyTicketCount-allowed_ticket_count)*additional_fee));
// //         $('#pricingCalcForm #monthly_fee_calc').html(plan_amount);
// //     }
// //     else {
// //         $('#pricingCalcForm #monthly_fee_calc').html(plan_amount);
// //     }
// // }

// function showRegisterForm(e,app) {
//     e.preventDefault();
//     if (app == 'freshdesk'){
//         $('#pricingCalcForm').show();
//     }
//     else {
//         $('#pricingCalcForm').show();
//     }
// }

function closeCardForm() {
    $('.card-form')[0].reset();
    $('.card-form').find('.payment-errors').text('');
    $(".jp-card-number").html("**** **** **** ****");
    $(".jp-card-name").html("Jose da Silva");
    $(".jp-card-expiry").html("**/****");
    $(".jp-card-cvc").html("***");
    $(".jp-card").removeClass().addClass('jp-card jp-card-unknown');
    $("#card_number").removeClass().addClass('form-control');
    $("#card_holder_name").removeClass().addClass('form-control');
    $("#expiry_date").removeClass().addClass('form-control');
    $("#cvv_number").removeClass().addClass('form-control');
    $('.subscribe_btn').prop('disabled', false);
}

function showCardorSubscribe(plan) {
    $('.subscribe_btn').prop('disabled', true);
    var plan = plan;
    $('#plan_id').val(plan);
    var date = $('#datepicker').val();
    $.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content') );
        }
    });
    var saveData = $.ajax({
        type: 'POST',
        url: "/subscribe/freshdesk",
        data: {'plan':plan, 'date' : date},
        dataType: "text",
        success: function(resultData) {
            var result = JSON.parse(resultData);
            console.log(result);
            if (result.response == 'success' && result.details == 1){
                console.log('success');
                window.location.href = result.url;
            }
            else if(result.response == 'success' && result.details == 0){
                $('#cardModal').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
                $('.subscribe_btn').prop('disabled', false);
            }
            else if(result.response == 'error'){
                alert(result.details);
            }
            else{
                alert('Something went wrong. Please try again later')
            }
        }
    });
}

function subscribeFreshdeskFreePlan(e) {
    e.preventDefault();
    $('.subscribe_btn').prop('disabled', true);
    var plan = $('#plan_id').val();
    var plan_interval = $('#plan_interval').val();
    var date = $('#datepicker').val();
    $('#spinner').show();
    $.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content') );
        }
    });
    var saveData = $.ajax({
        type: 'POST',
        url: "/subscribe/free/freshdesk",
        data: {'plan':plan, 'date' : date, 'plan_interval' : plan_interval},
        dataType: "text",
        success: function(resultData) {
            $('#spinner').hide();
            var result = JSON.parse(resultData);
            console.log(result);
            if (result.response == 'success' && result.details == 1){
                console.log('success');
                window.location.href = result.url;
            }
            else if(result.response == 'error'){
                alert(result.details);
            }
            else{
                alert('Something went wrong. Please try again later')
            }
        }
    });
}

function subscribeFreshdeskPlan(e) {
    e.preventDefault();
    var terms = $("#terms_check").is(":checked");
    if (!terms){
        alert('Please accept the Terms of Service');
        return false;
    }
    $('#spinner').show();
    $('.subscribe_btn').prop('disabled', true);
    $form = $('form.card-form');
    $form.find('.payment-errors').text('');

    var _month = $("#expiry_date").val().substring(0, 2);
    var _year = $("#expiry_date").val().substring($("#expiry_date").val().indexOf("/")+2, $("#expiry_date").val().length);

    $('#exp_month').val(_month);
    $('#exp_year').val(_year);

    Stripe.card.createToken($form, stripeResponseHandler);
}

function subscribeFreshdeskCallack() {
    var plan = $('#plan_id').val();
    var stripe_plan = $('#stripe_plan_id').val();
    var date = $('#datepicker').val();
    var stripeToken = $('#stripeToken').val();
    var plan_interval = $('#plan_interval').val();
    if (stripeToken){
        $.ajaxSetup({
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content') );
            }
        });
        var saveData = $.ajax({
            type: 'POST',
            url: "/subscribe/freshdesk",
            data: {'plan':plan,'stripeToken':stripeToken, 'date' : date, 'stripe_plan' :stripe_plan, 'plan_interval': plan_interval},
            dataType: "text",
            success: function(resultData) {
                $('#spinner').hide();
                var result = JSON.parse(resultData);
                console.log(result);
                if (result.response == 'success' && result.details == 1){
                    console.log('success');
                    window.location.href = result.url;
                }
                else if(result.response == 'success' && result.details == 0){
                    $('#cardModal').modal({
                        keyboard: false,
                        backdrop: 'static'
                    });
                    $('.subscribe_btn').prop('disabled', false);
                }
                else if(result.response == 'error'){
                    alert(result.details);
                }
                else{
                    alert('Something went wrong. Please try again later')
                }
            }
        });
    }
    else {
        $('.subscribe_btn').prop('disabled', false);
    }
}

var stripeResponseHandler = function(status, response) {
    var $form = $('form.card-form');

    if (response.error) {
        $form.find('.payment-errors').text(response.error.message);
        $('.subscribe_btn').prop('disabled', false);
        $('#spinner').hide();
    } else {
        var token = response.id;
        $form.append($('<input type="hidden" id="stripeToken" />').val(token));
        subscribeFreshdeskCallack();
    }
};

function changeFreshdeskPlan(e) {
    e.preventDefault();
    var r = confirm("Are you sure want to continue?");
    if (r == false) {
        return false;
    }
    $('.subscribe_btn').prop('disabled', true);
    var plan = $('select[name="change_plan"] option:selected').val();
    $('select[name="change_plan"]').prop('disabled', true);
    $('#spinner_change_plan').show();
    $.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content') );
        }
    });
    var saveData = $.ajax({
        type: 'POST',
        url: "/modify_plan/freshdesk",
        data: {'plan':plan},
        dataType: "text",
        success: function(resultData) {
            $('#spinner_change_plan').hide();
            var result = JSON.parse(resultData);
            console.log(result);
            if (result.response == 'success' && result.details == 1){
                $("#fd_plan_name").text(result.plan_name);
                $("#change_plan_success").fadeTo(2000, 500).slideUp(500, function(){
                    $("#change_plan_success").slideUp(500);
                });
                $('.subscribe_btn').prop('disabled', false);
                $('select[name="change_plan"]').prop('disabled', false);
                $('#spinner_change_plan').hide();
                window.location.reload();
            }
            else if(result.response == 'success' && result.details == 0){
                $('#cardModal').modal('show');
                $('.modify_plan_btn').show();
                $('.change_card_btn').hide();
            }
            else if(result.response == 'error'){
                $("#change_plan_error").text(result.details);
                $("#change_plan_danger").fadeTo(2000, 500).slideUp(500, function(){
                    $("#change_plan_danger").slideUp(500);
                });
                $('.subscribe_btn').prop('disabled', false);
                $('select[name="change_plan"]').prop('disabled', false);
                $('#spinner_change_plan').hide();
            }
            else{
                $("#change_plan_error").text('Something went wrong. Please try again later');
                $("#change_plan_danger").fadeTo(2000, 500).slideUp(500, function(){
                    $("#change_plan_danger").slideUp(500);
                });
                $('.subscribe_btn').prop('disabled', false);
                $('select[name="change_plan"]').prop('disabled', false);
                $('#spinner_change_plan').hide();
            }
        }
    });
}

function changeCardDetails(e) {
    e.preventDefault();
    $('.subscribe_btn').prop('disabled', true);
    $form = $('form.card-form');
    $form.find('.payment-errors').text('');

    var _month = $("#expiry_date").val().substring(0, 2);
    var _year = $("#expiry_date").val().substring($("#expiry_date").val().indexOf("/")+2, $("#expiry_date").val().length);

    $('#exp_month').val(_month);
    $('#exp_year').val(_year);

    Stripe.card.createToken($form, stripeResponseHandlerForChangeCard);
}

var stripeResponseHandlerForChangeCard = function(status, response) {
    var $form = $('form.card-form');

    if (response.error) {
        $form.find('.payment-errors').text(response.error.message);
        $('.subscribe_btn').prop('disabled', false);
    } else {
        var token = response.id;
        $form.append($('<input type="hidden" id="stripeToken" />').val(token));
        changeCardDetailsAjax();
    }
};

function changeCardDetailsAjax() {
    var stripeToken = $('#stripeToken').val();
    if (stripeToken){
        $.ajaxSetup({
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content') );
            }
        });
        var saveData = $.ajax({
            type: 'POST',
            url: "/user/billing/change-card",
            data: {'stripeToken':stripeToken},
            dataType: "text",
            success: function(resultData) {
                var result = JSON.parse(resultData);
                if (result.response == 'success' && result.details == 1){
                    console.log('success');
                    closeCardForm();
                    $('#cardModal').modal('toggle');
                    $("#change_card_success").fadeTo(2000, 500).slideUp(500, function(){
                        $("#change_card_success").slideUp(500);
                    });
                    $('.subscribe_btn').prop('disabled', false);
                }
                else if(result.response == 'error'){
                    $form.find('.payment-errors').text(result.details);
                    $('.subscribe_btn').prop('disabled', false);
                }
                else{
                    $form.find('.payment-errors').text(result.details);
                    $('.subscribe_btn').prop('disabled', false);
                }
            }
        });
    }
    else {
        $('.subscribe_btn').prop('disabled', false);
    }
}

$(function(){
    $('form.card-form').card({
        form: 'form',
        container: '.card-wrapper',
        placeholders: {
            number: '**** **** **** ****',
            name: 'Arya Stark',
            expiry: '**/****',
            cvc: '***'
        },
        formSelectors: { /* Defaults use name= attributes, not available per PCI compliance */
            numberInput: 'input#card_number',
            expiryInput: 'input#expiry_date',
            cvcInput: 'input#cvv_number',
            nameInput: 'input#card_holder_name'
        }
    });

    jQuery(function($) {
        $('.jp-card').css({
            'backface-visibility': 'hidden'
        });
    });
});

function showPriceCalc(planObj) {
    $('#tabs a[href="#tab_' + planObj.value + '"]').tab('show');
}

function setPlantoId(plan, plan_name,plan_amount, stripe_plan) {
    $('#plan_id').val(plan);
    $('#plan_amount').val(plan_amount);
    $('#stripe_plan_id').val(stripe_plan);
    if (plan == 'free'){
        $("#datepicker_div").hide();
        $("#avgTicketCount_div").hide();
    }
    else {
        $("#datepicker_div").show();
        $("#avgTicketCount_div").show();
    }
    $('#subscribe_confirm').text('Subscribe to '+plan_name);
}

$(function() {
    var plan = $('#tabs select').val();
    $('#tabs a[href="#tab_' + plan + '"]').tab('show');
});

function changeFdFreePlan(e) {
    e.preventDefault();
    $('.modify_plan_btn').prop('disabled', true);
    $form = $('form.card-form');
    $form.find('.payment-errors').text('');

    var _month = $("#expiry_date").val().substring(0, 2);
    var _year = $("#expiry_date").val().substring($("#expiry_date").val().indexOf("/")+2, $("#expiry_date").val().length);

    $('#exp_month').val(_month);
    $('#exp_year').val(_year);

    Stripe.card.createToken($form, changeFdFreePlanstripeResponseHandler);
}

function changeFdFreePlanCallack() {
    var plan = $('select[name="change_plan"] option:selected').val();
    var date = $('#datepicker').val();
    if (date == ''){
        $('#cardModal').modal('toggle');
        $('.modify_plan_btn').prop('disabled', false);
        $('.subscribe_btn').prop('disabled', false);
        $('select[name="change_plan"]').prop('disabled', false);
        $('#spinner_change_plan').hide();
        alert('Select the date to load tickets from');
        return false;
    }
    var stripeToken = $('#stripeToken').val();
    if (stripeToken){
        $.ajaxSetup({
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content') );
            }
        });
        var saveData = $.ajax({
            type: 'POST',
            url: "/modify_plan/freshdesk/free",
            data: {'plan':plan,'stripeToken':stripeToken, 'date':date},
            dataType: "text",
            success: function(resultData) {
                var result = JSON.parse(resultData);
                console.log(result);
                if (result.response == 'success' && result.details == 1){
                    $('#cardModal').modal('toggle');
                    $("#fd_plan_name").text(result.plan_name);
                    $("#change_plan_success").fadeTo(2000, 500).slideUp(500, function(){
                        $("#change_plan_success").slideUp(500);
                    });
                    $('.subscribe_btn').prop('disabled', false);
                    $('.modify_plan_btn').prop('disabled', false);
                    $('select[name="change_plan"]').prop('disabled', false);
                    $('#spinner_change_plan').hide();
                    window.location.reload();
                }
                else if(result.response == 'error'){
                    $('#cardModal').modal('toggle');
                    $("#change_plan_error").text('Something went wrong. Please try again later');
                    $("#change_plan_danger").fadeTo(2000, 500).slideUp(500, function(){
                        $("#change_plan_danger").slideUp(500);
                    });
                    $('.subscribe_btn').prop('disabled', false);
                    $('.modify_plan_btn').prop('disabled', false);
                    $('select[name="change_plan"]').prop('disabled', false);
                    $('#spinner_change_plan').hide();
                }
                else{
                    $('#cardModal').modal('toggle');
                    $("#change_plan_error").text('Something went wrong. Please try again later');
                    $("#change_plan_danger").fadeTo(2000, 500).slideUp(500, function(){
                        $("#change_plan_danger").slideUp(500);
                    });
                    $('.subscribe_btn').prop('disabled', false);
                    $('.modify_plan_btn').prop('disabled', false);
                    $('select[name="change_plan"]').prop('disabled', false);
                    $('#spinner_change_plan').hide();
                }
            }
        });
    }
    else {
        $('.subscribe_btn').prop('disabled', false);
        $('.modify_plan_btn').prop('disabled', false);
        $('select[name="change_plan"]').prop('disabled', false);
        $('#spinner_change_plan').hide();
    }
}

var changeFdFreePlanstripeResponseHandler = function(status, response) {
    var $form = $('form.card-form');

    if (response.error) {
        $form.find('.payment-errors').text(response.error.message);
        $('.modify_plan_btn').prop('disabled', false);
        $('#spinner').hide();
    } else {
        var token = response.id;
        $form.append($('<input type="hidden" id="stripeToken" />').val(token));
        changeFdFreePlanCallack();
    }
};