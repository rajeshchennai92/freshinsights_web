(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["screen1-screen1-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/layout/screen1/screen1.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/layout/screen1/screen1.component.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>\n  screen1 works!\n</p>\n");

/***/ }),

/***/ "./src/app/layout/screen1/screen1-routing.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/layout/screen1/screen1-routing.module.ts ***!
  \**********************************************************/
/*! exports provided: Screen1RoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Screen1RoutingModule", function() { return Screen1RoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _screen1_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./screen1.component */ "./src/app/layout/screen1/screen1.component.ts");




const routes = [
    {
        path: '',
        component: _screen1_component__WEBPACK_IMPORTED_MODULE_3__["Screen1Component"]
    }
];
let Screen1RoutingModule = class Screen1RoutingModule {
};
Screen1RoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], Screen1RoutingModule);



/***/ }),

/***/ "./src/app/layout/screen1/screen1.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/layout/screen1/screen1.component.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9zY3JlZW4xL3NjcmVlbjEuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/layout/screen1/screen1.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/layout/screen1/screen1.component.ts ***!
  \*****************************************************/
/*! exports provided: Screen1Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Screen1Component", function() { return Screen1Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let Screen1Component = class Screen1Component {
    constructor() { }
    ngOnInit() {
    }
};
Screen1Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-screen1',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./screen1.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/layout/screen1/screen1.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./screen1.component.scss */ "./src/app/layout/screen1/screen1.component.scss")).default]
    })
], Screen1Component);



/***/ }),

/***/ "./src/app/layout/screen1/screen1.module.ts":
/*!**************************************************!*\
  !*** ./src/app/layout/screen1/screen1.module.ts ***!
  \**************************************************/
/*! exports provided: Screen1Module */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Screen1Module", function() { return Screen1Module; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _screen1_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./screen1.component */ "./src/app/layout/screen1/screen1.component.ts");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
/* harmony import */ var _screen1_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./screen1-routing.module */ "./src/app/layout/screen1/screen1-routing.module.ts");






let Screen1Module = class Screen1Module {
};
Screen1Module = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_screen1_component__WEBPACK_IMPORTED_MODULE_3__["Screen1Component"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _screen1_routing_module__WEBPACK_IMPORTED_MODULE_5__["Screen1RoutingModule"],
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__["FlexLayoutModule"].withConfig({ addFlexToParent: false })
        ]
    })
], Screen1Module);



/***/ })

}]);
//# sourceMappingURL=screen1-screen1-module-es2015.js.map