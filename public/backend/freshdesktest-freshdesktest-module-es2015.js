(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["freshdesktest-freshdesktest-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/layout/freshdesktest/freshdesktest.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/layout/freshdesktest/freshdesktest.component.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"mb-20\" flFlex flexLayout=\"row\" flexLayout.lt-md=\"column\" fxFlex fxLayoutGap=\"20px\">\n        <div fxFlex>\n            <table mat-table [dataSource]=\"dataSource\" class=\"mat-elevation-z8\">\n                <ng-container matColumnDef=\"id\">\n                    <th mat-header-cell *matHeaderCellDef> No. </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.id}} </td>\n                </ng-container>\n                <ng-container matColumnDef=\"organization_name\">\n                    <th mat-header-cell *matHeaderCellDef> Name </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.organization_name}} </td>\n                </ng-container>\n                <ng-container matColumnDef=\"ticket_count\">\n                    <th mat-header-cell *matHeaderCellDef> Ticket Count </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.ticket_count}} </td>\n                </ng-container>\n                <ng-container matColumnDef=\"page_count\">\n                    <th mat-header-cell *matHeaderCellDef> Page Count </th>\n                    <td mat-cell *matCellDef=\"let element\"> <mat-chip-list aria-label=\"Fish selection\">\n                            <mat-chip selected>{{element.page_count}}</mat-chip>\n                          </mat-chip-list> </td>\n                </ng-container>\n                <ng-container matColumnDef=\"agent_count\">\n                    <th mat-header-cell *matHeaderCellDef> Agent Count </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.agent_count}} </td>\n                </ng-container>\n                <ng-container matColumnDef=\"updated_on\">\n                    <th mat-header-cell *matHeaderCellDef> Last Update </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.updated_on}}</td>\n                </ng-container>\n                <ng-container matColumnDef=\"updated_at\">\n                        <th mat-header-cell *matHeaderCellDef> Update Since </th>\n                        <td mat-cell *matCellDef=\"let element\"> {{element.updated_on | timeago:live}}</td>\n                    </ng-container>\n                <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n                <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n            </table>\n            \n                  \n        </div>\n    </div>\n    \n    ");

/***/ }),

/***/ "./src/app/layout/freshdesktest/freshdesktest-routing.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/layout/freshdesktest/freshdesktest-routing.module.ts ***!
  \**********************************************************************/
/*! exports provided: FreshdesktestRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FreshdesktestRoutingModule", function() { return FreshdesktestRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _freshdesktest_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./freshdesktest.component */ "./src/app/layout/freshdesktest/freshdesktest.component.ts");
/* harmony import */ var _shared_guards__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/guards */ "./src/app/shared/guards/index.ts");





const routes = [
    {
        path: '',
        component: _freshdesktest_component__WEBPACK_IMPORTED_MODULE_3__["FreshdesktestComponent"], canActivate: [_shared_guards__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]]
    }
];
let FreshdesktestRoutingModule = class FreshdesktestRoutingModule {
};
FreshdesktestRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], FreshdesktestRoutingModule);



/***/ }),

/***/ "./src/app/layout/freshdesktest/freshdesktest.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/layout/freshdesktest/freshdesktest.component.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("table {\n  width: 100%;\n}\n\n.mat-card {\n  text-align: center;\n}\n\n.mat-card img {\n  border-radius: 5px;\n  margin-top: -25px;\n}\n\n.mat-table {\n  box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12);\n}\n\n.mb-20 {\n  margin-bottom: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L2ZyZXNoZGVza3Rlc3QvRjpcXE1vaGFuXFxhbmd1bGFyXFxhbmd1bGFyLW1hdGVyaWFsOC10aGVtZS9zcmNcXGFwcFxcbGF5b3V0XFxmcmVzaGRlc2t0ZXN0XFxmcmVzaGRlc2t0ZXN0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvZnJlc2hkZXNrdGVzdC9mcmVzaGRlc2t0ZXN0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBQTtBQ0NKOztBRENBO0VBQ0ksa0JBQUE7QUNFSjs7QURESTtFQUNJLGtCQUFBO0VBQ0EsaUJBQUE7QUNHUjs7QURBQTtFQUNJLCtHQUFBO0FDR0o7O0FEQ0E7RUFDSSxtQkFBQTtBQ0VKIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L2ZyZXNoZGVza3Rlc3QvZnJlc2hkZXNrdGVzdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcbi5tYXQtY2FyZCB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBpbWcge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAtMjVweDtcclxuICAgIH1cclxufVxyXG4ubWF0LXRhYmxlIHtcclxuICAgIGJveC1zaGFkb3c6IDAgM3B4IDFweCAtMnB4IHJnYmEoMCwgMCwgMCwgMC4yKSwgMCAycHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE0KSxcclxuICAgICAgICAwIDFweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xyXG59XHJcblxyXG4ubWItMjAge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG4iLCJ0YWJsZSB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4ubWF0LWNhcmQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4ubWF0LWNhcmQgaW1nIHtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBtYXJnaW4tdG9wOiAtMjVweDtcbn1cblxuLm1hdC10YWJsZSB7XG4gIGJveC1zaGFkb3c6IDAgM3B4IDFweCAtMnB4IHJnYmEoMCwgMCwgMCwgMC4yKSwgMCAycHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE0KSwgMCAxcHggNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKTtcbn1cblxuLm1iLTIwIHtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/layout/freshdesktest/freshdesktest.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/layout/freshdesktest/freshdesktest.component.ts ***!
  \*****************************************************************/
/*! exports provided: FreshdesktestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FreshdesktestComponent", function() { return FreshdesktestComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _shared_services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/services/api.service */ "./src/app/shared/services/api.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/snack-bar */ "./node_modules/@angular/material/esm2015/snack-bar.js");






const ELEMENT_DATA = [];
let FreshdesktestComponent = class FreshdesktestComponent {
    constructor(apiService, snackBar) {
        this.apiService = apiService;
        this.snackBar = snackBar;
        this.displayedColumns = ['id', 'organization_name', 'ticket_count', 'page_count', 'agent_count', 'updated_on', 'updated_at'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](ELEMENT_DATA);
    }
    applyFilter(filterValue) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    }
    ngOnInit() {
        this.loadPaid();
    }
    loadPaid() {
        this.apiService.getFreshdeskFree().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["first"])())
            .subscribe(data => {
            // console.log(data);
            const ELEMENT_DATA = data['data'];
            this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](ELEMENT_DATA);
            // this.dashboardData = data['data'];
        }, error => {
            this.snackBar.open(error, 'Retry', { duration: 5000 });
            // console.log(error);
        });
    }
};
FreshdesktestComponent.ctorParameters = () => [
    { type: _shared_services_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"] },
    { type: _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"] }
];
FreshdesktestComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-freshdesktest',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./freshdesktest.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/layout/freshdesktest/freshdesktest.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./freshdesktest.component.scss */ "./src/app/layout/freshdesktest/freshdesktest.component.scss")).default]
    })
], FreshdesktestComponent);



/***/ }),

/***/ "./src/app/layout/freshdesktest/freshdesktest.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/layout/freshdesktest/freshdesktest.module.ts ***!
  \**************************************************************/
/*! exports provided: FreshdesktestModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FreshdesktestModule", function() { return FreshdesktestModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _freshdesktest_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./freshdesktest.component */ "./src/app/layout/freshdesktest/freshdesktest.component.ts");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
/* harmony import */ var _freshdesktest_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./freshdesktest-routing.module */ "./src/app/layout/freshdesktest/freshdesktest-routing.module.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/esm2015/grid-list.js");
/* harmony import */ var ngx_timeago__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-timeago */ "./node_modules/ngx-timeago/fesm2015/ngx-timeago.js");
/* harmony import */ var _angular_material_chips__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/chips */ "./node_modules/@angular/material/esm2015/chips.js");










let FreshdesktestModule = class FreshdesktestModule {
};
FreshdesktestModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_freshdesktest_component__WEBPACK_IMPORTED_MODULE_3__["FreshdesktestComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _freshdesktest_routing_module__WEBPACK_IMPORTED_MODULE_5__["FreshdesktestRoutingModule"],
            _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_7__["MatGridListModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatTableModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatIconModule"],
            _angular_material_chips__WEBPACK_IMPORTED_MODULE_9__["MatChipsModule"],
            ngx_timeago__WEBPACK_IMPORTED_MODULE_8__["TimeagoModule"],
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__["FlexLayoutModule"].withConfig({ addFlexToParent: false })
        ]
    })
], FreshdesktestModule);



/***/ })

}]);
//# sourceMappingURL=freshdesktest-freshdesktest-module-es2015.js.map