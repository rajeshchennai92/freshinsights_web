@extends('dashboard')

@section('pageTitle') User @stop

@section('content')
    @if($fdSubscription)
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Freshdesk</h2>
            </div>
            <div class="col-md-12">
                <div class="col-md-6 float-left">
                    Email : {{ $user->email }}
                </div>
                <div class="col-md-6 float-left">
                    @if($user->meta->subscribed(config('plans.subscriptions.freshdesk.subscription_name')) && is_null($fdSubscription->ends_at) && $fdSubscription->plan_name != 'free')
                        <a class="btn btn-primary btn-sm raw-margin-right-8" href="{{ url('user/billing/cancellation/'.$user->id) }}">Cancel Subscription</a>
                    @endif
                    @if($fdSubscription->plan_name == 'free')
                        @if($user->meta->subscribed(config('plans.subscriptions.freshdesk.subscription_name')))
                            <form action="{{ url('user/billing/free/enable/'.$user->id) }}" method="post">
                                {!! csrf_field() !!}
                                <div class="form-group col-md-12">
                                    <label for="enable" class="col-md-4 float-left">Extend Upto:</label>
                                    <input type="text" class="form-control col-md-6 float-left" required name="fddatepicker" id="fddatepicker" />
                                </div>
                                <button type="submit" class="btn btn-primary btn-sm raw-margin-right-8" onclick="return confirm('Are you sure?')">Extend Subscription</button>
                            </form>
                            <br>
                            <a class="btn btn-primary btn-sm raw-margin-right-8" href="{{ url('user/billing/free/cancellation/'.$user->id) }}" onclick="return confirm('Are you sure?')">Cancel Subscription</a>
                        @else
                            <form action="{{ url('user/billing/free/enable/'.$user->id) }}" method="post" class="form-inline">
                                {!! csrf_field() !!}
                                <div class="form-group col-md-12">
                                    <label for="enable" class="col-md-4 float-left">Enable Upto:</label>
                                    <input type="text" class="form-control col-md-6 float-left" required name="fddatepicker" id="fddatepicker" />
                                </div>
                                <button type="submit" class="btn btn-primary btn-sm raw-margin-right-8" onclick="return confirm('Are you sure?')">Enable Subscription</button>
                            </form>
                        @endif
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 raw-margin-top-24">
                @if (count($fdAllSubscriptions) <= 0)
                    <div class="card card-default text-center">
                        <div class="card-body">
                            <span>No Subscriptions found.</span>
                        </div>
                    </div>
                @else
                    <table class="table table-striped">
                        <thead>
                        <th>Subscription</th>
                        <th>Plan</th>
                        <th>Active</th>
                        <th>Created</th>
                        <th>Ends At</th>
                        </thead>
                        <tbody>
                        @foreach($fdAllSubscriptions as $subscription)
                            <tr>
                                <td>{{ $subscription->name }}</td>
                                <td>{{ $subscription->plan_name }}</td>
                                <td>{{ $subscription->valid()?'yes':'no' }}</td>
                                <td>{{ $subscription->created_at }}</td>
                                <td>{{ $subscription->ends_at }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    @endif

    @if($fsSubscription)
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Freshservice</h2>
            </div>
            <div class="col-md-12">
                <div class="col-md-6 float-left">
                    Email : {{ $user->email }}
                </div>
                <div class="col-md-6 float-left">
                    @if($fsSubscription->plan_name == 'free')
                        @if($user->meta->subscribed(config('plans.subscriptions.freshservice.subscription_name')))
                            <form action="{{ url('user/billing/freshservice/free/enable/'.$user->id) }}" method="post">
                                {!! csrf_field() !!}
                                <div class="form-group col-md-12">
                                    <label for="enable" class="col-md-4 float-left">Extend Upto:</label>
                                    <input type="text" class="form-control col-md-6 float-left" required name="fsdatepicker" id="fsdatepicker" />
                                </div>
                                <button type="submit" class="btn btn-primary btn-sm raw-margin-right-8" onclick="return confirm('Are you sure?')">Extend Subscription</button>
                            </form>
                            <br>
                            <a class="btn btn-primary btn-sm raw-margin-right-8" href="{{ url('user/billing/freshservice/free/cancellation/'.$user->id) }}" onclick="return confirm('Are you sure?')">Cancel Subscription</a>
                        @else
                            <form action="{{ url('user/billing/freshservice/free/enable/'.$user->id) }}" method="post">
                                {!! csrf_field() !!}
                                <div class="form-group col-md-12">
                                    <label for="enable" class="col-md-4 float-left">Enable Upto:</label>
                                    <input type="text" class="form-control col-md-6 float-left" required name="fsdatepicker" id="fsdatepicker" />
                                </div>
                                <button type="submit" class="btn btn-primary btn-sm raw-margin-right-8" onclick="return confirm('Are you sure?')">Enable Subscription</button>
                            </form>
                        @endif
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 raw-margin-top-24">
                @if (count($fsAllSubscriptions) <= 0)
                    <div class="card card-default text-center">
                        <div class="card-body">
                            <span>No Subscriptions found.</span>
                        </div>
                    </div>
                @else
                    <table class="table table-striped">
                        <thead>
                        <th>Subscription</th>
                        <th>Plan</th>
                        <th>Active</th>
                        <th>Created</th>
                        <th>Ends At</th>
                        </thead>
                        <tbody>
                        @foreach($fsAllSubscriptions as $subscription)
                            <tr>
                                <td>{{ $subscription->name }}</td>
                                <td>{{ $subscription->plan_name }}</td>
                                <td>{{ $subscription->valid()?'yes':'no' }}</td>
                                <td>{{ $subscription->created_at }}</td>
                                <td>{{ $subscription->ends_at }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    @endif

@stop
