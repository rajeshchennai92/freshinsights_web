<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <meta name="_token" content="{{ csrf_token() }}" />

    <!-- Favicons -->
    <link href="/img/favicon.png" rel="icon">
    <link href="/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700|Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Nunito Sans" />

    <!-- Bootstrap CSS File -->
    <link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="/lib/animate/animate.min.css" rel="stylesheet">
    <link href="/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="/lib/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="/lib/css-loader/css-loader.css" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="/css/style.css" rel="stylesheet">
    <!-- Main Stylesheet File -->
    <link href="/css/raw.min.css" rel="stylesheet">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126976011-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-126976011-1');
    </script>

    {{--<!-- Google Tag Manager -->--}}
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-55JDS6Q');</script>
    {{--<!-- End Google Tag Manager -->--}}
<!-- Hotjar Tracking Code for https://freshinsights.co/ -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1684789,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
    {{--<!-- FullStory script -->--}}
    
    <script>
        window['_fs_debug'] = false;
        window['_fs_host'] = 'fullstory.com';
        window['_fs_org'] = 'KMGSZ';
        window['_fs_namespace'] = 'FS';
        (function(m,n,e,t,l,o,g,y){
            if (e in m) {if(m.console && m.console.log) { m.console.log('FullStory namespace conflict. Please set window["_fs_namespace"].');} return;}
            g=m[e]=function(a,b,s){g.q?g.q.push([a,b,s]):g._api(a,b,s);};g.q=[];
            o=n.createElement(t);o.async=1;o.src='https://'+_fs_host+'/s/fs.js';
            y=n.getElementsByTagName(t)[0];y.parentNode.insertBefore(o,y);
            g.identify=function(i,v,s){g(l,{uid:i},s);if(v)g(l,v,s)};g.setUserVars=function(v,s){g(l,v,s)};g.event=function(i,v,s){g('event',{n:i,p:v},s)};
            g.shutdown=function(){g("rec",!1)};g.restart=function(){g("rec",!0)};
            g.consent=function(a){g("consent",!arguments.length||a)};
            g.identifyAccount=function(i,v){o='account';v=v||{};v.acctId=i;g(o,v)};
            g.clearUserCookie=function(){};
        })(window,document,window['_fs_namespace'],'script','user');
    </script>
{{--<!-- FullStory script end -->--}}

    @yield("individual-style")
    <!-- =======================================================
      Theme Name: Avilon
      Theme URL: https://bootstrapmade.com/avilon-bootstrap-landing-page-template/
      Author: BootstrapMade.com
      License: https://bootstrapmade.com/license/
    ======================================================= -->
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-55JDS6Q"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

@include("user.layouts.navigation")

@yield('user-page-content')

<!--==========================
      Contact Section
    ============================-->
<section id="contact">
    <div class="container">
        <div class="row wow fadeInUp">

            <div class="col-lg-4 col-md-4">
                <div class="contact-about">
                    <a href="#intro"><img src="/img/logo.png" alt="" title="" id="about-logo-img"></a>
                    <div class="info">
                        <div>
                            <i class="ion-ios-location-outline"></i>
                            <p>79 SENECA LANE, BORDENTOWN<br>New Jersey, 08505</p>
                        </div>

                        <div>
                            <i class="ion-ios-email-outline"></i>
                            <p><a href="mailto:partner@radiare.com">partner@radiare.com</a></p>
                        </div>

                        <div>
                            <i class="ion-ios-telephone-outline"></i>
                            <p><a href="tel:+18622723925">+1 (862) 272-3925</a></p>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-4">
            </div>

            <div class="col-lg-5 col-md-8">
                <h3 class="section-title">Reach us here</h3>
                <div class="form">
                    <div id="sendmessage">Your message has been sent. Thank you!</div>
                    <div id="errormessage"></div>
                    <form action="" method="post" role="form" class="contactForm">
                        <div class="form-row">
                            <div class="form-group col-lg-6">
                                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                <div class="validation"></div>
                            </div>
                            <div class="form-group col-lg-6">
                                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                                <div class="validation"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                            <div class="validation"></div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                            <div class="validation"></div>
                        </div>
                        <div class="text-center"><button type="submit" title="Send Message">Send Message</button></div>
                    </form>
                </div>
            </div>

        </div>

    </div>
</section><!-- #contact -->

</main>

<!--==========================
  Footer
============================-->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 text-lg-left text-center">
                <div class="copyright">
                    &copy; Copyright <strong>Radiare</strong>. All Rights Reserved
                </div>
                {{--<div class="credits">--}}
                    {{--<!----}}
                      {{--All the links in the footer should remain intact.--}}
                      {{--You can delete the links only if you purchased the pro version.--}}
                      {{--Licensing information: https://bootstrapmade.com/license/--}}
                      {{--Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Avilon--}}
                    {{---->--}}
                    {{--Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>--}}
                {{--</div>--}}
            </div>
            <div class="col-lg-6">
                {{--<nav class="footer-links text-lg-right text-center pt-2 pt-lg-0">--}}
                    {{--<a href="#intro" class="scrollto">Home</a>--}}
                    {{--<a href="#about-us" class="scrollto">About</a>--}}
                    {{--<a href="#">Privacy Policy</a>--}}
                    {{--<a href="#">Terms of Use</a>--}}
                {{--</nav>--}}
            </div>
        </div>
    </div>
</footer><!-- #footer -->

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

<script type="text/javascript">
    var _token = '{!! Session::token() !!}';
    var _url = '{!! url("/") !!}';
</script>
@yield("pre-javascript")

<!-- JavaScript Libraries -->
<script src="/lib/jquery/jquery.min.js"></script>
<script src="/lib/jquery/jquery-migrate.min.js"></script>
<script src="/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/lib/easing/easing.min.js"></script>
<script src="/lib/wow/wow.min.js"></script>
<script src="/lib/superfish/hoverIntent.js"></script>
<script src="/lib/superfish/superfish.min.js"></script>
<script src="/lib/magnific-popup/magnific-popup.min.js"></script>
<script src="/js/bootstrap-datepicker.min.js"></script>

<!-- Template Main Javascript File -->
<script src="/js/main.js"></script>

<!-- Subscription JS -->
<script src="/js/fd-pricing-calc.js"></script>
<script src="/js/freshservice_js.js"></script>

<!-- Card JS -->
<script src="/js/card.js"></script>

<!-- Contact JS -->
<script src="/js/contactform.js"></script>

@yield("post-javascript")

@yield("post-price-javascript")

</body>
</html>
