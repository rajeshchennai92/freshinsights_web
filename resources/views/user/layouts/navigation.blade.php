<!--==========================
  Header
============================-->
<header id="header">
    <div class="container">

        @if(Request::is('/'))
            <div id="logo" class="pull-left">
                <!--            <h1><a href="#intro" class="scrollto">Freshinsights</a></h1>-->
                <!-- Uncomment below if you prefer to use an image logo -->
                <a href="#intro" class="scrollto"><img src="/img/logo.png" alt="" title=""></a>
            </div>

            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li class="menu-active"><a href="#intro">Home</a></li>
                    <li><a href="#about-us">About Us</a></li>
                    <li class="menu-has-children"><a href="#solution">Solutions</a>
                        <ul>
                            <li><a href="#"><img src="/img/freshdesk.png"/> Freshdesk Reporting</a>
                                 <ul>
                                    <li><a href="{{ route('freshdeskPage') }}"><img src="/img/powerbi.png"/> Power BI</a>
                                    </li>
                                    <li><a href="{{ route('freshsdeskTableauPage') }}"><img src="/img/tableau.png"/> Tableau</a></li>
                                </ul>
                            </li>
                            <li class="menu-has-children"><a href="#"><img src="/img/freshservice.png"/> Freshservice Reporting</a>
                                <ul>
                                    <li><a href="{{ route('freshservicePage') }}"><img src="/img/powerbi.png"/> Power BI</a>
                                    </li>
                                    <li><a href="{{ route('freshserviceTableauPage') }}"><img src="/img/tableau.png"/> Tableau</a></li>
                                </ul>
                            </li>
                            {{--<li><a href="other-visualizations.php"><img src="/img/freshinsights.png"/> Other Visualizations</a></li>--}}
                        </ul>
                    </li>
                    <li><a href="#contact">Contact Us</a></li>
                    @if (Auth::user())
                        <li><a href="{{ route('userAccount') }}">My Account</a></li>
                        <li><a href="/logout">Sign out</a></li>
                    @else
                        <li><a href="/login">Sign in</a></li>
                    @endif
                </ul>
            </nav><!-- #nav-menu-container -->
        @else
            <div id="logo" class="pull-left">
                <!--            <h1><a href="#intro" class="scrollto">Freshinsights</a></h1>-->
                <!-- Uncomment below if you prefer to use an image logo -->
                <a href="{{ url('/') }}"><img src="/img/logo.png" alt="" title=""></a>
            </div>

            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li class="menu-active"><a href="{{ url('/') }}">Home</a></li>
                    <li><a href="{{ url('/') }}#about-us">About Us</a></li>
                    <li class="menu-has-children"><a href="{{ url('/') }}#solution">Solutions</a>
                        <ul>
                            <li><a href="#"><img src="/img/freshdesk.png"/> Freshdesk Reporting</a>
                             <ul>
                                    <li><a href="{{ route('freshdeskPage') }}"><img src="/img/powerbi.png"/> Power BI</a>
                                    </li>
                                    <li><a href="{{ route('freshsdeskTableauPage') }}"><img src="/img/tableau.png"/> Tableau</a></li>
                                </ul>
                            </li>
                            <li class="menu-has-children"><a href="#"><img src="/img/freshservice.png"/> Freshservice Reporting</a>
                                <ul>
                                    <li><a href="{{ route('freshservicePage') }}"><img src="/img/powerbi.png"/> Power BI</a>
                                    </li>
                                    <li><a href="{{ route('freshserviceTableauPage') }}"><img src="/img/tableau.png"/> Tableau</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="{{ url('/') }}#contact">Contact Us</a></li>
                    @if (Auth::user())
                        <li><a href="{{ route('userAccount') }}">My Account</a></li>
                        <li><a href="/logout">Sign out</a></li>
                    @else
                        <li><a href="/login">Sign in</a></li>
                    @endif
                </ul>
            </nav><!-- #nav-menu-container -->
         @endif
    </div>
</header><!-- #header -->