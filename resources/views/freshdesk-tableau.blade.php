@extends('user.layouts.master')

@section('title', 'FreshInsights - Freshdesk in Tableau')

@section('individual-style')
    @include('billing.freshdesk-tableau.style')
@stop

@section('user-page-content')

    @include('billing.freshdesk-tableau.navigation')
    <!--==========================
      About product Section
    ============================-->
    <section id="about-us" class="section-bg section-about-bg">
        <div class="container-fluid">
            <div class="section-header">
                <div>
                    <h3 class="section-title"><img src="/img/tableau.png" style="vertical-align:top;
"> Tableau Reporting on Freshdesk</h3>
                </div>
                <span class="section-divider"></span>
                <p class="section-description col-md-10 offset-md-1 pb-0">Radiare in partnership with Freshworks through the FreshInsights platform offers rich visualization and analytics solutions for Freshdesk and Freshservice in a seamless manner. FreshInsights is offered as <a href="https://apps.freshservice.com/" target="_blank">"FreshInsights on Tableau for Freshdesk"</a>  in the Freshdesk marketplace and is available to all Freshdesk customers who can consume the basic Reports and Dashboards leveraging their existing Tableau product suite without any further investments. This seamless integration between your Freskdesk data and Tableau reporting happens behind the scenes using our connectors to your Freshdesk instance and using our data models and Tableau reporting templates built by our teams.</br></br></br>FreshInsights enables our clients to get insightful and intuitive visualizations on the performance of their customer service agents, ticket analysis and trends, response time and ticket ageing among others. While some of the features are available for free, further customisation of the data management, reporting and analytics needs are provided for a nominal fee. All your data is securely hosted on the Azure cloud with multiple security layers. Currently many hundreds of customers of Freshdesk have registered with our FreshInsights solution.</p>
            </div>

        </div>
    </section><!-- #about-product -->

    <!--==========================
      Product Advanced Featuress Section
    ============================-->
    <section id="advanced-features">

        <div class="features-row section-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-header wow fadeIn" data-wow-duration="1s">
                            <h3 class="section-title">Product Features</h3>
                            <span class="section-divider"></span>
                            <p class="section-description text-center pb-0">Following functional areas are covered under the first phase of the application release.</p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="wow fadeIn">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 box wow fadeInRight float-left">
                                    <!--                                <div class="icon"><img src="img/ticket%20analysis-01.png"/></div>-->
                                    <h4 class="title mb-0"><img class="iconimg" src="/img/ticket analysis-01.png"/>Ticket Traffic Analysis</h4>
                                    <p class="description">This dashboard provides an overview of the origin of tickets, priority, resolution time, requester along with trend analysis based on historical data.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 box wow fadeInRight float-left">
                                    <!--                                <div class="icon"><img src="img/ticket%20analysis-01.png"/></div>-->
                                    <h4 class="title mb-0"><img class="iconimg" src="/img/ticket_resolution_icon.png"/>Ticket Resolution Analysis</h4>
                                    <p class="description">This dashboard provides analysis of tickets by priority, resolution time, first response status among others.</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 box wow fadeInRight float-left">
                                    <!--                                <div class="icon"><img src="img/ticket%20analysis-01.png"/></div>-->
                                    <h4 class="title mb-0"><img class="iconimg" src="/img/ticket_summary_icon.png"/>Ticket Summary Analysis</h4>
                                    <p class="description">This dashboard provides an overview of the status of tickets, impact, status by agent and priority, requester location and department along with analysis based on created date vs due date and created date vs resolved date.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 box wow fadeInRight float-left" data-wow-delay="0.1s">
                                    <h4 class="title mb-0"><img class="iconimg" src="/img/agent workload-01-01.png"/>Agent Workload Analysis</h4>
                                    <p class="description">This dashboard provides complete analysis of the workload of an agent over a period of time. The Dashboard gives a view of source of the tickets, peak periods, priority levels among other parameters.</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 box wow fadeInRight float-left" data-wow-delay="0.2s">
                                    <h4 class="title mb-0"><img class="iconimg" src="/img/agent%20performance-01.png"/>Agent Performance Analysis</h4>
                                    <p class="description">This dashboard measures the performance of the agents and also provides a comparison among the agents across various important parameters over a period of time.</p>
                                </div>
                                <div class="col-lg-6 col-md-6 box wow fadeInRight float-left" data-wow-delay="0.2s">
                                    <h4 class="title mb-0"><img class="iconimg" src="/img/custom icon-01.png"/>Customized reporting & dashboards</h4>
                                    <p class="description">In addition to the above mentioned KPIs/Dashboards, we also offer customized reporting/dashboards based on your needs. We also provide hosting on your inhouse server. Please contact us for the same.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="demo" class="carousel slide pb-3" data-ride="carousel" style="width: 80%;margin: 0 auto">
                            <!-- The slideshow -->
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <a href="/img/gallery/agent_performance_analysis_fd_tableau.png" class="gallery-popup">
                                        <img src="/img/gallery/agent_performance_analysis_fd_tableau.png" alt="Agent Performance" class="galleryimg">
                                    </a>
                                </div>
                                <div class="carousel-item">
                                    <a href="/img/gallery/company_details_fd-tableau.png" class="gallery-popup">
                                        <img src="/img/gallery/company_details_fd-tableau.png" alt="Company Details" class="galleryimg">
                                    </a>
                                </div>
                                <div class="carousel-item">
                                    <a href="/img/gallery/requester_details_fd-tableau.png" class="gallery-popup">
                                        <img src="/img/gallery/requester_details_fd-tableau.png" alt="Requester Details" class="galleryimg">
                                    </a>
                                </div>
                                <div class="carousel-item">
                                    <a href="/img/gallery/freshdesk_summary_fd-tableau.png" class="gallery-popup">
                                        <img src="/img/gallery/freshdesk_summary_fd-tableau.png" alt="Summary" class="galleryimg">
                                    </a>
                                </div>
                                <div class="carousel-item">
                                    <a href="/img/gallery/freshinsights_dashboard_Overview_fd-tableau.png" class="gallery-popup">
                                        <img src="/img/gallery/freshinsights_dashboard_Overview_fd-tableau.png" alt="Dashboard Overview" class="galleryimg">
                                    </a>
                                </div>
                                <div class="carousel-item">
                                    <a href="/img/gallery/freshinsights_ticket_resolution_analysis_fd-tableau.png" class="gallery-popup">
                                        <img src="/img/gallery/freshinsights_ticket_resolution_analysis_fd-tableau.png" alt="Ticket Reservation" class="galleryimg">
                                    </a>
                                </div>
                                <div class="carousel-item">
                                    <a href="/img/gallery/freshinsights_type_management_fd-tableau.png" class="gallery-popup">
                                        <img src="/img/gallery/freshinsights_type_management_fd-tableau.png" alt="Type Management" class="galleryimg">
                                    </a>
                                </div>
                            </div>

                            <!-- Left and right controls -->
                            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                                <span><i class="fa fa-arrow-left" aria-hidden="true"></i></span>
                            </a>
                            <a class="carousel-control-next" href="#demo" data-slide="next">
                                <span><i class="fa fa-arrow-right" aria-hidden="true"></i></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- #advanced-features -->


    <!--==========================
      Product Featuress Section
    ============================-->
    {{--<section id="features">--}}
        {{--<div class="container">--}}

            {{--<div class="row">--}}

                {{--<div class="col-12">--}}
                    {{--<div class="section-header wow fadeIn col-md-12 col-lg-12" data-wow-duration="1s">--}}
                        {{--<h3 class="section-title">How It Works</h3>--}}
                        {{--<span class="section-divider"></span>--}}
                        {{--<p class="section-description">--}}
                            {{--As part of enabling Tableau Reporting for Freshservice, a short registration process needs to be completed. Once the registration process is complete we would activate our connectors to link the Tableau reporting engine to your Freshservice instance to provide rich visualization for your Freshservice data. Also we take care of periodic refreshes to pull data and keep the reporting engine up-to-date. While the basic reporting and periodic refreshes are offered as part of the basic free tier, for customised reporting, timely refresh, role based security, email support etc can be availed as part of paid tier for a nominal monthly fee.--}}
                        {{--</p>--}}
                    {{--</div>--}}
                {{--</div>--}}

            {{--</div>--}}

        {{--</div>--}}

        {{--</div>--}}

    {{--</section><!-- #features -->--}}

    <!--==========================
      Call To Action Section
    ============================-->
    <section id="call-to-action">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 text-center text-lg-left">
                    <h3 class="cta-title">Contact Us</h3>
                    <p class="cta-text">To get started on Tableau for Freshdesk please reach out to us <a href="mailto:{{ env('FRESHSERVICE_CONTACT_EMAIL') }}" style="color: #f47920">{{ env('FRESHSERVICE_CONTACT_EMAIL') }}</a> or fill in the form below with your details.</p>
                </div>
                {{--<div class="col-lg-3 cta-btn-container text-center">--}}
                    {{--<a class="cta-btn align-middle" href="#">Register</a>--}}
                {{--</div>--}}
            </div>

        </div>
    </section><!-- #call-to-action -->

@stop

