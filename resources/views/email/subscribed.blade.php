
@component('mail::message')

You are subscribed to {{ $subscription }} plan for {{$data}} Reporting on {{ $product }}

<br>

Data Integration process has been initiated and you will receive a confirmation email once the process is complete.

<br>

Sign in to view more details

@component('mail::button', ['url' => $url])
Sign in
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent