@component('mail::message')
Your free subscription is going to end in {{ $day }} day{{ $day!=1?'s':'' }}.

End date is {{ $date }}

To ensure continued access, please subscribe by choosing any of our subscription plans
@component('mail::button', ['url' => $url])
Subscribe
@endcomponent

Please upgrade or contact support.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
