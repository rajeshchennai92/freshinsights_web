@component('mail::message')
Your Subscription has been upgraded.

Sign in to check the details
@component('mail::button', ['url' => $url])
Sign In
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
