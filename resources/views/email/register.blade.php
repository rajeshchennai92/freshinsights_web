@component('mail::message')

Welcome to Freshinsights

Your API Key is {{ $verification_code }}

Sign in to view more details

@component('mail::button', ['url' => $url])
Sign in
@endcomponent

NOTE: Please do not share your API Key with anyone

Thanks,<br>
{{ config('app.name') }}
@endcomponent