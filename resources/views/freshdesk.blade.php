@extends('user.layouts.master')

@section('title', 'FreshInsights - Freshdesk in Power BI')

@section('individual-style')
    @include('billing.freshdesk.style')
@stop

@section('user-page-content')

    @include('billing.freshdesk.navigation')
    <!--==========================
      About product Section
    ============================-->
    <section id="about-us" class="section-bg section-about-bg">
        <div class="container-fluid">
            <div class="section-header">
                <div>
                    <h3 class="section-title"><img src="/img/powerbi.png" style="vertical-align:top;
"> Power BI Reporting on Freshdesk</h3>
                </div>
                <span class="section-divider"></span>
                <p class="section-description col-md-10 offset-md-1 pb-0">Radiare in partnership with Freshworks through the FreshInsights platform offers rich visualization and analytics solutions for Freshdesk and Freshservice in a seamless manner. FreshInsights is offered as <a href="https://apps.freshdesk.com/freshdesk_on_powerbi/" target="_blank">"FreshInsights on PowerBI for Freshdesk"</a>  in the Freshdesk marketplace and is available to all Freshdesk customers who can consume the basic Reports and Dashboards leveraging their existing Microsoft Power BI product suite without any further investments. This seamless integration between your Freshdesk data and PowerBI reporting happens behind the scenes using our connectors to your Freshdesk instance and using our data models and PowerBI reporting templates built by our teams.</br></br></br>FreshInsights enables our clients to get insightful and intuitive visualizations on the performance of their customer service agents, ticket analysis and trends, response time and ticket ageing among others. While some of the features are available for free, further customisation of the data management, reporting and analytics needs are provided for a nominal fee. All your data is securely hosted on the Azure cloud with multiple security layers. Currently many hundreds of customers of Freshdesk have registered with our FreshInsights solution.</p>
            </div>

        </div>
    </section><!-- #about-product -->

    <!--==========================
      Product Advanced Featuress Section
    ============================-->
    <section id="advanced-features">

        <div class="features-row section-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-header wow fadeIn" data-wow-duration="1s">
                            <h3 class="section-title">Product Features</h3>
                            <span class="section-divider"></span>
                            <p class="section-description text-center pb-0">Following functional areas are covered under the first phase of the application release.</p>
                        </div>
                    </div>
                    <div class="col-md-12 mt-3">
                        <div class="wow fadeIn">
                            <div class="col-lg-6 col-md-6 box wow fadeInRight float-left">
                                <!--                                <div class="icon"><img src="img/ticket%20analysis-01.png"/></div>-->
                                <h4 class="title mb-0"><img class="iconimg" src="/img/ticket analysis-01.png"/>Ticket Traffic Analysis</h4>
                                <p class="description">This dashboard presents the Ticket Volume details by Ticket Status, Source, Ticket created date, Ticket type, Priority, Agent and Group. The report can be filtered by Ticket Created Date. The KPIs include Tickets received, Average Resolution Time, % Tickets closed within SLA , %Escalations and Busiest Month, Day, Date and Hour.</p>
                            </div>
                            <div class="col-lg-6 col-md-6 box wow fadeInRight float-left" data-wow-delay="0.1s">
                                <h4 class="title mb-0"><img class="iconimg" src="/img/agent workload-01-01.png"/>Agent Workload Analysis</h4>
                                <p class="description">This dashboard presents the Agent Workload details by Ticket Type, Source, Ticket created date and Priority. The report can be filtered by Agent name and Ticket Created Date. The KPIs include Total tickets handled, Open tickets, Closed/Resolved tickets and Busiest Month, Day, Date and Hour.</p>
                            </div>
                            <div class="col-lg-6 col-md-6 box wow fadeInRight float-left" data-wow-delay="0.2s">
                            <h4 class="title mb-0"><img class="iconimg" src="/img/agent%20performance-01.png"/>Agent Performance Analysis</h4>
                            <p class="description">This dashboard presents the Agent Performance Details such as Average Resolution Time by Priority, Source and Ticket Type, Escalations and Tickets closed within SLA by Priority. The KPIs include Total tickets handled, Open tickets, Closed/Resolved tickets, Average resolution Time,% Tickets closed within SLA and %Escalations. The report can be filtered by Agent name and Ticket Created Date.</p>
                            </div>
                            <div class="col-lg-6 col-md-6 box wow fadeInRight float-left" data-wow-delay="0.2s">
                                <h4 class="title mb-0"><img class="iconimg" src="/img/custom icon-01.png"/>Customized reporting & dashboards</h4>
                                <p class="description">In addition to the above mentioned KPIs/Dashboards, we also offer customized reporting/dashboards based on your needs. We also provide hosting on your inhouse server. Please refer to our pricing plans or contact us for the same.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="demo" class="carousel slide pb-3" data-ride="carousel" style="width: 80%;margin: 0 auto">
                            <!-- The slideshow -->
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <a href="/img/gallery/ticket_analysis-01.jpg" class="gallery-popup">
                                        <img src="/img/gallery/ticket_analysis-01.jpg" alt="Ticket Analysis" class="galleryimg">
                                    </a>
                                </div>
                                <div class="carousel-item">
                                    <a href="/img/gallery/agent_performance-01.jpg" class="gallery-popup">
                                        <img src="/img/gallery/agent_performance-01.jpg" alt="Agent Performance" class="galleryimg">
                                    </a>
                                </div>
                                <div class="carousel-item">
                                    <a href="/img/gallery/agent_workload-01.jpg" class="gallery-popup">
                                        <img src="/img/gallery/agent_workload-01.jpg" alt="Agent Workload" class="galleryimg">
                                    </a>
                                </div>
                                <div class="carousel-item">
                                    <a href="/img/gallery/custom-01.jpg" class="gallery-popup">
                                        <img src="/img/gallery/custom-01.jpg" alt="Custom Dashboards" class="galleryimg">
                                    </a>
                                </div>
                            </div>

                            <!-- Left and right controls -->
                            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                                <span><i class="fa fa-arrow-left" aria-hidden="true"></i></span>
                            </a>
                            <a class="carousel-control-next" href="#demo" data-slide="next">
                                <span><i class="fa fa-arrow-right" aria-hidden="true"></i></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- #advanced-features -->


    <!--==========================
      Product Featuress Section
    ============================-->
    <section id="features">
        <div class="container">

            <div class="row">

                <div class="col-12">
                    <div class="section-header wow fadeIn col-md-12 col-lg-12" data-wow-duration="1s">
                        <h3 class="section-title">How It Works</h3>
                        <span class="section-divider"></span>
                        <p class="section-description">
                            As part of enabling PowerBI Reporting for Freshdesk, a short registration process needs to be completed. Once the registration process is complete we would activate our connectors to link the PowerBI reporting engine to your Freshdesk instance to provide rich visualization for your Freshdesk data. Also we take care of periodic refreshes to pull data and keep the reporting engine up-to-date. While the basic reporting and periodic refreshes are offered as part of the basic free tier, for customised reporting, timely refresh, role based security, email support etc can be availed as part of paid tier for a nominal monthly fee.
                        </p>
                    </div>
                </div>

            </div>

        </div>

        </div>

    </section><!-- #features -->

    <!--==========================
      Call To Action Section
    ============================-->
    <section id="call-to-action">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 text-center text-lg-left">
                    <h3 class="cta-title">Pricing</h3>
                    <p class="cta-text">For details on our various pricing plans and associated features, please click on the Pricing button here.</p>
                </div>
                <div class="col-lg-3 cta-btn-container text-center">
                    <a class="cta-btn align-middle" href="{{ route('freshdeskPricing') }}">Pricing</a>
                </div>
            </div>

        </div>
    </section><!-- #call-to-action -->

@stop

