@if(\Request::route()->getName() == 'freshserviceTableauPage')
    <nav class="navbar navbar-expand-lg navbar-light bg-light" id="navbar">
        <div class="container">
            <a class="navbar-brand" href="#"><img src="/img/freshservice.png"/> Tableau Reporting on Freshservice</a>
            <button class="navbar-toggler menu-icon" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link scrollto" href="#about-us">Overview</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link scrollto" href="#advanced-features">Feature</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{ route('freshserviceTableauPricing') }}">Pricing</a>
                    </li>
                    @if(!Auth::user())
                        <li class="nav-item">
                            <a class="nav-link" style="padding: 0;padding-left: 5px;" href="/register/freshservice?for=2"><button class="btn btn-outline-success my-2 my-sm-0">Register</button></a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
@else
    <nav class="navbar navbar-expand-lg navbar-light bg-light" id="navbar">
        <div class="container">
            <a class="navbar-brand" href="{{ route('freshserviceTableauPage') }}"><img src="/img/freshservice.png"/> Freshservice Reporting on Tableau</a>
            <button class="navbar-toggler menu-icon" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link scrollto" href="{{ route('freshserviceTableauPage') }}">Overview</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link scrollto" href="{{ route('freshserviceTableauPage') }}">Features</a>
                    </li>
                     <li class="nav-item">
                      <a class="nav-link" href="{{ route('freshserviceTableauPricing') }}">Pricing</a>
                    </li>
                    @if(!Auth::user())
                        <li class="nav-item">
                            <a class="nav-link" style="padding: 0;padding-left: 5px;" href="/register/freshservice?for=2"><button class="btn btn-outline-success my-2 my-sm-0">Register</button></a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
@endif
@section('post-javascript')
    <script>
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            if (document.body.scrollTop > 10 || document.documentElement.scrollTop > 10) {
                document.getElementById("navbar").style.top = "0";
                document.getElementById("navbar").style.position = "sticky";
                document.getElementById("navbar").style.zIndex= "9999";
                $('header').slideUp('fast');
                if ($(window).width() < 768) {
                    $('#mobile-nav-toggle').slideUp('fast');
                }
            } else {
                $('header').slideDown('fast');
                if ($(window).width() < 768) {
                    $('#mobile-nav-toggle').slideDown('fast');
                }
                document.getElementById("navbar").style.top = "40px";
            }
        }
    </script>
@stop