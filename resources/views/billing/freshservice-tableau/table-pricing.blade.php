<div class="col-md-12">
{{--    <ul class="nav nav-tabs w-100 justify-content-center mb-3" id="pricingTab" role="tablist">--}}
{{--        <li class="nav-item">--}}
{{--            <a class="nav-link active" id="monthly-tab" data-toggle="tab" href="#monthly" role="tab" aria-controls="home" aria-selected="true">Monthly</a>--}}
{{--        </li>--}}
{{--        <li class="nav-item">--}}
{{--            <a class="nav-link" id="annually-tab" data-toggle="tab" href="#annually" role="tab" aria-controls="contact" aria-selected="false">Yearly</a>--}}
{{--        </li>--}}
{{--    </ul>--}}
    <div class="tab-content" id="pricingTabContent">
        <div class="tab-pane fade show active" id="monthly" role="tabpanel" aria-labelledby="home-tab">
            <div class="col-md-12 table-responsive">
                <table class="table pricing-table">
                    <thead>
                    <tr>
                       <th></th>
                        <th class="pricing-table-head p-0">
                            <h3>
                                Incidents
                            </h3>
                        </th>
                        <th class="pricing-table-head p-0">
                            <h3>
                                Incidents & Assets
                            </h3>
                        </th>

                    </tr>
                    </thead>
                    <tbody>


                    @foreach(config('plans.subscriptions.freshservice_tableau.plans.free.features') as $key => $feature)
                              <?php
                                $c = explode("+",$feature)
                              ?>
                            <tr>
                            {{--                            @foreach(config('plans.subscriptions.freshdesk.plans') as $key_plan => $plan)--}}
                            <td>{{ $key}}
                                <td><?php echo $c[0];?></td>
                                <td><?php echo $c[1]; ?></td>

                            {{--                            @endforeach--}}
                        </tr>


                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane fade" id="annually" role="tabpanel" aria-labelledby="contact-tab">
            <div class="col-md-12 table-responsive">
                <table class="table pricing-table">
                    <thead>
                    <tr>
                        <th></th>
                        @foreach(config('plans.subscriptions.freshservice.plans') as $key => $plan)
                            <th class="pricing-table-head p-0">
                                <h3>{{ $plan['name'] }}<br>
                                    @if($key == 'free')
                                        21 Days
                                    @else
                                    USD {{ $plan['annual_plan_amount'] }}/annual
                                        <!-- <span style="text-decoration: line-through;font-size: 14px;">USD {{ $plan['annual_plan_amount'] }}/annual</span> <span style="font-size: 14px;color:#FF8500;font-weight: bold;">20% Off</span><br>
                                        USD {{ $plan['annual_plan_amount']-(round($plan['annual_plan_amount']*20/100)) }}/annual -->
                                    @endif
                                </h3>
                            </th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @foreach(config('plans.subscriptions.freshservice.plans.free.features') as $key => $feature)
                        <tr>
                            <td>{{ $key }}<span class="error">{{ $key=='Historical Tickets Loading Limit'?'*':'' }}</span><span class="error">{{ $key=='Monthly incremental tickets limit'?'**':'' }}</span><span class=" pl-2 fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="{{ config('plans.subscriptions.freshservice.plans.free.tooltip')[$key] }}"></span></td>
                            @foreach(config('plans.subscriptions.freshservice.plans') as $key_plan => $plan)
                                <td>{{ $plan['features'][$key] }}</td>
                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="col-lg-12 pt-3" style="overflow: hidden">
    <p style="font-size: 0.9rem;" class="mb-0 pt-1 pr-3 text-left pb-2"><b>Note:</b>This is a non-refundable as the TWB file will be delivered to you. However you would be given a free trial period to check out the working of the TWB file with your own data hosted on our server before releasing the payment. Please reach out to <a href="mailto:{{ env('FRESHDESK_SUPPORT_EMAIL') }}">{{ env('FRESHDESK_SUPPORT_EMAIL') }}</a> for a quote.</p>
</div>
<div class="col-lg-12 pt-3" style="overflow: hidden">
    <p style="font-size: 0.9rem;" class="mb-0 pt-1 pr-3 text-left pb-2">In addition to the purchase of the Tableau TWB file, you need to avail the Data Refresh plan. For the first year you will be provided with a 30% discount on the plan chosen by you.</p>
</div>