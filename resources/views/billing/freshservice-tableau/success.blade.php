@extends('user.layouts.master')

@section('title', 'FreshInsights - Freshservice Tableau Sucess')

@section('individual-style')
    @include('billing.freshservice-tableau.style')
@stop

@section('user-page-content')
    @include('billing.freshservice-tableau.navigation')
    <main id="main">
        <section id="about-us" class="section-bg section-about-bg">
            <div class="container">
                <div class="section-header">
                    <div>
                        <h3 class="section-title"><img src="/img/freshservice.png">Freshservice Reporting on Tableau</h3>
                        <span class="section-divider"></span>
                        <h3 class="section-title">Thank you for registering. We will contact you soon.</h3>
                        <p class="section-description pb-0 text-center">You can contact us at <a href="mailto:{{ env('MAIL_FROM_ADDRESS') }}" style="color: #f47920">{{ env('MAIL_FROM_ADDRESS') }}.</a> </p>
                    </div>
                </div>
            </div>
        </section>
@stop