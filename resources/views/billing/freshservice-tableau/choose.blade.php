@extends('user.layouts.master')

@section('title', 'FreshInsights - Freshservice  Tableau Choose plan')

@section('individual-style')
    @include('billing.freshservice-tableau.style')
@stop

@section('pre-javascript')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script> Stripe.setPublishableKey('{{ Config::get("services.stripe.key") }}'); </script>
@stop

@section('user-page-content')
    @include('billing.freshservice-tableau.navigation')
    <main id="main">
        <section id="about-us" class="section-bg section-about-bg">
            <div class="container">
                <div class="section-header">
                    <div>
                        <h3 class="section-title"><img src="/img/tableau.png">Tableau Reporting on Freshservice</h3>
                        <h3 class="section-title">Choose Your Refresh Plan</h3>
                    </div>
                    <span class="section-divider"></span>
                    <p class="section-description pb-0">Choose your refresh plan that suits your needs here. A plan chosen can always be upgraded or downgraded based on your needs. On selection of a suitable plan, you will be asked to share your credit card details in the next page to complete the subscription process.</p>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 offset-md-3">
                            <form>
                                <input type="hidden" value="garden" name="plan" id="fs_plan_id">
                                <input type="hidden" value="garden" name="stripe_plan" id="fs_stripe_plan_id">
                                <input type="hidden" value="monthly" name="plan_interval" id="fs_plan_interval">
                                <input type="hidden" value="159" name="plan_amount" id="fs_plan_amount">
                            </form>
                        </div>
                        <div class="col-md-12">
                            <ul class="nav nav-tabs w-100 justify-content-center mb-3" id="fs_pricingTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="fs-monthly-tab" data-toggle="tab" href="#fs-monthly" role="tab" aria-controls="home" aria-selected="true">Monthly</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="fs-annually-tab" data-toggle="tab" href="#fs-annually" role="tab" aria-controls="contact" aria-selected="false">Yearly</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="fs_pricingTabContent">
                                <div class="tab-pane fade show active" id="fs-monthly" role="tabpanel" aria-labelledby="home-tab">
                                    @foreach(config('plans.subscriptions.freshservice.plans') as $key => $plan)
                                        @if(count($freeSubDetails)>0)
                                            @if ($key == 'free')
                                                @continue
                                            @endif
                                            <div class="col-md-3 col-sm-3 col-xs-12 float-left">
                                                <div class="pricing pricing-choose hover-effect {{ $key == 'garden'?'transform_div garden':'highlight_div' }}" onclick="setFSPlantoId('{{ $key }}','{{ $plan['name'] }}','{{ $plan['plan_amount'] }}','{{ $plan['stripe_id'] }}');">
                                                    <div class="pricing-head">
                                                        <h3>{{ $plan['name'] }}</h3>
                                                        {{--<h4><i>$</i>{{ $plan['plan_amount'] }}--}}
                                                        {{--<span>--}}
                                                        {{--Per Month </span>--}}
                                                        {{--</h4>--}}
                                                    </div>
                                                    <table class="table table-borderless">
                                                        <tr>
{{--                                                            <td><strong>Historical Ticket Fees</strong></td>--}}
{{--                                                            <td>$<span class="setup_fee_calc_{{ $plan['plan_amount']  }}">{{ config('plans.subscriptions.freshservice.setup_fee') }}</span></td>--}}
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Monthly <br>Fees</strong></td>
                                                            <td>${{ $plan['plan_amount'] }}</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        @else
                                            <div class="col-md-5-col col-sm-5-col col-xs-5-col">
                                                <div class="pricing pricing-choose hover-effect {{ $key == 'garden'?'transform_div garden':'highlight_div' }}" onclick="setFSPlantoId('{{ $key }}','{{ $plan['name'] }}','{{ $plan['plan_amount'] }}','{{ $plan['stripe_id'] }}');">
                                                    <div class="pricing-head">
                                                        <h3>{{ $plan['name'] }}</h3>
                                                        {{--<h4><i>$</i>{{ $plan['plan_amount'] }}--}}
                                                        {{--<span>--}}
                                                        {{--Per Month </span>--}}
                                                        {{--</h4>--}}
                                                    </div>
                                                    <table class="table table-borderless">
                                                        <tr>
{{--                                                            <td><strong>Historical Ticket Fees</strong></td>--}}
{{--                                                            <td>$<span class="setup_fee_calc_{{ $plan['plan_amount']  }}">{{ config('plans.subscriptions.freshservice.setup_fee') }}</span></td>--}}
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Monthly<br> Fees</strong></td>
                                                            <td>${{ $plan['plan_amount'] }}</td>
                                                        </tr>
                                                    </table>
                                                </div> <?php if($plan['plan_amount'] == 0){?>
                                                <div class="col-lg-12" style="font-size: 13px">
                                                    <p class="mb-0" style="font-size: 12px"><span class="error">*</span>&nbsp;For Free Trial,No credit card required.
                                                    </p>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="tab-pane fade" id="fs-annually" role="tabpanel" aria-labelledby="contact-tab">
                                    @foreach(config('plans.subscriptions.freshservice.plans') as $key => $plan)
                                        @if(count($freeSubDetails)>0)
                                            @if ($key == 'free')
                                                @continue
                                            @endif
                                            <div class="col-md-3 col-sm-3 col-xs-12 float-left">
                                                <div class="pricing pricing-choose hover-effect {{ $key == 'garden'?'transform_div garden':'highlight_div' }}" onclick="setFSPlantoId('{{ $key }}','{{ $plan['name'] }}','{{ $plan['annual_plan_amount'] }}','{{ $plan['annual_stripe_id'] }}');">
                                                    <div class="pricing-head">
                                                        <h3>{{ $plan['name'] }}</h3>
                                                        {{--<h4><i>$</i>{{ $plan['plan_amount'] }}--}}
                                                        {{--<span>--}}
                                                        {{--Per Month </span>--}}
                                                        {{--</h4>--}}
                                                    </div>
                                                    <table class="table table-borderless">
                                                        <tr>
{{--                                                            <td><strong>Historical Ticket Fees</strong></td>--}}
{{--                                                            <td>$<span class="setup_fee_calc_{{ $plan['annual_plan_amount']  }}">{{ config('plans.subscriptions.freshservice.setup_fee') }}</span></td>--}}
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Monthly<br> Fees</strong></td>
                                                            <td>${{ $plan['plan_amount'] }}</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        @else
                                            <div class="col-md-5-col col-sm-5-col col-xs-5-col">
                                                <div class="pricing pricing-choose hover-effect {{ $key == 'garden'?'transform_div garden':'highlight_div' }}" onclick="setFSPlantoId('{{ $key }}','{{ $plan['name'] }}','{{ $plan['annual_plan_amount'] }}','{{ $plan['annual_stripe_id'] }}');">
                                                    <div class="pricing-head">
                                                        <h3>{{ $plan['name'] }}</h3>
                                                        {{--<h4><i>$</i>{{ $plan['plan_amount'] }}--}}
                                                        {{--<span>--}}
                                                        {{--Per Month </span>--}}
                                                        {{--</h4>--}}
                                                    </div>
                                                    <table class="table table-borderless">
                                                        <tr>
{{--                                                            <td><strong>Historical Ticket Fees</strong></td>--}}
{{--                                                            <td>$<span class="setup_fee_calc_{{ $plan['annual_plan_amount']  }}">{{ config('plans.subscriptions.freshservice.setup_fee') }}</span></td>--}}
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Monthly<br> Fees</strong></td>
                                                            <td>${{ $plan['annual_plan_amount'] }}</td>
                                                        </tr>
                                                    </table>
                                                </div><?php if($plan['plan_amount'] == 0){?>
                                                <div class="col-lg-12" style="font-size: 13px">
                                                    <p class="mb-0" style="font-size: 12px"><span class="error">*</span>&nbsp;For Free Trial,No credit card required.
                                                    </p>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                        <button class="btn button_theme_color subscribe_btn" id="fs_subscribe_confirm" onclick="setFSPlanDetailsTableau();">Subscribe to Garden</button><i class="fa fa-spinner fa-spin ml-2" id="spinner1" style="font-size:24px;display: none"></i></br>
                        <a href="{{ route('freshserviceTableauPricing') }}" target="_blank"><u>View pricing plan <i class="fa fa-angle-double-right arrow1" aria-hidden="true"></i></u></a>
                    </div>
                </div>
            </div>
        </section>
@stop