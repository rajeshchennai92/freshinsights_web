@extends('user.layouts.master')

@section('title', 'Freshinsights - Invoices')

@section('individual-style')
    <style>
        #header{
            background: #fff;
            padding: 20px 0;
            height: 72px;
        }
        #about-us{
            padding: 90px 0 60px 0;
        }
    </style>
@stop

@section('user-page-content')

    <main id="main">
        <section id="about-us" class="section-bg">
            <div class="row">
                <div class="tabs-content col-6 offset-3">
                    <div role="tabpanel" class="tab-pane tab-active">
                        <div class="raw-margin-top-24 raw-margin-left-24 raw-margin-right-24">

                            <table class="table table-striped">

                                <thead>
                                <th>Date</th>
                                <th>Invoice ID</th>
                                <th>Amount</th>
                                <th>Download</th>
                                </thead>

                                <tbody>

                                @foreach($invoices as $invoice)
                                    <tr>
                                        <td>{{ date('d-m-Y', $invoice->date) }}</td>
                                        <td>{{ $invoice->number }}</td>
                                        <td>${{ ($invoice->total / 100) }}</td>
                                        <td><a href="{{ url('user/billing/invoice/'.$invoice->id) }}">Download</a></td>
                                    </tr>
                                @endforeach

                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@stop
