
@if(\Request::route()->getName() == 'freshdeskPage')
    <nav class="navbar navbar-expand-lg navbar-light bg-light" id="navbar">
        <div class="container">
            <a class="navbar-brand" href="#"><img src="/img/freshdesk.png"/> Freshdesk Reporting</a>
            <button class="navbar-toggler menu-icon" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link scrollto" href="#about-us">Overview</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link scrollto" href="#advanced-features">Features</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('freshdeskPricing') }}">Pricing</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link scrollto" href="{{ route('freshdeskPricing') }}#faq">FAQs</a>
                    </li>
                    @if(!Auth::user())
                        <li class="nav-item">
                            <a class="nav-link" style="padding: 0;padding-left: 5px;" href="/register/freshdesk?for=1"><button class="btn btn-outline-success my-2 my-sm-0">Register</button></a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
@else
    <nav class="navbar navbar-expand-lg navbar-light bg-light" id="navbar">
        <div class="container">
            <a class="navbar-brand" href="{{ route('freshdeskPage') }}"><img src="/img/freshdesk.png"/> Freshdesk Reporting</a>
            <button class="navbar-toggler menu-icon" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link scrollto" href="{{ route('freshdeskPage') }}">Overview</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link scrollto" href="{{ route('freshdeskPage') }}">Features</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('freshdeskPricing') }}">Pricing</a>
                    </li>
                    @if(\Request::route()->getName() == 'freshdeskPricing')
                        <li class="nav-item">
                            <a class="nav-link scrollto" href="#faq">FAQs</a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link scrollto" href="{{ route('freshdeskPricing') }}#faq">FAQs</a>
                        </li>
                    @endif
                    @if(!Auth::user())
                        <li class="nav-item">
                            <a class="nav-link" style="padding: 0;padding-left: 5px;" href="/register/freshdesk?for=1"><button class="btn btn-outline-success my-2 my-sm-0">Register</button></a>
                        </li>
                    @endif
                </ul>


            </div>
        </div>
    </nav>
@endif

@section('post-javascript')
    <script>
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            if (document.body.scrollTop > 10 || document.documentElement.scrollTop > 10) {
                document.getElementById("navbar").style.top = "0";
                document.getElementById("navbar").style.position = "sticky";
                document.getElementById("navbar").style.zIndex= "9999";
                $('header').slideUp('fast');
                if ($(window).width() < 768) {
                    $('#mobile-nav-toggle').slideUp('fast');
                }
            } else {
                $('header').slideDown('fast');
                if ($(window).width() < 768) {
                    $('#mobile-nav-toggle').slideDown('fast');
                }
                document.getElementById("navbar").style.top = "40px";
            }
        }
    </script>
@stop