@extends('user.layouts.master')

@section('title', 'FreshInsights - Freshdesk Pricing')

@section('individual-style')
    @include('billing.freshdesk.style')
@stop

@section('pre-javascript')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script> Stripe.setPublishableKey('{{ Config::get("services.stripe.key") }}'); </script>
@stop

@section('user-page-content')
    @include('billing.freshdesk-tableau.navigation')
    <main id="main">
{{--        <section id="about-us" class="section-bg section-about-bg">--}}
            <div class="container">
                <div class="section-header">
                    <div>
                        <h3 class="section-title"><img src="/img/tableau.png" style="vertical-align:top;
                            "> Tableau Reporting on Freshdesk</h3>
                        <span class="section-divider"></span>
                        <p class="section-description col-md-10 offset-md-1 pb-0">To avail Tableau reporting services on Freshdesk, you need to purchase the Tableau TWB file that is built for this purpose. We offer purchase of our pre-built Tableau TWB file that provides you with intuitive reporting and dashboards out of the box in a seamless fashion. Further if you have your own Tableau consultants, you can update the dashboard and charts as per your needs or continue to user our services to do the customization as per your needs.</br></br>You need to be subscribed to our data refresh plans to ensure that your Tableau dashboards reflects the recent information from Freshdesk. .</p>

                        <h3 class="section-title">Pricing Plans</h3>
                        <span class="section-divider"></span>
                        <p class="section-description col-md-10 offset-md-1 pb-0">We offer two options depending on where you want your Freshdesk data to be stored. The two options and the associated pricing are given below.</p>
                    </div>

                </div>
                <div class="row">
                    {{--<div class="col-12">--}}
                        {{--<div class="setup-section mb-3 text-center">--}}
                            {{--<h6 class="pt-4 mb-1">Setup Fee - ${{ config('plans.subscriptions.freshdesk.setup_fee') }}</h6>--}}
                            {{--<h6 class="mb-1 pb-3">Additional fee/10000 tickets - ${{ config('plans.subscriptions.freshdesk.additional_setup_fee') }}</h6>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    @include('billing.freshdesk-tableau.table-pricing')
                    <div class="col-lg-12 pt-3" style="overflow: hidden">
                        @if(!Auth::user() || !Auth::user()->freshdeskClients)
                            <p style="font-size: 0.9rem;" class="mb-0 pt-1 pr-3 text-center pb-2"> To register for the pricing plan of your choice, please visit the registration page.<a href="/register/freshdesk?for=2" style="margin-left: 20px;"><button class="btn button_theme_color btn_register button_theme_color_sm">Register Here</button></a></p>
                            {{--<p class="mb-0 pt-1 pr-3 text-center" style="font-size: 0.9rem;"><a href="#pricing-calc" class="scrollto"><u>Use our Pricing Calculator to estimate your fees.</u> <i class="fa fa-angle-double-right arrow1" aria-hidden="true"></i></a></p>--}}
                        @else
                            {{--<p class="mb-0 pt-1 pr-3 text-center" style="font-size: 0.9rem;"><a href="#pricing-calc" class="scrollto">Use our Pricing Calculator to estimate your fees. <i class="fa fa-angle-double-right arrow1" aria-hidden="true"></i></a></p>--}}
                        @endif
                    </div>
                </div>
            </div>
{{--        </section>--}}

        <!--==========================
          Frequently Asked Questions Section
        ============================-->
        <section id="faq">

                   <div class="container">
                <div class="section-header">
                    <h3 class="section-title">Frequently Asked Questions</h3>
                    <span class="section-divider"></span>
                    {{--<p class="section-description pb-0 text-center">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>--}}
                </div>

                <ul id="faq-list" class="wow fadeInUp">
                    <li>
                        <a data-toggle="collapse" class="collapsed" href="#faq1">We are already paying for Freshdesk? Why should we pay for this reporting service?<i class="ion-android-remove"></i></a>
                        <div id="faq1" class="collapse" data-parent="#faq-list">
                            <p>
                                The FreshInsights™ for Freshdesk offers superlative PowerBI’s visualisation features and is offered as a separate service by a third-party provider named Radiare. The services include building connectors between your Freshdesk instance and the data models that churn out the PowerBI reports with data refreshed at regular intervals. This is provided outside of the Freshdesk environment on our secure Azure cloud environment. The nominal payment that is asked is for the regular refreshing of the data, allowing sharing of PowerBI internally to other stakeholders in the organisation, ensuring that API connectors are kept upto date with the changes as and when they are made on the Freshdesk side.
                            </p>
                            <p>
                                Also please note that the subscription fees is irrespective of the number of agents that you may have with a restriction on the number of tickets that will shown on the reports based on the plan that you choose.
                            </p>
                        </div>
                    </li>

                    <li>
                        <a data-toggle="collapse" class="collapsed" href="#faq2">How can we make this payment?<i class="ion-android-remove"></i></a>
                        <div id="faq2" class="collapse" data-parent="#faq-list">
                            <p>
                                There are two options available for making the payment. We have enabled Stripe for monthly recurring payments or alternately we can raise a quarterly or annual invoice based on which you can do a bank transfer.
                            </p>
                        </div>
                    </li>

           <!--         <li>
                        <a data-toggle="collapse" class="collapsed" href="#faq3">I am not able to edit the changes to the PowerBI Dashboards. I want make changes, how do I do that?<i class="ion-android-remove"></i></a>
                        <div id="faq3" class="collapse" data-parent="#faq-list">
                            <p>
                                The FreshInsights™ PowerBI Dashboards offer most of the common features that are sought after by customers. However if you want any additional feature there are two ways you can do it.
                            </p>
                            <p>
                                1.	We can take your requirements and build the customised dashboard for you for a nominal fee. Typical customizations can be done for prices ranging from $500-$2,000. Our customer service manager will provide you with an estimation and take your approval and deliver the dashboard to your satisfaction.
                            </p>
                            <p>
                                2.	Second option is for you is to purchase the FreshInsights™ dashboards along with the underlying data models and do the customisation yourself. In this scenario we will do the entire deployment on-premise on your server and we will continue to provide data refresh services between your Freshdesk instance and the FreshInsights data base. You can reach out to our support team and our customer service manager will reach out to you with a quote for the same.

                            </p>
                        </div>
                    </li>

                    <li>
                        <a data-toggle="collapse" class="collapsed" href="#faq4">Where is the dashboard located in my Power BI account?  Or, do I have to click the link each time I want to see?<i class="ion-android-remove"></i></a>
                        <div id="faq4" class="collapse" data-parent="#faq-list">
                            <p>
                                The dashboard is accessible via the link sent to your email address. It is NOT available under your Power BI account options bar.
                            </p>
                        </div>
                    </li>

                    <li>
                        <a data-toggle="collapse" href="#faq5" class="collapsed">How do I integrate Freshdesk to Power BI? I do not see an option to make the connection in Power BI pro to the Freshdesk server.<i class="ion-android-remove"></i></a>
                        <div id="faq5" class="collapse" data-parent="#faq-list">
                            <p>
                                You do not have to perform an integration between Freshdesk and Power BI to view your dashboard. Once we share the dashboard, you will receive an email containing the dashboard link that has the information to access your dashboard at any point in time.
                            </p>
                        </div>
                    </li> -->

                    {{--<li>--}}
                        {{--<a data-toggle="collapse" href="#faq6" class="collapsed">How can I share this dashboard with other users within my organization? <i class="ion-android-remove"></i></a>--}}
                        {{--<div id="faq6" class="collapse" data-parent="#faq-list">--}}
                            {{--<p>--}}
                                {{--You will not able to share the dashboard with other users as read-only access is provided per       user. However, you can upgrade to the paid services where you will be able to add more users    and the dashboard will be mailed to them by our team.--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</li>--}}

{{--                    <li>--}}
{{--                        <a data-toggle="collapse" href="#faq7" class="collapsed">Are we able to customize this dashboard at all? For example, I'm looking to add our Logo instead of the Freshdesk Logo and take specific items from the dashboard you created.<i class="ion-android-remove"></i></a>--}}
{{--                        <div id="faq7" class="collapse" data-parent="#faq-list">--}}
{{--                            <p>--}}
{{--                                Currently read-only access is provided to users for the dashboard. Any customization to the standard dashboards will be available as part of our paid services.--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                    </li>--}}

                    <li>
                        <a data-toggle="collapse" href="#faq8" class="collapsed">Is there a way to filter the dashboard by Group?  If not, can it be added? <i class="ion-android-remove"></i></a>
                        <div id="faq8" class="collapse" data-parent="#faq-list">
                            <p>
                                At present, we have a graph which displays "Ticket Volume by Group" under Ticket Analysis tab. Selecting any specific group in this graph will filter the entire report based on the selected group. If you need Group to be a part of the report filter as a dropdown, you can upgrade to our paid services and avail such customizations.
                            </p>
                        </div>
                    </li>

                    <li>
                        <a data-toggle="collapse" href="#faq9" class="collapsed">Why do I have to pay the monthly fee? <i class="ion-android-remove"></i></a>
                        <div id="faq9" class="collapse" data-parent="#faq-list">
                            <p>
                                Radiare has spent significant time and energy in building the dashboards and reports. Also the underlying data structures are built in a suitable fashion to support the periodic data freshes from your Freshdesk instance. To keep up with the data infrastructure costs and the reporting layer that our team has built, we charge a nominal fee that is based on your data volume.
                            </p>
                        </div>
                    </li>

                    <li>
                        <a data-toggle="collapse" href="#faq10" class="collapsed">Can I cancel the subscription and will I get a refund? <i class="ion-android-remove"></i></a>
                        <div id="faq10" class="collapse" data-parent="#faq-list">
                            <p>
                                Yes, you can cancel anytime as the services are offered as “pay as you go” kind of model. If it is monthly pricing plan you will not get any refund but you can continue to use the services until end of the month. If it is an bi-annual or annual subscription, you will get a refund for the months that are remaining excluding the current month.
                            </p>
                        </div>
                    </li>

{{--                    <li>--}}
{{--                        <a data-toggle="collapse" href="#faq11" class="collapsed">Can I buy the PBIX file so that I can have my team to customize as needed? <i class="ion-android-remove"></i></a>--}}
{{--                        <div id="faq11" class="collapse" data-parent="#faq-list">--}}
{{--                            <p>--}}
{{--                                Yes , we offer PBIX files for a minimal fee. You can reach out to us at <a class="p-0 d-inline" href="mailto:freshdesksupport@radiare.com">freshdesksupport@radiare.com</a> and we will respond within 24 hours.--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                    </li>--}}

                    <li>
                        <a data-toggle="collapse" href="#faq12" class="collapsed">I would like to customize my reports the way they are rendered. Would you do it for me? <i class="ion-android-remove"></i></a>
                        <div id="faq12" class="collapse" data-parent="#faq-list">
                            <p>
                                Yes, we offer our services to customize the reports and also continue to maintain your version. There will be additional development fees involved based on your customization needs. We ensure that this is kept to a minimal value just to cover our costs of customisation. You can reach out to us at <a class="p-0 d-inline" href="mailto:freshdesksupport@radiare.com">freshdesksupport@radiare.com</a> and we will respond within 24 hours.
                            </p>
                        </div>
                    </li>

                    <li>
                        <a data-toggle="collapse" href="#faq13" class="collapsed">I do not want to pay by my card, is there an alternate way of paying the subscription? <i class="ion-android-remove"></i></a>
                        <div id="faq13" class="collapse" data-parent="#faq-list">
                            <p>
                                Yes, we accept bank transfers. Please reach out to us at <a class="p-0 d-inline" href="mailto:freshdesksupport@radiare.com">freshdesksupport@radiare.com</a> with your request and we will generate an invoice with all details needed for you to do the bank transfer.
                            </p>
                        </div>
                    </li>

                </ul>

            </div>
        </section><!-- #faq -->

        {{--<div class="modal" id="priceCalcModal">--}}
            {{--<div class="modal-dialog modal-lg">--}}
                {{--<div class="modal-content">--}}

                    {{--<!-- Modal Header -->--}}
                    {{--<div class="modal-header">--}}
                        {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
                    {{--</div>--}}

                    {{--<!-- Modal body -->--}}
                    {{--<div class="modal-body pt-0 pb-0">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<h3 class="modal-title mb-2">Calculate Pricing</h3>--}}
                            {{--</div>--}}
                            {{--<div class="col-12 tabs pt-0 pb-0" id="tabs">--}}
                                {{--<div class="col-md-8 float-left form-group row">--}}
                                    {{--<label for="choose-input" class="col-md-6 col-form-label" style="font-size: 18px">Choose Your Pricing Plan</label>--}}
                                    {{--<select class="form-control col-md-6" onchange="showPriceCalc(this);">--}}
                                        {{--@foreach(config('plans.subscriptions.freshdesk.plans') as $key => $plan)--}}
                                            {{--@if($key != 'free')--}}
                                                {{--<option value="{{ $key }}">{{ $plan['name'] }}</option>--}}
                                            {{--@endif--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                    {{--<ul class="nav nav-pills nav-stacked" style="display: none">--}}
                                        {{--@foreach(config('plans.subscriptions.freshdesk.plans') as $key => $plan)--}}
                                            {{--@if($key != 'free')--}}
                                                {{--<li class="{{ $key=='free'?'active':'' }} tab-li col-md-5-col col-sm-5-col col-xs-5-col"><a href="#tab_{{ $key }}" data-toggle="pill">{{ $plan['name'] }}</a></li>--}}
                                            {{--@endif--}}
                                        {{--@endforeach--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-12 float-left">--}}
                                    {{--<div class="tab-content">--}}
                                        {{--@foreach(config('plans.subscriptions.freshdesk.plans') as $key => $plan)--}}
                                            {{--<div class="tab-pane {{ $key=='free'?'active':'' }}" id="tab_{{ $key }}">--}}
                                                {{--@if($key != 'free')--}}
                                                    {{--<form name="{{ $key }}" style="position: relative">--}}
                                                        {{--<input type="hidden" value="{{ config('plans.subscriptions.freshdesk.setup_fee') }}" id="setup_fee_amount_{{ $key }}" />--}}
                                                        {{--<input type="hidden" value="{{ $plan['plan_amount'] }}" id="plan_amount_{{ $key }}" />--}}
                                                        {{--<input type="hidden" value="{{ $plan['additional_fee'] }}" id="additional_fee_{{ $key }}" />--}}
                                                        {{--<input type="hidden" value="{{ $plan['allowed_ticket_count'] }}" id="allowed_ticket_count_{{ $key }}" />--}}
                                                        {{--<h2 class="raw-color-000000 mt-1" style="font-size: 18px;">One Time Setup Fees Calculation</h2>--}}
                                                        {{--<h6 class="raw-color-000000 pb-4" style="font-size: 14px;">Select the number of historical tickets to be loaded</h6>--}}
                                                        {{--<div class="col-md-10 offset-md-1">--}}
                                                            {{--<input class="range-slider__range" orient="horizontal" min="10000" max="500000" step="1000" offset="0" value="10000" type="range" name="setupfee">--}}
                                                            {{--<output for="setup" onforminput="value = setupfee.valueAsNumber;"></output>--}}
                                                            {{--<div class="col-md-12 mt-2 cost" style="overflow: hidden;">--}}
                                                                {{--<p class="raw-color-000000 float-left mb-1"><span style="font-size: 24px;line-height: 0px;">*</span>Upto 10,000 Tickets: $ {{ config('plans.subscriptions.freshdesk.setup_fee') }}</p>--}}
                                                                {{--<p class="float-right mb-1">SetUp Fees $ <span id="setup_fee_calc_{{ $key }}">{{ config('plans.subscriptions.freshdesk.setup_fee') }}</span></p>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                        {{--<h2 class="raw-color-000000 mt-3" style="font-size: 18px;">Recurring Monthly Fees Calculation</h2>--}}
                                                        {{--<h6 class="raw-color-000000 pb-4" style="font-size: 14px;">Select the likely number of monthly tickets volume</h6>--}}
                                                        {{--<div class="col-md-10 offset-md-1">--}}
                                                            {{--<input class="range-slider__range" orient="horizontal" min="{{ $plan['allowed_ticket_count'] }}" max="30000" step="500" offset="0" value="{{ $plan['allowed_ticket_count'] }}" type="range" name="monthlyfee">--}}
                                                            {{--<output for="monthlyfee" onforminput="value = monthlyfee.valueAsNumber;"></output>--}}
                                                            {{--<div class="col-md-12mt-2 cost" style="overflow: hidden;">--}}
                                                                {{--<p class="float-right">Monthly Fees $ <span id="monthly_fee_calc_{{ $key }}">{{ $plan['plan_amount'] }}</span></p>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</form>--}}
                                                {{--@endif--}}
                                            {{--</div>--}}
                                        {{--@endforeach--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        @stop

@section('post-price-javascript')
    <script>
        function calculatePrice() {
            var avgTicketCount = $("input[name='setupfee']").val();
            var monthlyTicketCount = $("input[name='monthlyfee']").val();

            @forEach(config('plans.subscriptions.freshdesk.plans') as $key => $plan)
                var setup_fee = {{ config('plans.subscriptions.freshdesk.setup_fee') }};
                var additional_setup_fee = {{ config('plans.subscriptions.freshdesk.additional_setup_fee') }};
                var plan_amount = {{ $plan['plan_amount'] }};
                var allowed_ticket_count = {{ $plan['allowed_ticket_count'] }};
                var allowed_history_ticket_count = {{ $plan['allowed_history_ticket_count'] }};
                var additional_fee = {{ $plan['additional_fee'] }};
                if (+avgTicketCount>+allowed_history_ticket_count){
                    setup_fee = +setup_fee + +(Math.round((avgTicketCount-allowed_history_ticket_count)/allowed_history_ticket_count)*additional_setup_fee);
                    $('#compare_pricing_section #setup_fee_calc_{{ $plan["plan_amount"] }}').html(setup_fee);
                    $('#compare_pricing_section #setup_fee_calc_0').html(0);
                }
                else {
                    $('#compare_pricing_section #setup_fee_calc_{{ $plan["plan_amount"] }}').html(setup_fee);
                    $('#compare_pricing_section #setup_fee_calc_0').html(0);
                }

                if (+monthlyTicketCount>+allowed_ticket_count){
                    plan_amount = +plan_amount + +(Math.round(((monthlyTicketCount-allowed_ticket_count)/100)*additional_fee));
                    $('#compare_pricing_section #monthly_fee_calc{{ $plan["plan_amount"] }}').html(plan_amount);
                }
                else {
                    $('#compare_pricing_section #monthly_fee_calc{{ $plan["plan_amount"] }}').html(plan_amount);
                }
            @endforeach
        }
        var el, newPoint, newPlace, offset;

        $('input[type=range]').on('input', function () {
            $(this).trigger('change');
        });
        // Select all range inputs, watch for change
        $("input[name='setupfee']").change(function() {

            // Cache this for efficiency
            el = $(this);

            // Measure width of range input
            width = el.width();

            // Figure out placement percentage between left and right of input
            newPoint = (el.val() - el.attr("min")) / (el.attr("max") - el.attr("min"));

            offset = 1;

            // Prevent bubble from going beyond left or right (unsupported browsers)
            if (newPoint < 0) { newPlace = 0; }
            else if (newPoint > 1) { newPlace = width; }
            else { newPlace = width * newPoint + offset; offset -= newPoint; }

            // Move bubble
            el
                .next("output")
                .css({
                    left: newPlace,
                    marginLeft: offset + "%",
                })
                .text(el.val());

            $(this).css('background',
                '-webkit-gradient(linear, left top, right top, '
                + 'color-stop(' + newPoint + ', #003a47), '
                + 'color-stop(' + newPoint + ', #e2e2e2)'
                + ')'
            );
            calculatePrice();
        })
        // Fake a change to position bubble at page load
            .trigger('change');

        // Select all range inputs, watch for change
        $("input[name='monthlyfee']").change(function() {

            // Cache this for efficiency
            el = $(this);

            // Measure width of range input
            width = el.width();

            // Figure out placement percentage between left and right of input
            newPoint = (el.val() - el.attr("min")) / (el.attr("max") - el.attr("min"));

            offset = 1;

            // Prevent bubble from going beyond left or right (unsupported browsers)
            if (newPoint < 0) { newPlace = 0; }
            else if (newPoint > 1) { newPlace = width; }
            else { newPlace = width * newPoint + offset; offset -= newPoint; }

            // Move bubble
            el
                .next("output")
                .css({
                    left: newPlace,
                    marginLeft: offset + "%",
                })
                .text(el.val());

            $(this).css('background',
                '-webkit-gradient(linear, left top, right top, '
                + 'color-stop(' + newPoint + ', #003a47), '
                + 'color-stop(' + newPoint + ', #e2e2e2)'
                + ')'
            );
            calculatePrice();
        })
        // Fake a change to position bubble at page load
            .trigger('change');

    </script>
@stop