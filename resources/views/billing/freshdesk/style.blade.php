<style>
    #navbar {
        transition: top 0.3s;
        top: 40px;
        background-color: #71b9b8 !important;
    }
    #header #logo {
        float: left;
        width: 20%;
    }
    #header {
        /*background: linear-gradient(45deg,#ffece1, #ffcbae);*/
        padding: 6px 0;
        height: 40px;
        z-index: 99999;
    }
    #about-us{
        padding: 60px 0 30px 0;
    }
    #advanced-features h3 {
        font-size: 28px;
        line-height: 24px;
        font-weight: 400;
        font-style: normal;
        color: #8e2602;
    }
    #advanced-features p {
        line-height: 24px;
        color: #000;
        margin-bottom: 30px;
        font-size: 16px;
        <!--text-align: justify;-->
    }
    #advanced-features .title {
        font-size: 18px;
    }
    #advanced-features h4{
        padding: 5px;
    }
    #header #logo img {
        margin-top: -2px;
    }
    #mobile-nav-toggle {
        top: 0;
    }
</style>