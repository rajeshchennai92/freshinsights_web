@extends('user.layouts.master')

@section('title', 'FreshInsights - My Account')

@section('individual-style')
    <style>
        #header{
            background: #fff;
            padding: 20px 0;
            height: 72px;
        }
        #about-us{
            padding: 90px 0 60px 0;
        }
    </style>
@stop

@section('pre-javascript')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script> Stripe.setPublishableKey('{{ Config::get("services.stripe.key") }}'); </script>
@stop

@section('user-page-content')

    <main id="main">
        <section id="about-us" class="section-bg">
            <div class="row ml-2 mr-2">
                @if ($user->meta->subscribed(config('plans.subscriptions.freshdesk.subscription_name')))
                    <div class="account__row w-100">
                        <div class="card-section account__column--left">
                            <div class="account__title">
                                Freshdesk Subscription Plan
                            </div>
                        </div>
                        <div class="card-section account__column--right">
                            <div class="w-100">
                                <p class="text-left raw-color-000000"><span id="fd_plan_name">{{ config('plans.subscriptions.freshdesk.plans')[$fdSubscription->plan_name]['name'] }}</span> Plan
                                    @if($fdSubscription->plan_name == 'free')
                                        will expire after 21 days
{{--                                        will expire after {{ $fd_remaining_day }}--}}
                                    @endif
                                </p>
                            </div>
                            <div class="w-100">
                                <div class="col-md-8 raw-margin-top-12 form_same_line">
                                    @if($fdSubscription->plan_name == 'free')
                                        <p>To upgrade the plan click choose plan &nbsp; <a href="{{ route('freshdeskSubscriptionChoose') }}"><button class="btn btn-primary pull-right button_theme_color button_theme_color_sm">Choose Plan</button></a></p>
                                    @endif
                                        <p>Cancel the subscription &nbsp; <a href="{{ route('freshdeskCancelSubscription') }}"><button class="btn btn-primary pull-right button_theme_color button_theme_color_sm">Cancel Subscription</button></a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="account__row w-100">
                        <div class="card-section account__column--left">
                            <div class="account__title">Freshdesk Subscription Plan</div>
                        </div>
                        <div class="card-section account__column--right">
                            <div>
                                <p>You haven't subscribed. To subscribe choose plan &nbsp; <a href="{{ route('freshdeskSubscriptionChoose') }}"><button class="btn btn-primary pull-right button_theme_color button_theme_color_sm">Choose Plan</button></a></p>
                            </div>
                        </div>
                    </div>
                @endif
                @if ($user->meta->subscribed(config('plans.subscriptions.freshservice.subscription_name')))
                    <div class="account__row w-100">
                        <div class="card-section account__column--left">
                            <div class="account__title">
                                Freshservice Subscription Plan
                            </div>
                        </div>
                        <div class="card-section account__column--right">
                            <div class="w-100">
                                <p class="text-left raw-color-000000"><span id="fs_plan_name">{{ config('plans.subscriptions.freshservice.plans')[$fsSubscription->plan_name]['name'] }}</span> Plan
                                    @if($fsSubscription->plan_name == 'free')
{{--                                        will expire after {{ $fs_remaining_day }}--}}
                                        will expire after 21 days
                                    @endif
                                </p>
                            </div>
                            <div class="w-100">
                                <div class="col-md-8 raw-margin-top-12 form_same_line">
                                    @if($fsSubscription->plan_name == 'free')
                                        <p>To upgrade the plan click choose plan &nbsp; <a href="{{ route('freshserviceSubscriptionChoose') }}"><button class="btn btn-primary pull-right button_theme_color button_theme_color_sm">Choose Plan</button></a></p>
                                    @endif
                                    <p>Cancel the subscription &nbsp; <a href="{{ route('freshserviceCancelSubscription') }}"><button class="btn btn-primary pull-right button_theme_color button_theme_color_sm">Cancel Subscription</button></a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="account__row w-100">
                        <div class="card-section account__column--left">
                            <div class="account__title">Freshservice Subscription Plan</div>
                        </div>
                        <div class="card-section account__column--right">
                            <div>
                                <p>You haven't subscribed. To subscribe choose plan &nbsp; <a href="{{ route('freshserviceSubscriptionChoose') }}"><button class="btn btn-primary pull-right button_theme_color button_theme_color_sm">Choose Plan</button></a></p>
                            </div>
                        </div>
                    </div>
                @endif
                @if (isset(Auth::user()->meta->stripe_id) && Auth::user()->meta->stripe_id != null)
                    <div class="account__row w-100">
                        <div class="card-section account__column--left">
                            <div class="account__title">Billing Information</div>
                        </div>
                        <div class="card-section account__column--right">
                            <div>
                                <div class="alert alert-success" id="change_card_success" style="display: none">
                                    <strong>Success!</strong> Card details changed successfully.
                                </div>
                                <button class="btn btn-primary button_theme_color button_theme_color_sm" data-toggle="modal" data-target="#cardModal" onclick="$('.modify_plan_btn').hide();$('.change_card_btn').show();">Change Card</button>
                                <a href="/user/billing/invoices"><button class="btn btn-primary button_theme_color_sm button_theme_color" >View Invoices</button></a>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="account__row w-100">
                    <div class="card-section account__column--left">
                        <div class="account__title">Change Password</div>
                    </div>
                    <div class="card-section account__column--right">
                        <div class="w-100">
                            <form method="POST" action="/user/password">
                                {!! csrf_field() !!}

                                <div>
                                    @input_maker_label('Old Password')
                                    @input_maker_create('old_password', ['type' => 'password', 'placeholder' => 'Old Password', 'custom' => 'required'])
                                </div>

                                <div class="raw-margin-top-24">
                                    @input_maker_label('New Password')
                                    @input_maker_create('new_password', ['type' => 'password', 'placeholder' => 'New Password', 'custom' => 'required'])
                                </div>

                                <div class="raw-margin-top-24">
                                    @input_maker_label('Confirm Password')
                                    @input_maker_create('new_password_confirmation', ['type' => 'password', 'placeholder' => 'Confirm Password', 'custom' => 'required'])
                                </div>
                                @include("partials.errors")
                                @include("partials.message")
                                <div class="raw-margin-top-24">
                                    <div class="btn-toolbar justify-content-between">
                                        <button class="btn btn-primary pull-right button_theme_color button_theme_color_sm" type="submit">Change Password</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <div class="modal" id="cardModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="card-wrapper"></div>
                    <form class="card-form">
                        @include('billing.card-form')
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger button_theme_color_sm" data-dismiss="modal" onclick="closeCardForm()">Close</button>
                    <button type="button" class="btn button_theme_color button_theme_color_sm change_card_btn" onclick="changeCardDetails(event)">Change</button>
                    <button type="button" class="btn button_theme_color button_theme_color_sm modify_plan_btn" onclick="changeFdFreePlan(event)">Upgrade</button>
                </div>

            </div>
        </div>
    </div>
@stop
