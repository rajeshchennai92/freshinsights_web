<div class="col-md-12">
{{--    <ul class="nav nav-tabs w-100 justify-content-center mb-3" id="pricingTab" role="tablist">--}}
{{--        <li class="nav-item">--}}
{{--            <a class="nav-link active" id="monthly-tab" data-toggle="tab" href="#monthly" role="tab" aria-controls="monthly" aria-selected="true">Monthly</a>--}}
{{--        </li>--}}
{{--        <li class="nav-item">--}}
{{--            <a class="nav-link" id="annually-tab" data-toggle="tab" href="#annually" role="tab" aria-controls="annual" aria-selected="false">Annually</a>--}}
{{--        </li>--}}

{{--    </ul>--}}
    <div class="tab-content" id="pricingTabContent">
        <div class="tab-pane fade show active" id="monthly" role="tabpanel" aria-labelledby="home-tab">
            <div class="col-md-12 table-responsive">
                <table class="table pricing-table">
                    <thead>
                    <tr>

                            <th class="pricing-table-head p-0">
                                <h3>
                                    Plans
                                </h3>
                            </th>
                            <th class="pricing-table-head p-0">
                                <h3>
                                   Price
                                </h3>
                        </th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach(config('plans.subscriptions.freshdesk_tableau.plans.free.features') as $key => $feature)
                        <tr>
{{--                            @foreach(config('plans.subscriptions.freshdesk.plans') as $key_plan => $plan)--}}
                                <td>{{ $key}}</td>
                               <td>{{ $feature}}</td>

                            {{--                            @endforeach--}}
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
{{--            <div class="col-lg-12" style="font-size: 13px">--}}
{{--                <p class="mb-0" style="font-size: 13px"><span class="error">**</span>&nbsp;For additional incremental tickets during the month, it would be charged at the rate of $1 for every 100 tickets.--}}
{{--                </p>--}}
{{--            </div>--}}

            <div class="col-lg-12 pt-3" style="overflow: hidden">
                <p style="font-size: 0.9rem;" class="mb-0 pt-1 pr-3 text-left pb-2"><b>Note:</b>This is a non-refundable as the TWB file will be delivered to you. However you would be given a free trial period to check out the working of the TWB file with your own data hosted on our server before releasing the payment. Please reach out to <a href="mailto:{{ env('FRESHDESK_SUPPORT_EMAIL') }}">{{ env('FRESHDESK_SUPPORT_EMAIL') }}</a> for a quote.</p>
            </div>
            <div class="col-lg-12 pt-3" style="overflow: hidden">
                <p style="font-size: 0.9rem;" class="mb-0 pt-1 pr-3 text-left pb-2">In addition to the purchase of the Tableau TWB file, you need to avail the Data Refresh plan. For the first year you will be provided with a 30% discount on the plan chosen by you</p>
            </div>

        </div>
    </div>
</div>