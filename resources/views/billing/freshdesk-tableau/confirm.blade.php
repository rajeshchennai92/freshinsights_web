@extends('user.layouts.master')

@section('title', 'FreshInsights - Freshdesk Tableau Confirm Subscription')

@section('individual-style')
    @include('billing.freshdesk-tableau.style')
@stop

@section('pre-javascript')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script> Stripe.setPublishableKey('{{ Config::get("services.stripe.key") }}'); </script>
@stop

@section('user-page-content')
    @include('billing.freshdesk-tableau.navigation')
    <main id="main">
        <section id="about-us" class="section-bg section-about-bg">
            <div class="container">
                <div class="section-header">
                    <div>
                        <h3 class="section-title"><img src="/img/tableau.png">Tableau Reporting on Freshdesk</h3>
                        <h3 class="section-title">Confirm and Subscribe</h3>
                    </div>
                    <span class="section-divider"></span>
                </div>
                <div class="row">
                    @if($data['plan'] == 'free')
                        <div class="col-md-6 offset-md-3">
                            <input type="hidden" value="{{ $data['plan'] }}" name="plan" id="plan_id">
                            <input type="hidden" value="{{ $data['plan_interval'] }}" name="plan_interval" id="plan_interval">
                            <input type="hidden" value="{{ $data['date'] }}" id="datepicker">
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <td><img class="tab-list-img" src="/img/bullet.png" />Free trial</td>
                                    <td>21 days</td>
                                </tr>
                                    @foreach(config('plans.subscriptions.freshdesk.plans')[$data['plan']]['features'] as $keyfeature => $feature)
                                        <tr>
                                            <td><img class="tab-list-img" src="/img/bullet.png" />{{ $keyfeature }}</td>
                                            <td>{{ $feature }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" id="terms_check">I accept the <a href="{{ route('termsPage') }}" target="_blank">Terms of Service</a>
                                </label>
                            </div>
                            <button type="button" class="btn button_theme_color subscribe_btn" onclick="subscribeFreshdeskFreePlan(event)">Subscribe</button><i class="fa fa-spinner fa-spin ml-2" id="spinner" style="font-size:24px;display: none"></i>
                        </div>
                    @else
                        <div class="col-md-6">
                            <input type="hidden" value="{{ $data['stripe_plan'] }}" name="plan" id="stripe_plan_id">
                            <input type="hidden" value="{{ $data['plan_interval'] }}" name="plan_interval" id="plan_interval">
{{--                            <input type="hidden" value="{{ $data['plan'] }}" name="plan" id="plan_id">--}}
{{--                            <input type="hidden" value="{{ $data['date'] }}" id="datepicker">--}}
                            <table class="table table-borderless">
                                <tbody>
{{--                                    <tr>--}}
{{--                                        <td><img class="tab-list-img" src="/img/bullet.png" />Historical Ticket Fees</td>--}}
{{--                                        <td>${{ $data['setup_fee'] }}</td>--}}
{{--                                    </tr>--}}
                                    @if($data['plan_interval'] == 'annual')
                                    <tr>
                                        <td><img class="tab-list-img" src="/img/bullet.png" />Recurring yearly fees</td>
                                        <td>${{ $data['plan_amount'] }}</td>
                                        <!-- <td>${{ $data['plan_amount']-(round($data['plan_amount']*20/100)) }} <span style="font-size: 14px;color:#FF8500;font-weight: bold;">20% Off</span></td> -->
                                    </tr>
                                    @else
                                    <tr>
                                        <td><img class="tab-list-img" src="/img/bullet.png" />Recurring monthly fees</td>
                                        <td>${{ $data['plan_amount'] }}</td>
                                    </tr>
                                    @endif
{{--                                    <tr>--}}
{{--                                        <td><img class="tab-list-img" src="/img/bullet.png" />Historical tickets loaded from</td>--}}
{{--                                        <td>{{ $data['date'] }}</td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <td><img class="tab-list-img" src="/img/bullet.png" />Total historical ticket from date</td>--}}
{{--                                        <td>{{ $data['avgTicketCount'] }}</td>--}}
{{--                                    </tr>--}}
                                @foreach(config('plans.subscriptions.freshdesk.plans')[$data['plan']]['features'] as $keyfeature => $feature)
                                    <tr>
                                        <td><img class="tab-list-img" src="/img/bullet.png" />{{ $keyfeature }}</td>
                                        <td>{{ $feature }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{--<div class="form-check">--}}
                                {{--<label class="form-check-label">--}}
                                    {{--<input type="checkbox" class="form-check-input" id="terms_check">I accept the terms and conditions--}}
                                {{--</label>--}}
                            {{--</div>--}}
                        </div>
                        <div class="col-md-6">
                            <div class="card-wrapper"></div>
                            <form class="card-form">
                                @include('billing.card-form')
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" id="terms_check" required>I accept the <a href="{{ route('termsPage') }}" target="_blank">Terms of Service</a>
                                    </label>
                                </div>
                                <button type="button" class="btn button_theme_color subscribe_btn" onclick="subscribeFreshdeskPlan(event)">Subscribe</button><i class="fa fa-spinner fa-spin ml-2" id="spinner" style="font-size:24px;display: none"></i>
                            </form>
                        </div>
                    @endif
                </div>
            </div>
        </section>
@stop