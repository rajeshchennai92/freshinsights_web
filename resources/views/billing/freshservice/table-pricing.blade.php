<div class="col-md-12">
    <ul class="nav nav-tabs w-100 justify-content-center mb-3" id="pricingTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="monthly-tab" data-toggle="tab" href="#monthly" role="tab" aria-controls="home" aria-selected="true">Monthly</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="annually-tab" data-toggle="tab" href="#annually" role="tab" aria-controls="contact" aria-selected="false">Yearly</a>
        </li>
    </ul>
    <div class="tab-content" id="pricingTabContent">
        <div class="tab-pane fade show active" id="monthly" role="tabpanel" aria-labelledby="home-tab">
            <div class="col-md-12 table-responsive">
                <table class="table pricing-table">
                    <thead>
                    <tr>
                        <th></th>
                        @foreach(config('plans.subscriptions.freshservice.plans') as $key => $plan)
                            <th class="pricing-table-head p-0">
                                <h3>{{ $plan['name'] }}<br>
                                    @if($key == 'free')
                                        21 Days
                                    @else
                                        ${{ $plan['plan_amount'] }}/mo
                                    @endif
                                </h3>
                            </th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @foreach(config('plans.subscriptions.freshservice.plans.free.features') as $key => $feature)
                        <tr>
                            <td>{{ $key }}<span class="error">{{ $key=='Historical Tickets Loading Limit'?'*':'' }}</span><span class="error">{{ $key=='Monthly incremental tickets limit'?'**':'' }}</span><span class=" pl-2 fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="{{ config('plans.subscriptions.freshservice.plans.free.tooltip')[$key] }}"></span></td>
                            @foreach(config('plans.subscriptions.freshservice.plans') as $key_plan => $plan)
                                <td>{{ $plan['features'][$key] }}</td>
                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane fade" id="annually" role="tabpanel" aria-labelledby="contact-tab">
            <div class="col-md-12 table-responsive">
                <table class="table pricing-table">
                    <thead>
                    <tr>
                        <th></th>
                        @foreach(config('plans.subscriptions.freshservice.plans') as $key => $plan)
                            <th class="pricing-table-head p-0">
                                <h3>{{ $plan['name'] }}<br>
                                    @if($key == 'free')
                                        21 Days
                                    @else
                                    USD {{ $plan['annual_plan_amount'] }}/annual
                                        <!-- <span style="text-decoration: line-through;font-size: 14px;">USD {{ $plan['annual_plan_amount'] }}/annual</span> <span style="font-size: 14px;color:#FF8500;font-weight: bold;">20% Off</span><br>
                                        USD {{ $plan['annual_plan_amount']-(round($plan['annual_plan_amount']*20/100)) }}/annual -->
                                    @endif
                                </h3>
                            </th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @foreach(config('plans.subscriptions.freshservice.plans.free.features') as $key => $feature)
                        <tr>
                            <td>{{ $key }}<span class="error">{{ $key=='Historical Tickets Loading Limit'?'*':'' }}</span><span class="error">{{ $key=='Monthly incremental tickets limit'?'**':'' }}</span><span class=" pl-2 fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="{{ config('plans.subscriptions.freshservice.plans.free.tooltip')[$key] }}"></span></td>
                            @foreach(config('plans.subscriptions.freshservice.plans') as $key_plan => $plan)
                                <td>{{ $plan['features'][$key] }}</td>
                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12" style="font-size: 13px">
    <p class="mb-0" style="font-size: 13px"><span class="error">*</span>&nbsp;For additional historical tickets to be loaded, it would be charged at the rate of $1 for every 1,000 tickets.
    </p>
    <p class="mb-0" style="font-size: 13px"><span class="error">**</span>&nbsp;For additional incremental tickets during the month, it would be charged at the rate of $1 for every 100 tickets.
    </p>
</div>
<div class="col-lg-12 pt-3" style="font-size: 13px">
    <p class="mb-0 pt-1 pr-3 text-left pb-2">The subscription amount is charged on a monthly basis, not on a pro data basis and is non-refundable. If you cancel subscription in between the billing cycle, you will not be refunded, however you can continue using the services till the end of the billing cycle.</p>
</div>
<div class="col-lg-12 pt-3" style="overflow: hidden">
    <p style="font-size: 0.9rem;" class="mb-0 pt-1 pr-3 text-left pb-2"><b>Additional Services:</b> Customisation of dashboards and creation of new dashboards would be provided at a nominal pricing based on the requirements. Please reach out to <a href="mailto:{{ env('FRESHDESK_SUPPORT_EMAIL') }}">{{ env('FRESHDESK_SUPPORT_EMAIL') }}</a> for a quote.</p>
</div>