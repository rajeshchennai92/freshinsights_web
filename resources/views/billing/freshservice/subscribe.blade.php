@extends('user.layouts.master')

@section('title', 'FreshInsights - Freshservice Pricing')

@section('individual-style')
    @include('billing.freshservice.style')
@stop

@section('pre-javascript')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script> Stripe.setPublishableKey('{{ Config::get("services.stripe.key") }}'); </script>
@stop

@section('user-page-content')
    @include('billing.freshservice.navigation')
    <main id="main">
        <section id="about-us" class="section-bg section-about-bg">
            <div class="container">
                <div class="section-header">
                    <div>
                        <h3 class="section-title"><img src="/img/powerbi.png" style="vertical-align:top;
"> Power BI Reporting on Freshservice</h3>
                        <h3 class="section-title">Pricing Plans</h3>
                    </div>
                    <span class="section-divider"></span>
                </div>
                <div class="row">
                    {{--<div class="col-12">--}}
                        {{--<div class="setup-section mb-3 text-center">--}}
                            {{--<h6 class="pt-4 mb-1">Setup Fee - ${{ config('plans.subscriptions.freshservice.setup_fee') }}</h6>--}}
                            {{--<h6 class="mb-1 pb-3">Additional fee/10000 tickets - ${{ config('plans.subscriptions.freshservice.additional_setup_fee') }}</h6>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    @include('billing.freshservice.table-pricing')
                    <div class="col-lg-12 pt-3" style="overflow: hidden">
                        @if(!Auth::user())
                            <p style="font-size: 0.9rem;" class="mb-0 pt-1 pr-3 text-center pb-2"> To register for the pricing plan of your choice, please visit the registration page.<a href="/register/freshservice?for=1" style="margin-left: 20px;"><button class="btn button_theme_color btn_register button_theme_color_sm">Register Here</button></a></p>
                            {{--<p class="mb-0 pt-1 pr-3 text-center" style="font-size: 0.9rem;"><a href="#pricing-calc" class="scrollto"><u>Use our Pricing Calculator to estimate your fees.</u> <i class="fa fa-angle-double-right arrow1" aria-hidden="true"></i></a></p>--}}
                        @else
                            {{--<p class="mb-0 pt-1 pr-3 text-center" style="font-size: 0.9rem;"><a href="#pricing-calc" class="scrollto">Use our Pricing Calculator to estimate your fees. <i class="fa fa-angle-double-right arrow1" aria-hidden="true"></i></a></p>--}}
                        @endif
                    </div>
                </div>
            </div>
        </section>

{{--        <section id="pricing-calc">--}}
{{--            <div class="container">--}}
{{--                <div class="section-header">--}}
{{--                    <div>--}}
{{--                        <h3 class="section-title">Pricing Calculator</h3>--}}
{{--                    </div>--}}
{{--                    <span class="section-divider"></span>--}}
{{--                </div>--}}
{{--                <div class="row">--}}
{{--                    <div class="col-12 tabs" id="tabs">--}}
{{--                        <div class="col-md-12">--}}
{{--                            <form name="pricingForm">--}}
{{--                                <div class="col-md-6 float-left">--}}
{{--                                    <h6 class="raw-color-000000 pb-4 text-center" style="font-size: 14px;">Select the number of historical tickets to be loaded</h6>--}}
{{--                                    <div class="col-md-10 offset-md-1">--}}
{{--                                        <input class="range-slider__range" orient="horizontal" min="5000" max="500000" step="500" offset="0" value="5000" type="range" name="setupfee">--}}
{{--                                        <output for="setup" onforminput="value = setupfee.valueAsNumber;"></output>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-md-6 float-left">--}}
{{--                                    <h6 class="raw-color-000000 pb-4 text-center" style="font-size: 14px;">Select the likely number of monthly tickets volume</h6>--}}
{{--                                    <div class="col-md-10 offset-md-1">--}}
{{--                                        <input class="range-slider__range" orient="horizontal" min="1000" max="30000" step="500" offset="0" value="1000" type="range" name="monthlyfee">--}}
{{--                                        <output for="monthlyfee" onforminput="value = monthlyfee.valueAsNumber;"></output>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </form>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-12" id="compare_pricing_section">--}}
{{--                        @foreach(config('plans.subscriptions.freshservice.plans') as $key => $plan)--}}
{{--                            <div class="col-md-5-col col-sm-5-col col-xs-5-col">--}}
{{--                                <div class="pricing hover-effect">--}}
{{--                                    <div class="pricing-head">--}}
{{--                                        <h3>{{ $plan['name'] }}</h3>--}}
{{--                                        --}}{{--<h4><i>$</i>{{ $plan['plan_amount'] }}--}}
{{--                                        --}}{{--<span>--}}
{{--                                        --}}{{--Per Month </span>--}}
{{--                                        --}}{{--</h4>--}}
{{--                                    </div>--}}
{{--                                    <table class="table table-borderless">--}}
{{--                                        <tr>--}}
{{--                                            <td><strong>Historical Ticket Fees</strong></td>--}}
{{--                                            <td>$<span id="setup_fee_calc_{{ $plan["plan_amount"] }}">{{ config('plans.subscriptions.freshservice.setup_fee') }}</span></td>--}}
{{--                                        </tr>--}}
{{--                                        <tr>--}}
{{--                                            <td><strong>Avg Monthly Fees</strong></td>--}}
{{--                                            <td>$<span id="monthly_fee_calc{{ $plan['plan_amount'] }}">{{ $plan['plan_amount'] }}</span></td>--}}
{{--                                        </tr>--}}
{{--                                    </table>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        @endforeach--}}
{{--                    </div>--}}
{{--                    <div class="col-lg-12 text-center">--}}
{{--                        @if(!Auth::user() || !Auth::user()->freshserviceClients)--}}
{{--                            <p style="font-size: 0.9rem;" class="mb-0 pt-1 pr-3 text-center pb-2"> To register for the pricing plan of your choice, please visit the registration page.<a href="/register/freshservice" style="margin-left: 20px;"><button class="btn button_theme_color btn_register button_theme_color_sm">Register Here</button></a></p>--}}
{{--                        @elseif($fsSubscription == null)--}}
{{--                            <p style="font-size: 0.9rem;" class="mb-0 pt-1 pr-3 text-center pb-2"> To subscribe for the pricing plan of your choice, please click the choose plan.<a href="{{ route('freshserviceSubscriptionChoose') }}" style="margin-left: 20px;"><button class="btn button_theme_color btn_register button_theme_color_sm">Choose plan</button></a></p>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </section>--}}

        <!--==========================
          Frequently Asked Questions Section
        ============================-->
        <section id="faq">
            <div class="container">

                <div class="section-header">
                    <h3 class="section-title">Frequently Asked Questions</h3>
                    <span class="section-divider"></span>
                    {{--<p class="section-description pb-0 text-center">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>--}}
                </div>

                <ul id="faq-list" class="wow fadeInUp">
                    <li>
                        <a data-toggle="collapse" class="collapsed" href="#faq1">We are already paying for Freshservice? Why should we pay for this reporting service?<i class="ion-android-remove"></i></a>
                        <div id="faq1" class="collapse" data-parent="#faq-list">
                            <p>
                                The FreshInsights™ for Freshservice offers superlative PowerBI’s visualisation features and is offered as a separate service by a third-party provider named Radiare. The services include building connectors between your Freshservice instance and the data models that churn out the PowerBI reports with data refreshed at regular intervals. This is provided outside of the Freshservice environment on our secure Azure cloud environment. The nominal payment that is asked is for the regular refreshing of the data, allowing sharing of PowerBI internally to other stakeholders in the organisation, ensuring that API connectors are kept upto date with the changes as and when they are made on the Freshservice side.
                            </p>
                            <p>
                                Also please note that the subscription fees is irrespective of the number of agents that you may have with a restriction on the number of tickets that will shown on the reports based on the plan that you choose.
                            </p>
                        </div>
                    </li>

                    <li>
                        <a data-toggle="collapse" class="collapsed" href="#faq2">How can we make this payment?<i class="ion-android-remove"></i></a>
                        <div id="faq2" class="collapse" data-parent="#faq-list">
                            <p>
                                There are two options available for making the payment. We have enabled Stripe for monthly recurring payments or alternately we can raise a quarterly or annual invoice based on which you can do a bank transfer.
                            </p>
                        </div>
                    </li>

                    <li>
                        <a data-toggle="collapse" class="collapsed" href="#faq3">I am not able to edit the changes to the PowerBI Dashboards. I want make changes, how do I do that?<i class="ion-android-remove"></i></a>
                        <div id="faq3" class="collapse" data-parent="#faq-list">
                            <p>
                                The FreshInsights™ PowerBI Dashboards offer most of the common features that are sought after by customers. However if you want any additional feature there are two ways you can do it.
                            </p>
                            <p>
                                1.	We can take your requirements and build the customised dashboard for you for a nominal fee. Typical customizations can be done for prices ranging from $500-$2,000. Our customer service manager will provide you with an estimation and take your approval and deliver the dashboard to your satisfaction.
                            </p>
                            <p>
                                2.	Second option is for you is to purchase the FreshInsights™ dashboards along with the underlying data models and do the customisation yourself. In this scenario we will do the entire deployment on-premise on your server and we will continue to provide data refresh services between your Freshservice instance and the FreshInsights data base. You can reach out to our support team and our customer service manager will reach out to you with a quote for the same.

                            </p>
                        </div>
                    </li>
                    
                    <li>
                        <a data-toggle="collapse" class="collapsed" href="#faq4">Where is the dashboard located in my Power BI account?  Or, do I have to click the link each time I want to see?<i class="ion-android-remove"></i></a>
                        <div id="faq4" class="collapse" data-parent="#faq-list">
                            <p>
                                The dashboard is accessible via the link sent to your email address. It is NOT available under your Power BI account options bar.
                            </p>
                        </div>
                    </li>

                    <li>
                        <a data-toggle="collapse" href="#faq5" class="collapsed">How do I integrate Freshservice to Power BI? I do not see an option to make the connection in Power BI pro to the Freshservice server.<i class="ion-android-remove"></i></a>
                        <div id="faq5" class="collapse" data-parent="#faq-list">
                            <p>
                                You do not have to perform an integration between Freshservice and Power BI to view your dashboard. Once we share the dashboard, you will receive an email containing the dashboard link that has the information to access your dashboard at any point in time.
                            </p>
                        </div>
                    </li>

                    {{--<li>--}}
                        {{--<a data-toggle="collapse" href="#faq6" class="collapsed">How can I share this dashboard with other users within my organization? <i class="ion-android-remove"></i></a>--}}
                        {{--<div id="faq6" class="collapse" data-parent="#faq-list">--}}
                            {{--<p>--}}
                                {{--You will not able to share the dashboard with other users as read-only access is provided per       user. However, you can upgrade to the paid services where you will be able to add more users    and the dashboard will be mailed to them by our team.--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</li>--}}

                    <li>
                        <a data-toggle="collapse" href="#faq7" class="collapsed">Are we able to customize this dashboard at all? For example, I'm looking to add our Logo instead of the Freshservice Logo and take specific items from the dashboard you created.<i class="ion-android-remove"></i></a>
                        <div id="faq7" class="collapse" data-parent="#faq-list">
                            <p>
                                Currently read-only access is provided to users for the dashboard. Any customization to the standard dashboards will be available as part of our paid services.
                            </p>
                        </div>
                    </li>

                    <li>
                        <a data-toggle="collapse" href="#faq8" class="collapsed">Is there a way to filter the dashboard by Group?  If not, can it be added? <i class="ion-android-remove"></i></a>
                        <div id="faq8" class="collapse" data-parent="#faq-list">
                            <p>
                                At present, we have a graph which displays "Ticket Volume by Group" under Ticket Analysis tab. Selecting any specific group in this graph will filter the entire report based on the selected group. If you need Group to be a part of the report filter as a dropdown, you can upgrade to our paid services and avail such customizations.
                            </p>
                        </div>
                    </li>

                    <li>
                        <a data-toggle="collapse" href="#faq9" class="collapsed">Why do I have to pay the monthly fee? <i class="ion-android-remove"></i></a>
                        <div id="faq9" class="collapse" data-parent="#faq-list">
                            <p>
                                Radiare has spent significant time and energy in building the dashboards and reports. Also the underlying data structures are built in a suitable fashion to support the periodic data freshes from your Freshservice instance. To keep up with the data infrastructure costs and the reporting layer that our team has built, we charge a nominal fee that is based on your data volume.
                            </p>
                        </div>
                    </li>

                    <li>
                        <a data-toggle="collapse" href="#faq10" class="collapsed">Can I cancel the subscription and will I get a refund? <i class="ion-android-remove"></i></a>
                        <div id="faq10" class="collapse" data-parent="#faq-list">
                            <p>
                                Yes, you can cancel anytime as the services are offered as “pay as you go” kind of model. If it is monthly pricing plan you will not get any refund but you can continue to use the services until end of the month. If it is an bi-annual or annual subscription, you will get a refund for the months that are remaining excluding the current month.
                            </p>
                        </div>
                    </li>

                    <li>
                        <a data-toggle="collapse" href="#faq11" class="collapsed">Can I buy the PBIX file so that I can have my team to customize as needed? <i class="ion-android-remove"></i></a>
                        <div id="faq11" class="collapse" data-parent="#faq-list">
                            <p>
                                Yes , we offer PBIX files for a minimal fee. You can reach out to us at <a class="p-0 d-inline" href="mailto:freshservicesupport@radiare.com">freshservicesupport@radiare.com</a> and we will respond within 24 hours.
                            </p>
                        </div>
                    </li>

                    <li>
                        <a data-toggle="collapse" href="#faq12" class="collapsed">I would like to customize my reports the way they are rendered. Would you do it for me? <i class="ion-android-remove"></i></a>
                        <div id="faq12" class="collapse" data-parent="#faq-list">
                            <p>
                                Yes, we offer our services to customize the reports and also continue to maintain your version. There will be additional development fees involved based on your customization needs. We ensure that this is kept to a minimal value just to cover our costs of customisation. You can reach out to us at <a class="p-0 d-inline" href="mailto:freshservicesupport@radiare.com">freshservicesupport@radiare.com</a> and we will respond within 24 hours.
                            </p>
                        </div>
                    </li>

                    <li>
                        <a data-toggle="collapse" href="#faq13" class="collapsed">I do not want to pay by my card, is there an alternate way of paying the subscription? <i class="ion-android-remove"></i></a>
                        <div id="faq13" class="collapse" data-parent="#faq-list">
                            <p>
                                Yes, we accept bank transfers. Please reach out to us at <a class="p-0 d-inline" href="mailto:freshservicesupport@radiare.com">freshservicesupport@radiare.com</a> with your request and we will generate an invoice with all details needed for you to do the bank transfer.
                            </p>
                        </div>
                    </li>
                </ul>

            </div>
        </section><!-- #faq -->

        @stop

@section('post-price-javascript')
    <script>
        function calculatePrice() {
            var avgTicketCount = $("input[name='setupfee']").val();
            var monthlyTicketCount = $("input[name='monthlyfee']").val();

            @forEach(config('plans.subscriptions.freshservice.plans') as $key => $plan)
                var setup_fee = {{ config('plans.subscriptions.freshservice.setup_fee') }};
                var additional_setup_fee = {{ config('plans.subscriptions.freshservice.additional_setup_fee') }};
                var plan_amount = {{ $plan['plan_amount'] }};
                var allowed_ticket_count = {{ $plan['allowed_ticket_count'] }};
                var allowed_history_ticket_count = {{ $plan['allowed_history_ticket_count'] }};
                var additional_fee = {{ $plan['additional_fee'] }};
                if (+avgTicketCount>+allowed_history_ticket_count){
                    setup_fee = +setup_fee + +(Math.round((avgTicketCount-allowed_history_ticket_count)/1000)*additional_setup_fee);
                    $('#compare_pricing_section #setup_fee_calc_{{ $plan["plan_amount"] }}').html(setup_fee);
                    $('#compare_pricing_section #setup_fee_calc_0').html(0);
                }
                else {
                    $('#compare_pricing_section #setup_fee_calc_{{ $plan["plan_amount"] }}').html(setup_fee);
                    $('#compare_pricing_section #setup_fee_calc_0').html(0);
                }

                if (+monthlyTicketCount>+allowed_ticket_count){
                    plan_amount = +plan_amount + +(Math.round(((monthlyTicketCount-allowed_ticket_count)/100)*additional_fee));
                    $('#compare_pricing_section #monthly_fee_calc{{ $plan["plan_amount"] }}').html(plan_amount);
                }
                else {
                    $('#compare_pricing_section #monthly_fee_calc{{ $plan["plan_amount"] }}').html(plan_amount);
                }
            @endforeach
        }
        var el, newPoint, newPlace, offset;

        $('input[type=range]').on('input', function () {
            $(this).trigger('change');
        });
        // Select all range inputs, watch for change
        $("input[name='setupfee']").change(function() {

            // Cache this for efficiency
            el = $(this);

            // Measure width of range input
            width = el.width();

            // Figure out placement percentage between left and right of input
            newPoint = (el.val() - el.attr("min")) / (el.attr("max") - el.attr("min"));

            offset = 1;

            // Prevent bubble from going beyond left or right (unsupported browsers)
            if (newPoint < 0) { newPlace = 0; }
            else if (newPoint > 1) { newPlace = width; }
            else { newPlace = width * newPoint + offset; offset -= newPoint; }

            // Move bubble
            el
                .next("output")
                .css({
                    left: newPlace,
                    marginLeft: offset + "%",
                })
                .text(el.val());

            $(this).css('background',
                '-webkit-gradient(linear, left top, right top, '
                + 'color-stop(' + newPoint + ', #003a47), '
                + 'color-stop(' + newPoint + ', #e2e2e2)'
                + ')'
            );
            calculatePrice();
        })
        // Fake a change to position bubble at page load
            .trigger('change');

        // Select all range inputs, watch for change
        $("input[name='monthlyfee']").change(function() {

            // Cache this for efficiency
            el = $(this);

            // Measure width of range input
            width = el.width();

            // Figure out placement percentage between left and right of input
            newPoint = (el.val() - el.attr("min")) / (el.attr("max") - el.attr("min"));

            offset = 1;

            // Prevent bubble from going beyond left or right (unsupported browsers)
            if (newPoint < 0) { newPlace = 0; }
            else if (newPoint > 1) { newPlace = width; }
            else { newPlace = width * newPoint + offset; offset -= newPoint; }

            // Move bubble
            el
                .next("output")
                .css({
                    left: newPlace,
                    marginLeft: offset + "%",
                })
                .text(el.val());

            $(this).css('background',
                '-webkit-gradient(linear, left top, right top, '
                + 'color-stop(' + newPoint + ', #003a47), '
                + 'color-stop(' + newPoint + ', #e2e2e2)'
                + ')'
            );
            calculatePrice();
        })
        // Fake a change to position bubble at page load
            .trigger('change');

    </script>
@stop