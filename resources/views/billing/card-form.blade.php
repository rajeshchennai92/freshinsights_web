{!! csrf_field() !!}

<input type="hidden" id="exp_month" data-stripe="exp-month">
<input type="hidden" id="exp_year" data-stripe="exp-year">

<div class="form-group number">
    <label for="number">Number</label>
    <input class="form-control" type="text" required id="card_number" name="number" placeholder="Card Number" value="{{ old('number') }}" data-stripe="number">
</div>

<div class="form-group name">
    <label for="name">Name</label>
    <input class="form-control" type="text" required id="card_holder_name" name="name" placeholder="Full Name" value="{{ old('name') }}" data-stripe="name">
</div>

<div class="form-group expiry">
    <label for="expiry">Expiry Date</label>
    <input class="form-control" type="text" required id="expiry_date" name="expiry" placeholder="MM/YYYY" value="{{ old('expiry') }}">
</div>

<div class="form-group cvc">
    <label for="cvc">CVC</label>
    <input class="form-control" type="password" required id="cvv_number" name="cvc" placeholder="CVC" value="{{ old('cvc') }}" data-stripe="cvc">
</div>

<div class="payment-errors">
</div>
