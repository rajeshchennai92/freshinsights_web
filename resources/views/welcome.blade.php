@extends('user.layouts.master')

@section('title', 'Freshinsights')

@section('user-page-content')

    <!--==========================
  Intro Section
============================-->
    <section id="intro">

        <div class="intro-text">
            <h2>Welcome to FreshInsights</h2>
            <p>Enabling Digital Intelligence</p>
        </div>

    </section><!-- #intro -->

    <main id="main">

        <!--==========================
      About Us Section
    ============================-->
        <section id="solution" class="section-about-bg section-bg">
            <div class="container-fluid">
                <div class="row">
                    {{--<div class="col-lg-6 about-img wow fadeInLeft">--}}
                        {{--<img src="img/homepage-about.gif" alt="">--}}
                    {{--</div>--}}

                    <div class="content pr-3 pl-3">
                        <div class="section-header">
                            <div>
                                <h3 class="section-title text-center" style="font-size: 24px;font-style: normal;color: #003a47">Our Solutions</h3>
                            </div>
                            <span class="section-divider"></span>
                        </div>
                        <div class="row text-center">
                            <span class="section-divider"></span>
                            <div class="col-sm-6 mt-3">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title"><img src="/img/freshdesk.png"/> Freshdesk Reporting</h5>
                                        <p class="card-text text-left"><a href="https://apps.freshdesk.com/freshdesk_on_powerbi/" target="_blank">"FreshInsights on PowerBI for Freshdesk"</a> and <a href="https://apps.freshdesk.com/freshdesk_on_powerbi/" target="_blank">"FreshInsights on Tableau for Freshdesk"</a> are available in the Freshdesk marketplace to all Freshdesk customers who can consume the basic Reports and Dashboards leveraging their existing Microsoft Power BI and Tableau product suite without any further investments. </p>
{{--                                        <a href="{{ route('freshdeskPage') }}" class="btn button_theme_color_sm button_theme_color">View Solution</a>--}}
                                        <a href="{{ route('freshdeskPage') }}" class="btn button_theme_color_sm button_theme_color" style="margin-right: 5px;">View Solution in Power BI</a>
                                        <a href="{{ route('freshdeskTableauPage') }}" class="btn button_theme_color_sm button_theme_color" style="margin-left: 5px;padding: 4px 20px;">View Solution in Tableau</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 mt-3">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title"><img src="/img/freshservice.png"/> Freshservice Reporting</h5>
                                        <p class="card-text text-left"><a href="https://apps.freshservice.com/freshservice_on_powerbi/" target="_blank">"FreshInsights on PowerBI for Freshservice"</a> and <a href="https://apps.freshservice.com/tableau_reporting_on_freshservice/" target="_blank">"FreshInsights on Tableau for Freshservice"</a> are available in the Freshservice marketplace to all Freshservice customers who can consume the basic Reports and Dashboards leveraging their existing Power BI and Tableau product suite without any further investments.</p>
                                        <a href="{{ route('freshservicePage') }}" class="btn button_theme_color_sm button_theme_color" style="margin-right: 5px;">View Solution in Power BI</a>
                                        <a href="{{ route('freshserviceTableauPage') }}" class="btn button_theme_color_sm button_theme_color" style="margin-left: 5px;padding: 4px 20px;">View Solution in Tableau</a>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="col-sm-4 mt-3">--}}
                                {{--<div class="card">--}}
                                    {{--<div class="card-body">--}}
                                        {{--<h5 class="card-title"><img src="/img/freshinsights.png"/> Other Visualizations</h5>--}}
                                        {{--<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>--}}
                                        {{--<a href="{{ route('otherPage') }}" class="btn btn-primary button_theme_color_sm button_theme_color">View Solution</a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                </div>

            </div>
        </section><!-- #about -->


        <!--==========================
      About Us Section
    ============================-->
        <section id="about-us" class="section-bg">
            <div class="container-fluid">
                <div class="section-header">
                    <div>
                        <h3 class="section-title">About Us</h3>
                    </div>
                    <span class="section-divider"></span>
                    <div class="col-12">
                        <div class="col-lg-2 col-md-2" style="float: left">
                            <img src="img/logo.png" style="width: 100%;padding-bottom: 20px;">
                        </div>
                        <div class="col-lg-10 col-md-10" style="float: left">
                            <p class="section-description mb-0 text-left">
                                FreshInsights is a brand of Radiare. It encompasses various platforms built towards effectively managing and mining the client's enterprise data. The platform offers visually intuitive dashboards and analytical reports that enables clients to get effective insights from their data.</p>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="col-lg-2 col-md-2" style="float: left">
                            <img src="img/radiare.png" style="padding-top: 10px;padding-bottom: 20px;margin: 0 35px;width: 60%;">
                        </div>
                        <div class="col-lg-10 col-md-10" style="float: left">
                            <p class="section-description mb-0  text-left pb-2">Radiare Software Solutions is a new age Digital Intelligence Solutions company that helps our customers to make informed business decisions in the evolving digital space. Radiare specializes in providing solutions and services in the areas of Visualization, Analytics and Data Management solutions. We work with clients in a consultative manner in building decision making solutions using structured and unstructured data in a seamless manner.<br>
                            </p>
                            <p class="section-description mb-0 text-left">For more information on Radiare, visit us at <a href="http://www.radiare.com" target="_blank">www.radiare.com.</a>
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </section><!-- #about-us -->

@stop

