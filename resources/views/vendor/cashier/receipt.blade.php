<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Invoice</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            background: #fff;
            background-image: none;
            font-size: 12px;
        }
        address{
            margin-top:15px;
        }
        h2 {
            font-size:28px;
            color:#cccccc;
        }
        .container {
            padding-top:30px;
        }
        .invoice-head td {
            padding: 0 8px;
        }
        .invoice-body{
            background-color:transparent;
        }
        .logo {
            padding-bottom: 10px;
        }
        .table th {
            vertical-align: bottom;
            font-weight: bold;
            padding: 8px;
            line-height: 20px;
            text-align: left;
        }
        .table td {
            padding: 8px;
            line-height: 20px;
            text-align: left;
            vertical-align: top;
            border-top: 1px solid #dddddd;
        }
        .well {
            margin-top: 15px;
        }
    </style>
</head>

<body>
<div class="container">
    <table style="margin-left: auto; margin-right: auto" width="550">
        <tr>
            <!-- Organization Name / Image -->
            <td>
                <table width="100%" border="0">
                    <tr>
                        <td><img src="{{ public_path().$url }}"></td>
                        <td  align="right" style="padding-top:20px;font-size:16px;">
                            <strong>{{ $company }}</strong><br>
                            @if (isset($street))
                                {{ $street }}<br>
                            @endif
                            @if (isset($location))
                                {{ $location }}<br>
                            @endif
                            @if (isset($country))
                                {{ $country }}<br>
                            @endif
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr valign="top">
            <!-- Organization Name / Date -->
            <td>
                <table width="100%" border="0">
                    <tr>
                        <td>
                            <br><br>
                            <strong>Bill To:</strong> {{ $user_name }}
                        </td>
                        <td  align="right" style="padding-top:20px;font-size:16px;">
                            <br><br>
                            <strong>Invoice Date:</strong> {{ $invoice->date()->toFormattedDateString() }}
                            <br>
                            <strong>Invoice Number:</strong> {{ $invoice->number }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr valign="top">
            <!-- Organization Details -->
            <td>
                <br><br>

                <!-- Invoice Table -->
                <table width="100%" class="table" border="0">
                    <tr>
                        <th align="left">Solutions</th>
                        <th align="right">Date</th>
                        <th align="right">Quantity</th>
                        <th align="right">Amount</th>
                    </tr>

                    <!-- Display The Invoice Items -->
                    @foreach ($invoice->invoiceItems() as $item)
                        <tr>
                            <td>{{ $item->description }}</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>{{ $item->total() }}</td>
                        </tr>
                    @endforeach

                <!-- Display The Subscriptions -->
                    @foreach ($invoice->subscriptions() as $subscription)
                        <tr>
                            <td>{{ $subscription->plan->nickname }} for {{ $subscription->plan->product }}</td>
                            <td>
                                {{ $subscription->startDateAsCarbon()->formatLocalized('%B %e, %Y') }} -
                                {{ $subscription->endDateAsCarbon()->formatLocalized('%B %e, %Y') }}
                            </td>
                            <td>{{ $subscription->quantity }}</td>
                            <td>{{ $subscription->total() }}</td>
                        </tr>
                    @endforeach

                <!-- Display The Discount -->
                    @if ($invoice->hasDiscount())
                        <tr>
                            @if ($invoice->discountIsPercentage())
                                <td>{{ $invoice->coupon() }} ({{ $invoice->percentOff() }}% Off)</td>
                            @else
                                <td>{{ $invoice->coupon() }} ({{ $invoice->amountOff() }} Off)</td>
                            @endif
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>-{{ $invoice->discount() }}</td>
                        </tr>
                    @endif

                <!-- Display The Tax Amount -->
                    @if ($invoice->tax_percent)
                        <tr>
                            <td>Tax ({{ $invoice->tax_percent }}%)</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>{{ Laravel\Cashier\Cashier::formatAmount($invoice->tax) }}</td>
                        </tr>
                @endif

                <!-- Display The Final Total -->
                    <tr style="border-top:2px solid #000;">
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><strong>Total</strong></td>
                        <td><strong>{{ $invoice->total() }}</strong></td>
                    </tr>
                    <tr style="border-top:2px solid #000;">
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><strong>Amont Paid</strong></td>
                        <td><strong>{{ $invoice->total() }}</strong></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
