@extends('user.layouts.master')

@section('title', 'FreshInsights - Freshdesk Login')

@section('individual-style')
    @include('billing.freshdesk.style')
@stop

@section('user-page-content')
    @include('billing.freshdesk.navigation')
   <main id="main">
       <section id="about-us" class="section-bg">
           <div class="col-md-8 offset-md-2">

               <h2 class="text-center">Please sign in</h2>

               <form method="POST" action="/freshdesk/login">
                   {!! csrf_field() !!}
                   <div class="col-md-12 raw-margin-top-24">
                       <label>Email</label>
                       <input class="form-control" type="email" name="email" placeholder="Email" value="{{ old('email') }}">
                   </div>
                   <div class="col-md-12 raw-margin-top-24">
                       <label>Password</label>
                       <input class="form-control" type="password" name="password" placeholder="Password" id="password">
                   </div>
                   <div class="col-md-12 raw-margin-top-24">
                       <label>
                           Remember Me <input type="checkbox" name="remember">
                       </label>
                   </div>
                   @include('partials.errors')
                   <div class="col-md-12 raw-margin-top-24">
                       <div class="btn-toolbar justify-content-between">
                           <button class="btn btn-primary button_theme_color" type="submit">Sign in</button>
                           <a class="btn btn-link" href="/password/reset">Forgot Password</a>
                       </div>
                   </div>

                   {{--<div class="col-md-12 raw-margin-top-24">--}}
                       {{--<a class="btn raw100 button_theme_color" href="/register/freshdesk">Register</a>--}}
                   {{--</div>--}}
               </form>

           </div>
       </section>
   </main>

@stop

