@extends('user.layouts.master')

@section('title', 'FreshInsights - Reset Password')

@section('individual-style')
    <style>
        #header{
            background: #fff;
            padding: 20px 0;
            height: 72px;
        }
        #about-us{
            padding: 90px 0 60px 0;
        }
    </style>
@stop


@section('user-page-content')

    <main id="main">
        <section id="about-us" class="section-bg">
            <div class="col-md-8 offset-md-2">

                <h2 class="text-center">Forgot Password</h2>

                <form method="POST" action="/password/email">
                    {!! csrf_field() !!}
                    @include('partials.errors')
                    @include('partials.status')

                    <div class="col-md-12 raw-margin-top-24">
                        <label>Email</label>
                        <input class="form-control" type="email" name="email" placeholder="Email" value="{{ old('email') }}">
                    </div>
                    <div class="col-md-12 raw-margin-top-24">
                        <button class="btn button_theme_color btn-block" type="submit" class="button">Send Password Reset Link</button>
                    </div>
                    <div class="col-md-12 raw-margin-top-24">
                        <a class="btn btn-link" href="/login">Wait I remember!</a>
                    </div>
                </form>

            </div>
        </section>
    </main>
@stop
