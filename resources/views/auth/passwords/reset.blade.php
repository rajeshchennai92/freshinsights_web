@extends('user.layouts.master')

@section('title', 'FreshInsights - Reset Password')

@section('individual-style')
    <style>
        #header{
            background: #fff;
            padding: 20px 0;
            height: 72px;
        }
        #about-us{
            padding: 90px 0 60px 0;
        }
    </style>
@stop


@section('user-page-content')

    <main id="main">
        <section id="about-us" class="section-bg">
            <div class="col-md-8 offset-md-2">

                <h1 class="text-center">Password Reset</h1>

                <form method="POST" action="/password/reset">
                    {!! csrf_field() !!}
                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="col-md-12 raw-margin-top-24">
                        <label>Email</label>
                        <input class="form-control" type="email" name="email" value="{{ old('email') }}">
                    </div>
                    <div class="col-md-12 raw-margin-top-24">
                        <label>Password</label>
                        <input class="form-control" type="password" name="password">
                    </div>
                    <div class="col-md-12 raw-margin-top-24">
                        <label>Confirm Password</label>
                        <input class="form-control" type="password" name="password_confirmation">
                    </div>
                    <div class="col-md-12 raw-margin-top-24">
                        <button class="btn button_theme_color" type="submit">Reset Password</button>
                    </div>
                </form>
            </div>
        </section>
    </main>
@stop