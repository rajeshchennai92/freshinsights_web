@extends('user.layouts.master')

@section('title', 'FreshInsights - Freshdesk Tableau Register')

@section('individual-style')
    @include('billing.freshdesk-tableau.style')
@stop

@section('user-page-content')
    @include('billing.freshdesk-tableau.navigation')
    <main id="main">
        <section id="about-us" class="section-bg">
            <h2 class="text-center">Register</h2>
            <div class="col-md-6 offset-md-3">
                <form method="POST" action="/register/freshdesk/tableau" name="registerFreshdesk" id="registerFreshdesk">
                    {!! csrf_field() !!}
                    <div class="col-md-12 raw-margin-top-12 form_same_line">
                        <label class="col-md-4 float-left">Organization Name <span class="error">*</span></label>
                        <div class="col-md-8 float-left">
                            <input class="form-control" type="text" name="name" value="{{ old('name') }}" placeholder="Organization Name" required>
                            @if ($errors->has('name'))
                                <div class="error message">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                    </div>
                    @if(!Auth::check())
                        <div class="col-md-12 raw-margin-top-12 form_same_line">
                            <label class="col-md-4 float-left">Email <span class="error">*</span></label>
                            <div class="col-md-8 float-left">
                                <input class="form-control" type="email" name="email" value="{{ old('email') }}" placeholder="Email" required>
                                @if ($errors->has('email'))
                                    <div class="error message">{{ $errors->first('email') }}</div>
                                @endif
                            </div>
                        </div>
                    @endif
                    <div class="col-md-12 raw-margin-top-12 form_same_line">
                        <label class="col-md-4 float-left">Helpdesk URL <span class="error">*</span><span class="pl-2 fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Example if the organization name is 'radiare' Helpdesk url is : https://radiare.freshdesk.com"></span></label>
                        <div class="col-md-8 float-left">
                            <input class="form-control" type="text" name="organization_url" value="{{ old('organization_url') }}" placeholder="Organization Helpdesk URL" required>
                            <div class="org_url_error message" style="color: red;"></div>
                            @if ($errors->has('organization_url'))
                                <div class="error">{{ $errors->first('organization_url') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12 raw-margin-top-12 form_same_line">
                        <label class="col-md-4 float-left">API key <span class="error">*</span><span class="pl-2 fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Steps to generate API Key
1) Login to your Freshdesk Account.
2) Click on your Agent Avatar on the top right and select Profile Settings.
3) On the right pane, you would find the API Key.
4) Copy-paste this as required to authenticate third-party solutions."></span></label>
                        <div class="col-md-8 float-left">
                            <input class="form-control" type="password" name="organization_api_key" placeholder="Organization API key" required>
                            <div class="org_api_error message" style="color: red;"></div>
                            @if ($errors->has('organization_api_key'))
                                <div class="error message">{{ $errors->first('organization_api_key') }}</div>
                            @endif
                            <div class="org_api_success message" style="color: green;"></div>
                            <button class="btn button_theme_color btn-sm raw-margin-top-5 button_theme_color_sm" onclick="testConnection(event)">Test Connection</button><i class="fa fa-spinner fa-spin ml-2" id="spinner" style="font-size:24px;display: none"></i>
                        </div>
                    </div>
                    @if(Auth::check())
                        <div class="col-md-8 offset-md-2 raw-margin-top-12 form_same_line">
                            <div class="btn-toolbar justify-content-between">
                                <button class="btn button_theme_color" type="submit" id="registerFsButton">Register</button>
                            </div>
                        </div>
                    @else
                        <div class="col-md-12 raw-margin-top-12 form_same_line">
                            <label class="col-md-4 float-left">Password <span class="error">*</span></label>
                            <div class="col-md-8 float-left">
                                <input class="form-control" type="password" name="password" placeholder="Password" required>
                                @if ($errors->has('password'))
                                    <div class="error message">{{ $errors->first('password') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12 raw-margin-top-24 form_same_line">
                            <label class="col-md-4 float-left">Confirm Password <span class="error">*</span></label>
                            <div class="col-md-8 float-left">
                                <input class="form-control" type="password" name="password_confirmation" placeholder="Password Confirmation" required>
                            </div>
                        </div>
                        <div class="col-md-8 offset-md-4 raw-margin-top-24">
                            <p style="font-size: 12px;"><span class="error">*</span> We assure you that no data is downloaded on registration. The data download will happen only on chosing the pricing plan.</p>
                            <div class="btn-toolbar justify-content-between">
                                <button class="btn button_theme_color" type="submit" id="registerButton" data-toggle="tooltip" data-placement="top" title="Test connection before registering">Register</button>
                                <a class="btn btn-link" href="/freshdesk/tableau/login">Sign in</a>
                            </div>
                        </div>
                    @endif
                </form>

            </div>
        </section>

@stop