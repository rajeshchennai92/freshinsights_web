@extends('user.layouts.master')

@section('title', 'FreshInsights - Freshdesk Register')

@section('individual-style')
    @include('billing.freshdesk-tableau.style')
@stop

@section('user-page-content')
    @include('billing.freshdesk-tableau.navigation')
    <main id="main">
        <section id="about-us" class="section-bg">
            <h2 class="text-center">Register</h2>
            <div class="col-md-6 offset-md-3">
                <form method="POST" action="/register/freshdesk" name="registerFreshdesk" id="registerFreshdesk">
                    {!! csrf_field() !!}
                    <div class="col-md-12 raw-margin-top-12 form_same_line">
                        <label class="col-md-4 float-left">Organization Name <span class="error">*</span></label>
                        <div class="col-md-8 float-left">
                            <input class="form-control" type="text" name="name" value="{{ old('name') }}" placeholder="Organization Name" required>
                            @if ($errors->has('name'))
                                <div class="error message">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                    </div>
                    @if(!Auth::check())
                        <div class="col-md-12 raw-margin-top-12 form_same_line">
                            <label class="col-md-4 float-left">Email <span class="error">*</span></label>
                            <div class="col-md-8 float-left">
                                <input class="form-control" type="email" name="email" value="{{ old('email') }}" placeholder="Email" required>
                                @if ($errors->has('email'))
                                    <div class="error message">{{ $errors->first('email') }}</div>
                                @endif
                            </div>
                        </div>


                        <div class="col-md-12 raw-margin-top-12 form_same_line">
                            <label class="col-md-4 float-left"><span class="error"></span></label>
                                <div class="col-md-8 float-left form-check">
                                    <label class="radio-inline">
                                        <input type="radio"  onclick="javascript:yesnoCheck();"id="powerbi" name="plan" value="1" checked> <label><img src="/img/powerbi.png" style="height: 18px"> Power BI</label>
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" onclick="javascript:yesnoCheck();"id="tableau" name="plan"  value="2"> <label><img src="/img/tableau.png" style="height: 20px;border-radius:50%;"> Tableau</label>
                                    </label>

                                </div>

                        </div>

                        <div class="col-md-12 raw-margin-top-12 form_same_line" id="showcase" style="display:block">
                            <label class="col-md-4 float-left">Power BI Email <span class="error">*</span><span class="pl-2 fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="The email contains the Power BI Pro account to share the dashboard"></span></label>
                            <div class="col-md-8 float-left">
                                <input class="form-control" type="email" name="powerbi_email" value="{{ old('powerbi_email') }}" placeholder="Power BI Email" id="pmail"  required>
                                @if ($errors->has('powerbi_email'))
                                    <div class="error message">{{ $errors->first('powerbi_email') }}</div>
                                @endif
                            </div>
                        </div>
                        <div   class="col-md-12 raw-margin-top-12 form_same_line"  id="show1" style="display:block">
                            <label class="col-md-4 float-left"></label>
                            <div class="col-md-8 float-left form-check ">
                                <input class="form-check-input ml-0" type="checkbox" name="powerbi_pro_account" value="1" id="pbro" required><label class="form-check-label ml-4"> Yes, I have a Power BI pro account<span class="error">*</span><span class="pl-2 fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Power BI pro account is mandatory to register"></span></label>
                                @if ($errors->has('powerbi_pro_account'))
                                    <div class="error message">{{ $errors->first('powerbi_pro_account') }}</div>
                                @endif
                            </div>
                        </div>
                    @endif
                    <div class="col-md-12 raw-margin-top-12 form_same_line">
                        <label class="col-md-4 float-left">Helpdesk URL <span class="error">*</span><span class="pl-2 fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Example if the organization name is 'radiare' Helpdesk url is: https://radiare.freshdesk.com"></span></label>
                        <div class="col-md-8 float-left">
                            <input class="form-control" type="text" name="organization_url" value="{{ old('organization_url') }}" placeholder="Organization Helpdesk URL" required>
                            <div class="org_url_error message" style="color: red;"></div>
                            @if ($errors->has('organization_url'))
                                <div class="error">{{ $errors->first('organization_url') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12 raw-margin-top-12 form_same_line">
                        <label class="col-md-4 float-left">API key <span class="error">*</span><span class="pl-2 fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Steps to generate API Key
1) Login to your Freshdesk Account.
2) Click on your Agent Avatar on the top right and select Profile Settings.
3) On the right pane, you would find the API Key.
4) Copy-paste this as required to authenticate third-party solutions."></span></label>
                        <div class="col-md-8 float-left">
                            <input class="form-control" type="password" name="organization_api_key" placeholder="Organization API key" required>
                            <div class="org_api_error message" style="color: red;"></div>
                            @if ($errors->has('organization_api_key'))
                                <div class="error message">{{ $errors->first('organization_api_key') }}</div>
                            @endif
                            <div class="org_api_success message" style="color: green;"></div>
                            <button class="btn button_theme_color btn-sm raw-margin-top-5 button_theme_color_sm" onclick="testConnection(event)">Test Connection</button><i class="fa fa-spinner fa-spin ml-2" id="spinner" style="font-size:24px;display: none"></i>
                        </div>
                    </div>
                    @if(Auth::check())
                        <div class="col-md-8 offset-md-2 raw-margin-top-12 form_same_line">
                            <div class="btn-toolbar justify-content-between">
                                <button class="btn button_theme_color" type="submit" id="registerButton">Register</button>
                            </div>
                        </div>
                    @else
                        <div class="col-md-12 raw-margin-top-12 form_same_line">
                            <label class="col-md-4 float-left">Password <span class="error">*</span></label>
                            <div class="col-md-8 float-left">
                                <input class="form-control" type="password" name="password" placeholder="Password" required>
                                @if ($errors->has('password'))
                                    <div class="error message">{{ $errors->first('password') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12 raw-margin-top-24 form_same_line">
                            <label class="col-md-4 float-left">Confirm Password <span class="error">*</span></label>
                            <div class="col-md-8 float-left">
                                <input class="form-control" type="password" name="password_confirmation" placeholder="Password Confirmation" required>
                            </div>
                        </div>
                        <div class="col-md-8 offset-md-4 raw-margin-top-24">
                            <p style="font-size: 12px;"><span class="error">*</span> We assure you that no data is downloaded on registration. The data download will happen only on chosing the pricing plan.</p>
                            <div class="btn-toolbar justify-content-between">
                                <button class="btn button_theme_color" type="submit" id="registerButton" data-toggle="tooltip" data-placement="top" title="Test connection before registering">Register</button>
                                <a class="btn btn-link" href="/freshdesk/login">Sign in</a>
                            </div>
                        </div>
                    @endif
                </form>

            </div>
        </section>
        <script>
            function yesnoCheck() {
                var pbi = $('#pmail');
                var cpbi = $('#pbro');

                if (document.getElementById('tableau').checked) {
                    document.getElementById('showcase').style.display = 'none';
                    document.getElementById('show1').style.display = 'none';
                    pbi.attr('required', false);
                    cpbi.attr('required', false);


                }
                if (document.getElementById('powerbi').checked) {
                    document.getElementById('showcase').style.display = 'block';
                    document.getElementById('show1').style.display = 'block';
                    pbi.attr('required', true);
                    cpbi.attr('required', true);



                }

            }
        </script>
@stop